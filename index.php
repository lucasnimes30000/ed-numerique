<?php


parse_str($_SERVER["QUERY_STRING"], $qs);
$keys = array_keys($qs);
$route = array_shift($keys);

switch ($route) {
    case 'villechoix':
        include("views/annuaire.php");
        break;
    
    case "faq":
        include("views/faq.php");
        break;

    case "charte":
        include("views/charte.php");
        break;

    case "quisommesnous":
        include("views/quisommesnous.php");
        break;
        
    case "poldeconf":
        include("views/poldeconf.php");
        break;

    case "contact":
        include("controller/ctrl.contact.php");
        break;

    case "cgu":
        include("views/cgu.php");
        break;

    case "mentionslegals":
        include("views/mentionslegals.php");
        break;

    case "poldeconf":
        include("views/poldeconf.php");
        break;

    case "connexion":
        include("views/connexion.php");
        break;

    case "verif_login":
        include("controller/verif_login.php");
        break;

    case "mdpoublie":
        include("controller/ctrl.mdpoublie.php");
        break;

    case "espace_pro":
        include("views/espace_pro.php");
        break;

    case "fiche_ed":
        include("controller/ctrl.fiche_ed.php");
        break;

    case "logout":
        include("controller/logout.php");
        break;

    case "edit_mdp":
        include("controller/ctrl.edit_mdp.php");
        break;

    case "creation_mdp":
        include("views/creation_mdp.php");
        break;
    
    case "bruh":
    	include("controller/bruh.php");
    
    default:
        include("views/accueil.php");
        break;
}
