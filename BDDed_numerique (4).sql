-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 22, 2021 at 11:11 AM
-- Server version: 8.0.23-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `BDDed_numerique`
--

-- --------------------------------------------------------

--
-- Table structure for table `TBLadmin`
--

CREATE TABLE `TBLadmin` (
  `id_admin` int NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `statut` tinyint UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLadmin`
--

INSERT INTO `TBLadmin` (`id_admin`, `username`, `password`, `statut`, `role`) VALUES
(1, 'user_test', '$2y$10$FOGOdmksZV/pUejGvU66heBTgeTXuOacTjqySYbNVqPytfTgq5586', 1, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `TBLassoc_avis_user`
--

CREATE TABLE `TBLassoc_avis_user` (
  `id_assoc_avis_user` int NOT NULL,
  `id_fiche_ed` int NOT NULL,
  `id_avis` int NOT NULL,
  `statut` tinyint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `TBLassoc_demarche_user`
--

CREATE TABLE `TBLassoc_demarche_user` (
  `id_assoc_demarche_user` int NOT NULL,
  `id_demarche` int NOT NULL,
  `id_user` int NOT NULL,
  `specialite` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLassoc_demarche_user`
--

INSERT INTO `TBLassoc_demarche_user` (`id_assoc_demarche_user`, `id_demarche`, `id_user`, `specialite`) VALUES
(2, 1, 72, 0),
(3, 1, 74, 0),
(5, 1, 67, 0),
(6, 1, 76, 1),
(7, 1, 75, 1),
(8, 1, 77, 1),
(15, 1, 71, 1),
(16, 1, 79, 0),
(17, 1, 69, 0);

-- --------------------------------------------------------

--
-- Table structure for table `TBLassoc_fiche_ed`
--

CREATE TABLE `TBLassoc_fiche_ed` (
  `id_assoc_fiche_ed` int NOT NULL,
  `id_fiche_ed` int NOT NULL,
  `id_user` int NOT NULL,
  `statut` tinyint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLassoc_fiche_ed`
--

INSERT INTO `TBLassoc_fiche_ed` (`id_assoc_fiche_ed`, `id_fiche_ed`, `id_user`, `statut`) VALUES
(13, 17, 72, 1),
(14, 18, 71, 1),
(15, 19, 74, 0),
(16, 20, 73, 0),
(17, 21, 69, 1),
(18, 22, 67, 0),
(20, 24, 75, 1),
(21, 25, 77, 1),
(22, 26, 79, 0);

-- --------------------------------------------------------

--
-- Table structure for table `TBLassoc_image_jv_user`
--

CREATE TABLE `TBLassoc_image_jv_user` (
  `id_assoc_image_jv_user` int NOT NULL,
  `id_image_jv` int NOT NULL,
  `id_user` int NOT NULL,
  `specialite` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLassoc_image_jv_user`
--

INSERT INTO `TBLassoc_image_jv_user` (`id_assoc_image_jv_user`, `id_image_jv`, `id_user`, `specialite`) VALUES
(1, 6, 72, 0),
(2, 6, 72, 0),
(8, 4, 74, 0),
(9, 6, 74, 0),
(10, 2, 74, 0),
(19, 3, 67, 0),
(20, 4, 67, 0),
(21, 6, 75, 0),
(22, 5, 75, 0),
(84, 2, 77, 1),
(86, 4, 79, 0),
(87, 4, 69, 0),
(88, 5, 69, 1);

-- --------------------------------------------------------

--
-- Table structure for table `TBLassoc_infrastructure_user`
--

CREATE TABLE `TBLassoc_infrastructure_user` (
  `id_assoc_infrastructure_user` int NOT NULL,
  `id_infrastructure` int NOT NULL,
  `id_user` int NOT NULL,
  `specialite` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLassoc_infrastructure_user`
--

INSERT INTO `TBLassoc_infrastructure_user` (`id_assoc_infrastructure_user`, `id_infrastructure`, `id_user`, `specialite`) VALUES
(1, 3, 72, 0),
(13, 5, 74, 0),
(14, 6, 74, 0),
(21, 2, 67, 0),
(22, 6, 67, 0),
(23, 1, 67, 0),
(24, 6, 75, 0),
(38, 3, 75, 1),
(41, 6, 72, 1),
(50, 5, 71, 0),
(51, 4, 69, 0),
(52, 3, 79, 1),
(53, 6, 69, 0),
(54, 4, 69, 0),
(55, 6, 69, 0),
(56, 4, 69, 0),
(57, 2, 69, 0);

-- --------------------------------------------------------

--
-- Table structure for table `TBLassoc_logiciel_systeme_user`
--

CREATE TABLE `TBLassoc_logiciel_systeme_user` (
  `id_assoc_logiciel_systeme_user` int NOT NULL,
  `id_logiciel_systeme` int NOT NULL,
  `id_user` int NOT NULL,
  `specialite` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLassoc_logiciel_systeme_user`
--

INSERT INTO `TBLassoc_logiciel_systeme_user` (`id_assoc_logiciel_systeme_user`, `id_logiciel_systeme`, `id_user`, `specialite`) VALUES
(1, 1, 72, 0),
(2, 3, 74, 0),
(3, 2, 74, 0),
(5, 1, 67, 0),
(6, 1, 75, 0),
(9, 1, 76, 1),
(11, 2, 71, 1),
(12, 2, 69, 0),
(13, 3, 79, 0),
(14, 1, 79, 0);

-- --------------------------------------------------------

--
-- Table structure for table `TBLassoc_materiel_user`
--

CREATE TABLE `TBLassoc_materiel_user` (
  `id_assoc_materiel_user` int NOT NULL,
  `id_materiel` int NOT NULL,
  `id_user` int NOT NULL,
  `specialite` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLassoc_materiel_user`
--

INSERT INTO `TBLassoc_materiel_user` (`id_assoc_materiel_user`, `id_materiel`, `id_user`, `specialite`) VALUES
(41, 1, 76, 1),
(42, 1, 75, 1),
(43, 1, 72, 1),
(47, 1, 79, 0),
(48, 3, 69, 0),
(49, 2, 69, 0);

-- --------------------------------------------------------

--
-- Table structure for table `TBLassoc_statut_user`
--

CREATE TABLE `TBLassoc_statut_user` (
  `id_assoc_statut_user` int NOT NULL,
  `id_statut` int NOT NULL,
  `id_user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLassoc_statut_user`
--

INSERT INTO `TBLassoc_statut_user` (`id_assoc_statut_user`, `id_statut`, `id_user`) VALUES
(96, 3, 72),
(98, 4, 75),
(99, 4, 69),
(100, 4, 71),
(102, 3, 79);

-- --------------------------------------------------------

--
-- Table structure for table `TBLassoc_ville_user`
--

CREATE TABLE `TBLassoc_ville_user` (
  `id_assoc_ville_user` int NOT NULL,
  `id_ville` int NOT NULL,
  `id_user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLassoc_ville_user`
--

INSERT INTO `TBLassoc_ville_user` (`id_assoc_ville_user`, `id_ville`, `id_user`) VALUES
(120, 1, 70),
(126, 1, 72),
(127, 2, 72),
(129, 2, 67),
(131, 1, 74),
(133, 2, 74),
(134, 1, 67),
(135, 2, 69),
(136, 1, 75),
(137, 3, 69),
(139, 3, 75),
(140, 1, 77),
(141, 3, 76),
(142, 3, 71),
(143, 3, 77),
(146, 2, 76),
(147, 2, 71),
(148, 2, 75),
(149, 1, 79),
(150, 3, 79),
(151, 1, 80);

-- --------------------------------------------------------

--
-- Table structure for table `TBLavis`
--

CREATE TABLE `TBLavis` (
  `id_avis` int NOT NULL,
  `titre_avis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `nom_avis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `prenom_avis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `date_avis` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contenu_avis` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `TBLclick_mail`
--

CREATE TABLE `TBLclick_mail` (
  `id_click_mail` int NOT NULL,
  `nbr_click_mail` int NOT NULL,
  `annee` int NOT NULL,
  `mois` int NOT NULL,
  `semaine` int DEFAULT NULL,
  `id_user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLclick_mail`
--

INSERT INTO `TBLclick_mail` (`id_click_mail`, `nbr_click_mail`, `annee`, `mois`, `semaine`, `id_user`) VALUES
(1, 2, 2021, 4, NULL, 71),
(2, 2, 2021, 4, NULL, 77),
(3, 2, 2021, 4, NULL, 75),
(4, 1, 2021, 4, NULL, 69),
(5, 1, 2021, 4, NULL, 72);

-- --------------------------------------------------------

--
-- Table structure for table `TBLclick_profil`
--

CREATE TABLE `TBLclick_profil` (
  `id_click_profil` int NOT NULL,
  `nbr_click_profil` int NOT NULL,
  `mois` int NOT NULL,
  `annee` int NOT NULL,
  `semaine` int DEFAULT NULL,
  `id_user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLclick_profil`
--

INSERT INTO `TBLclick_profil` (`id_click_profil`, `nbr_click_profil`, `mois`, `annee`, `semaine`, `id_user`) VALUES
(4, 3, 4, 2021, NULL, 72),
(5, 4, 4, 2021, NULL, 75),
(6, 3, 4, 2021, NULL, 69),
(7, 3, 4, 2021, NULL, 71),
(8, 3, 4, 2021, NULL, 77);

-- --------------------------------------------------------

--
-- Table structure for table `TBLclick_telephone`
--

CREATE TABLE `TBLclick_telephone` (
  `id_click_telephone` int NOT NULL,
  `nbr_click_telephone` int NOT NULL,
  `annee` int NOT NULL,
  `mois` int NOT NULL,
  `semaine` int DEFAULT NULL,
  `id_user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLclick_telephone`
--

INSERT INTO `TBLclick_telephone` (`id_click_telephone`, `nbr_click_telephone`, `annee`, `mois`, `semaine`, `id_user`) VALUES
(13, 2, 2021, 4, NULL, 75),
(14, 1, 2021, 4, NULL, 69),
(15, 1, 2021, 4, NULL, 71),
(16, 1, 2021, 4, NULL, 77);

-- --------------------------------------------------------

--
-- Table structure for table `TBLcontrat`
--

CREATE TABLE `TBLcontrat` (
  `id_contrat` int NOT NULL,
  `id_user` int NOT NULL,
  `nom_contrat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `chemin` varchar(511) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `date_ajout` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `statut` tinyint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLcontrat`
--

INSERT INTO `TBLcontrat` (`id_contrat`, `id_user`, `nom_contrat`, `chemin`, `date_ajout`, `statut`) VALUES
(7, 72, 'Exercices -4- La Gestion de la Mémoire Linux.pdf', 'Exercices -4- La Gestion de la Mémoire Linux.pdf', '2021-03-11 11:12:27', 0),
(8, 73, '05.3.6 - TP06 - Correction Requêtes avec oprérateurs ensemblistes.pdf', '05.3.6 - TP06 - Correction Requêtes avec oprérateurs ensemblistes.pdf', '2021-03-11 15:07:57', 0),
(9, 67, '03.2.1 - TD Type, Orientations et Rôles des Relations.pdf', '03.2.1 - TD Type, Orientations et Rôles des Relations.pdf', '2021-03-12 10:09:37', 0),
(10, 72, '05.2 - UML to PostgreSQL.pdf', '05.2 - UML to PostgreSQL.pdf', '2021-03-12 14:45:38', 0),
(11, 72, 'C-02 - Tableaux et chaînes de caractères.pdf', 'C-02 - Tableaux et chaînes de caractères.pdf', '2021-03-12 14:45:43', 0),
(12, 69, '03.3.3 - TP Modèle Gite - Correction.pdf', '03.3.3 - TP Modèle Gite - Correction.pdf', '2021-03-18 12:40:22', 0),
(14, 69, 'UTC 503 - 018 - Fiche C-11.pdf', 'UTC 503 - 018 - Fiche C-11.pdf', '2021-03-19 16:22:57', 0),
(15, 69, '05.2 - UML to PostgreSQL.pdf', '05.2 - UML to PostgreSQL.pdf', '2021-03-19 16:23:12', 0),
(16, 71, '03.3.3 - TP Modèle Gite - Correction.pdf', '03.3.3 - TP Modèle Gite - Correction.pdf', '2021-04-19 10:37:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `TBLdemarche`
--

CREATE TABLE `TBLdemarche` (
  `id_demarche` int NOT NULL,
  `nom_demarche` tinytext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLdemarche`
--

INSERT INTO `TBLdemarche` (`id_demarche`, `nom_demarche`) VALUES
(1, 'Création de compte France Connect, Ameli, Impots, DMP, Doctolib, etc');

-- --------------------------------------------------------

--
-- Table structure for table `TBLfacture`
--

CREATE TABLE `TBLfacture` (
  `id_facture` int NOT NULL,
  `nom_facture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `chemin` varchar(511) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `TBLfiche_ed`
--

CREATE TABLE `TBLfiche_ed` (
  `id_fiche_ed` int NOT NULL,
  `me_connaitre` varchar(511) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `j_aime` varchar(511) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `info_complementaire` varchar(511) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `savoir_faire` varchar(511) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLfiche_ed`
--

INSERT INTO `TBLfiche_ed` (`id_fiche_ed`, `me_connaitre`, `j_aime`, `info_complementaire`, `savoir_faire`) VALUES
(11, '...', '...', '...', '...'),
(12, '...', '...', '...', '...'),
(13, '...', '...', '...', '...'),
(14, '...', '...', '...', '...'),
(15, '...', '...', '...', '...'),
(16, '...', '...', '...', '...'),
(17, '...', '...', '...', '...'),
(18, '...', '...', '...', '...'),
(19, '...', '...', '...', '...'),
(20, '...', '...', '...', '...'),
(21, 'Description courte. corper elit. Maecenas sodales ligula at accumsan tristique. In iaculis lobortis justo. Curabitur quis velit augue. Description courte pré-définie. corper elit. Maecenas sodales ligula at accumsan tristique. ', 'Description courte. corper elit. Maecenas sodales ligula at accumsan tristique. In iaculis lobortis justo. ', '<li>Lorem ipsum</li><li>C\'est un test</li><li>J\'accepte !</li>', '...dzqdzqdzq'),
(22, '...', '...', '...', '...'),
(23, '...', '...', '...', '...'),
(24, '...', '...', '...', '...'),
(25, '...', '...', '...', '...'),
(26, '...', '...', '...', '...');

-- --------------------------------------------------------

--
-- Table structure for table `TBLimage_jv`
--

CREATE TABLE `TBLimage_jv` (
  `id_image_jv` int NOT NULL,
  `nom_image_jv` tinytext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLimage_jv`
--

INSERT INTO `TBLimage_jv` (`id_image_jv`, `nom_image_jv`) VALUES
(2, 'TV'),
(3, 'Audio'),
(4, 'Box internet'),
(5, 'Console'),
(6, 'Conseil à l\'achat');

-- --------------------------------------------------------

--
-- Table structure for table `TBLinfrastructure`
--

CREATE TABLE `TBLinfrastructure` (
  `id_infrastructure` int NOT NULL,
  `nom_infrastructure` tinytext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLinfrastructure`
--

INSERT INTO `TBLinfrastructure` (`id_infrastructure`, `nom_infrastructure`) VALUES
(1, 'TCP/IP'),
(2, 'Firewall'),
(3, 'Modem/Routeur'),
(4, 'Switch'),
(5, 'Conseil'),
(6, 'Digitalisation');

-- --------------------------------------------------------

--
-- Table structure for table `TBLlogiciel_systeme`
--

CREATE TABLE `TBLlogiciel_systeme` (
  `id_logiciel_systeme` int NOT NULL,
  `nom_logiciel_systeme` tinytext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLlogiciel_systeme`
--

INSERT INTO `TBLlogiciel_systeme` (`id_logiciel_systeme`, `nom_logiciel_systeme`) VALUES
(1, 'Windows'),
(2, 'MacOS'),
(3, 'Linux');

-- --------------------------------------------------------

--
-- Table structure for table `TBLmateriel`
--

CREATE TABLE `TBLmateriel` (
  `id_materiel` int NOT NULL,
  `nom_materiel` tinytext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLmateriel`
--

INSERT INTO `TBLmateriel` (`id_materiel`, `nom_materiel`) VALUES
(1, 'PC'),
(2, 'MAC'),
(3, 'iOS'),
(4, 'Android'),
(5, 'Conseil à l\'achat');

-- --------------------------------------------------------

--
-- Table structure for table `TBLpedagogie`
--

CREATE TABLE `TBLpedagogie` (
  `id_pedagogie` int NOT NULL,
  `id_user` int NOT NULL,
  `commentaire` varchar(1000) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLpedagogie`
--

INSERT INTO `TBLpedagogie` (`id_pedagogie`, `id_user`, `commentaire`) VALUES
(1, 67, 'test =)'),
(2, 69, 'Test avis -- --'),
(3, 73, 'TEST d\'avis'),
(4, 74, '..qdzd'),
(5, 76, 'qdzqdz'),
(6, 78, 'qdzzqd');

-- --------------------------------------------------------

--
-- Table structure for table `TBLstatut`
--

CREATE TABLE `TBLstatut` (
  `id_statut` int NOT NULL,
  `nom_statut` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLstatut`
--

INSERT INTO `TBLstatut` (`id_statut`, `nom_statut`) VALUES
(1, 'Demandeur d\'emploi'),
(2, 'Micro entreprise'),
(3, 'Salarié'),
(4, 'Travailleur en situation de handicap');

-- --------------------------------------------------------

--
-- Table structure for table `TBLuser`
--

CREATE TABLE `TBLuser` (
  `id_user` int NOT NULL,
  `nom_user` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `prenom` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `mail` varchar(768) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `telephone` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `mot_de_passe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `token_oublie` varchar(768) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `date_inscription` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `accord_condition` tinyint(1) NOT NULL,
  `numero_siret` bigint UNSIGNED NOT NULL,
  `url_photo` varchar(2500) COLLATE utf8mb4_bin NOT NULL,
  `statut` tinyint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLuser`
--

INSERT INTO `TBLuser` (`id_user`, `nom_user`, `prenom`, `mail`, `telephone`, `mot_de_passe`, `token_oublie`, `date_inscription`, `accord_condition`, `numero_siret`, `url_photo`, `statut`) VALUES
(67, 'BOILLOT', 'Lucas', 'lucasnimes30000@gmail.com', '0779992333', '$2y$10$rUSCU2E3b2owKH0DcalCAOXMIMdDouc1sNLh/G.Eli9/ly5abGR0S', '636b7a6c7fc0476b10dc83748a5be16ff7679cea', '2021-02-25 16:20:59', 1, 12345678912345, '124115263_1263776740669305_4722941799667369597_n.jpg', 1),
(69, 'Gasq', 'Cédric', 'cedric@gmail.com', '0678965236', '$2y$10$Lx68ObNZ7WN/D6p9bol6i.4zu1/fc1LcJJawVZ.fhmcOLcpz2UAhi', '15cf1f93cee984a7f1a57b566d974b8beee7cc15', '2021-02-25 16:36:19', 1, 12345678912349, 'Cédric.jpg', 1),
(71, 'Verre', 'Azrael', 'verdeau@gmail.com', '0745552589', 'waiting', '1873ae352d6eaed8fca701e2fec034039e66e7fa', '2021-02-25 16:52:24', 0, 0, '123678386_181116366898230_3973946809677432777_n.jpg', 0),
(72, 'LANDRES', 'Azrael', 'az@gmail.com', '0123456789', '$2y$10$KpgEKVgl5MTYWkGCbH8yduoeZsqx869K.pK1x3iB3xonJPIARbDOO', '1812c70ff5674343b0045ca9e554ca51ae35dac0', '2021-02-25 18:02:15', 1, 12345678912345, '123678386_181116366898230_3973946809677432777_n.jpg', 1),
(73, 'Verre', 'Lucas', 'verdeau@gmail.com', '1234567890', 'waiting', '46e071cd540dc5a554648678c6e4b347f2859f47', '2021-03-11 12:34:40', 0, 0, '0', 0),
(74, 'Wesley', 'Barto', 'wesley@gmail.com', '0123456789', 'waiting', '8c405a0d70e515706c64ecc41b2bc27c84aee66c', '2021-03-12 13:49:52', 0, 0, '0', 0),
(75, 'Verd', 'Marchal', 'marchal@gmail.com', '0123456789', 'waiting', 'fdfaf16853bb47dfb9321ef79b372e3bbf9e3095', '2021-03-17 09:47:32', 0, 12345678912345, '0', 0),
(77, 'Bruh', 'Bruh', 'bruh@gmail.com', '0123456789', 'waiting', 'a8724d17006eb9ff722066bac411b2c363f28f21', '2021-04-12 12:04:17', 0, 0, '0', 0),
(79, 'BOILLOT', 'Jean', 'verdeau@gmail.com', '1234567890', 'waiting', '604c768f48d8590d865713ca8fb9537ad0f31d11', '2021-04-20 09:17:55', 0, 12345678912345, '0', 0),
(80, 'bruhbruh', 'bruhrbruh', 'bruh@gmail.com', '1234567890', 'waiting', '80939f117a30f061df4b344615b69f2a22d99085', '2021-04-20 16:21:36', 0, 0, '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `TBLville`
--

CREATE TABLE `TBLville` (
  `id_ville` int NOT NULL,
  `nom_ville` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `code_postal` int NOT NULL,
  `image` varchar(511) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `statut_liste` tinyint NOT NULL,
  `statut_vignette` tinyint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `TBLville`
--

INSERT INTO `TBLville` (`id_ville`, `nom_ville`, `code_postal`, `image`, `statut_liste`, `statut_vignette`) VALUES
(1, 'Alès', 30, 'views/assets/img/accueil/Alès_b.png', 1, 1),
(2, 'Nîmes', 30, 'views/assets/img/accueil/Nîmes_b.png', 1, 1),
(3, 'Montpellier', 34, 'views/assets/img/accueil/Montpellier_b.png', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `TBLadmin`
--
ALTER TABLE `TBLadmin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `TBLassoc_avis_user`
--
ALTER TABLE `TBLassoc_avis_user`
  ADD PRIMARY KEY (`id_assoc_avis_user`),
  ADD KEY `id_fiche_ed` (`id_fiche_ed`),
  ADD KEY `id_avis` (`id_avis`);

--
-- Indexes for table `TBLassoc_demarche_user`
--
ALTER TABLE `TBLassoc_demarche_user`
  ADD PRIMARY KEY (`id_assoc_demarche_user`),
  ADD KEY `id_demarche` (`id_demarche`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLassoc_fiche_ed`
--
ALTER TABLE `TBLassoc_fiche_ed`
  ADD PRIMARY KEY (`id_assoc_fiche_ed`),
  ADD KEY `id_fiche_ed` (`id_fiche_ed`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLassoc_image_jv_user`
--
ALTER TABLE `TBLassoc_image_jv_user`
  ADD PRIMARY KEY (`id_assoc_image_jv_user`),
  ADD KEY `id_image_jv` (`id_image_jv`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLassoc_infrastructure_user`
--
ALTER TABLE `TBLassoc_infrastructure_user`
  ADD PRIMARY KEY (`id_assoc_infrastructure_user`),
  ADD KEY `id_infrastructure` (`id_infrastructure`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLassoc_logiciel_systeme_user`
--
ALTER TABLE `TBLassoc_logiciel_systeme_user`
  ADD PRIMARY KEY (`id_assoc_logiciel_systeme_user`),
  ADD KEY `id_logiciel_systeme` (`id_logiciel_systeme`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLassoc_materiel_user`
--
ALTER TABLE `TBLassoc_materiel_user`
  ADD PRIMARY KEY (`id_assoc_materiel_user`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_materiel` (`id_materiel`);

--
-- Indexes for table `TBLassoc_statut_user`
--
ALTER TABLE `TBLassoc_statut_user`
  ADD PRIMARY KEY (`id_assoc_statut_user`),
  ADD KEY `id_statut` (`id_statut`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_statut_2` (`id_statut`);

--
-- Indexes for table `TBLassoc_ville_user`
--
ALTER TABLE `TBLassoc_ville_user`
  ADD PRIMARY KEY (`id_assoc_ville_user`),
  ADD KEY `id_ville` (`id_ville`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLavis`
--
ALTER TABLE `TBLavis`
  ADD PRIMARY KEY (`id_avis`);

--
-- Indexes for table `TBLclick_mail`
--
ALTER TABLE `TBLclick_mail`
  ADD PRIMARY KEY (`id_click_mail`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLclick_profil`
--
ALTER TABLE `TBLclick_profil`
  ADD PRIMARY KEY (`id_click_profil`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLclick_telephone`
--
ALTER TABLE `TBLclick_telephone`
  ADD PRIMARY KEY (`id_click_telephone`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLcontrat`
--
ALTER TABLE `TBLcontrat`
  ADD PRIMARY KEY (`id_contrat`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLdemarche`
--
ALTER TABLE `TBLdemarche`
  ADD PRIMARY KEY (`id_demarche`);

--
-- Indexes for table `TBLfacture`
--
ALTER TABLE `TBLfacture`
  ADD PRIMARY KEY (`id_facture`);

--
-- Indexes for table `TBLfiche_ed`
--
ALTER TABLE `TBLfiche_ed`
  ADD PRIMARY KEY (`id_fiche_ed`);

--
-- Indexes for table `TBLimage_jv`
--
ALTER TABLE `TBLimage_jv`
  ADD PRIMARY KEY (`id_image_jv`);

--
-- Indexes for table `TBLinfrastructure`
--
ALTER TABLE `TBLinfrastructure`
  ADD PRIMARY KEY (`id_infrastructure`);

--
-- Indexes for table `TBLlogiciel_systeme`
--
ALTER TABLE `TBLlogiciel_systeme`
  ADD PRIMARY KEY (`id_logiciel_systeme`);

--
-- Indexes for table `TBLmateriel`
--
ALTER TABLE `TBLmateriel`
  ADD PRIMARY KEY (`id_materiel`);

--
-- Indexes for table `TBLpedagogie`
--
ALTER TABLE `TBLpedagogie`
  ADD PRIMARY KEY (`id_pedagogie`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `TBLstatut`
--
ALTER TABLE `TBLstatut`
  ADD PRIMARY KEY (`id_statut`);

--
-- Indexes for table `TBLuser`
--
ALTER TABLE `TBLuser`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `TBLville`
--
ALTER TABLE `TBLville`
  ADD PRIMARY KEY (`id_ville`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `TBLadmin`
--
ALTER TABLE `TBLadmin`
  MODIFY `id_admin` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `TBLassoc_avis_user`
--
ALTER TABLE `TBLassoc_avis_user`
  MODIFY `id_assoc_avis_user` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `TBLassoc_demarche_user`
--
ALTER TABLE `TBLassoc_demarche_user`
  MODIFY `id_assoc_demarche_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `TBLassoc_fiche_ed`
--
ALTER TABLE `TBLassoc_fiche_ed`
  MODIFY `id_assoc_fiche_ed` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `TBLassoc_image_jv_user`
--
ALTER TABLE `TBLassoc_image_jv_user`
  MODIFY `id_assoc_image_jv_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `TBLassoc_infrastructure_user`
--
ALTER TABLE `TBLassoc_infrastructure_user`
  MODIFY `id_assoc_infrastructure_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `TBLassoc_logiciel_systeme_user`
--
ALTER TABLE `TBLassoc_logiciel_systeme_user`
  MODIFY `id_assoc_logiciel_systeme_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `TBLassoc_materiel_user`
--
ALTER TABLE `TBLassoc_materiel_user`
  MODIFY `id_assoc_materiel_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `TBLassoc_statut_user`
--
ALTER TABLE `TBLassoc_statut_user`
  MODIFY `id_assoc_statut_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `TBLassoc_ville_user`
--
ALTER TABLE `TBLassoc_ville_user`
  MODIFY `id_assoc_ville_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;

--
-- AUTO_INCREMENT for table `TBLavis`
--
ALTER TABLE `TBLavis`
  MODIFY `id_avis` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `TBLclick_mail`
--
ALTER TABLE `TBLclick_mail`
  MODIFY `id_click_mail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `TBLclick_profil`
--
ALTER TABLE `TBLclick_profil`
  MODIFY `id_click_profil` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `TBLclick_telephone`
--
ALTER TABLE `TBLclick_telephone`
  MODIFY `id_click_telephone` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `TBLcontrat`
--
ALTER TABLE `TBLcontrat`
  MODIFY `id_contrat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `TBLdemarche`
--
ALTER TABLE `TBLdemarche`
  MODIFY `id_demarche` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `TBLfacture`
--
ALTER TABLE `TBLfacture`
  MODIFY `id_facture` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `TBLfiche_ed`
--
ALTER TABLE `TBLfiche_ed`
  MODIFY `id_fiche_ed` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `TBLimage_jv`
--
ALTER TABLE `TBLimage_jv`
  MODIFY `id_image_jv` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `TBLinfrastructure`
--
ALTER TABLE `TBLinfrastructure`
  MODIFY `id_infrastructure` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `TBLlogiciel_systeme`
--
ALTER TABLE `TBLlogiciel_systeme`
  MODIFY `id_logiciel_systeme` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `TBLmateriel`
--
ALTER TABLE `TBLmateriel`
  MODIFY `id_materiel` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `TBLpedagogie`
--
ALTER TABLE `TBLpedagogie`
  MODIFY `id_pedagogie` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `TBLstatut`
--
ALTER TABLE `TBLstatut`
  MODIFY `id_statut` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `TBLuser`
--
ALTER TABLE `TBLuser`
  MODIFY `id_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `TBLville`
--
ALTER TABLE `TBLville`
  MODIFY `id_ville` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `TBLassoc_infrastructure_user`
--
ALTER TABLE `TBLassoc_infrastructure_user`
  ADD CONSTRAINT `TBLassoc_infrastructure_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `TBLuser` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
