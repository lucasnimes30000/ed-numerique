<?php

include('id.php');

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);

if(isset($id_user) && !empty($id_user)) {

    require("../models/select_logiciel_systeme_user.php");
    
    if(!empty($resultat_logiciel_systeme)) {
        echo "<h4 id='logiciel_systeme_titre'>Ses logiciels et systèmes</h4>
            <ul>";
        
        require("../models/select_logiciel_systeme_liste.php");

        echo "</h4></ul>";
    }
    
} else {
    echo "L'identifiant utilisateur est manquant";
}
