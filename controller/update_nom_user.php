<?php

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);
$nom_user = htmlspecialchars($_GET['nom_user'], ENT_QUOTES);

include('id.php');

if(!empty($nom_user) && !empty($id_user)) {

    require("../models/select_nom_user_with_id_user.php");
    
    if($resultat_select_nom_user[0]['nom_user'] === $nom_user) {
        echo "Nom déjà à jour !";
    } else if (!empty($resultat_select_nom_user)) {

        require("../models/update_nom_user_with_user_id.php");

    } else {
        // Nothing happens
    }

} else {
    echo "Informations manquantes !";
}