<?php

    include("models/model.contact.php"); //inclu le model

    if(isset($_POST["envoyer"])){ //vérif validation du formulaire

        if(isset($_POST["Condition"])){ //vérif validation des conditions

            $nom = htmlspecialchars(trim($_POST["nom"])); //récupération infos formulaire
            $prenom = htmlspecialchars(trim($_POST["prenom"])); //récupération infos formulaire
            $mail = htmlspecialchars(trim($_POST["mail"])); //récupération infos formulaire
            $tel = htmlspecialchars(trim($_POST["telephone"])); //récupération infos formulaire
            $code_postal = htmlspecialchars(trim($_POST["cp"])); //récupération infos formulaire
            $message = htmlspecialchars(trim($_POST["message"])); //récupération infos formulaire

            if(!empty($message)){

                $reussite = envoieMessageUtilisateur($nom,$prenom,$mail,$tel,$code_postal,$message); //fonction d'envoi du mail avec une variable de vérification

                if($reussite){ //vérif de l'envoi du mail
                    
                    $reussite = envoieMessageDeConfirmation($nom,$mail,$message); //fonction d'envoi du mail de confirmation avec une variable de vérification

                }
            }
        }
    }

    include("views/contact.php"); //inclu la vue
