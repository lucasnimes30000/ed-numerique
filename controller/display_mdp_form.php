<h3 id="alert_password">error</h3>

<form action="post">
    <input type="hidden" id="id_user" value="<?= htmlspecialchars($_GET['id_user'], ENT_QUOTES) ?>">

    <section class="input_block">
        <label for="old_password">Votre ancien mot de passe</label>
        <input class='input_password' name="old_password" placeholder="Ancien mot de passe" type="password" id="old_password" required>
    </section>

    <section class="input_block">
        <label for="new_password">Votre nouveau mot de passe</label>
        <input class='input_password' name="new_password" placeholder="Nouveau mot de passe" type="password" id="new_password" required>
    </section>

    <section class="input_block">
        <label for="new_password_2">Confirmation nouveau mot de passe</label>
        <input class='input_password' name="new_password_2" placeholder="Confirmation" type="password" id="new_password_2" required>
    </section>

    <section id="btn_block" class="input_block">
        <p id="p_submit">En modifiant votre mot de passe vous acceptez les <a href="../?cgu">conditions générales d'utilisation</a></p>
        <button class="btn_password" type="button" id="btn_password">Confirmer</button>
    </section>
</form>
