<?php

    include("models/model.mdpoublie.php");

    if(isset($_POST["sendmail"]) && isset($_POST["checkbox"])){
        $mail_1 = htmlspecialchars(trim($_POST['mail_1']));

        if(isset($mail_1)){

            $array = VerificationMailDB($mail_1);
            $test = $array[0];
            $infos = $array[1];

            if($test == 1){
                $id_user = $infos["id_user"];
                $token_oublie = $infos["token_oublie"];
                $nom = $infos["nom_user"];

                $reussite = EnvoiMailDeRecuperation($mail_1, $id_user, $token_oublie, $nom);

                header("Location: ?mdpoublie#EnvoiConfirme");
                exit;
            }
        }
    }

    include("views/mdpoublie.php");