<?php

$commentaire = htmlspecialchars($_POST['commentaire'], ENT_QUOTES);
$btn_commentaire = htmlspecialchars($_POST['commentaire'], ENT_QUOTES);
$id_user = htmlspecialchars($_POST['id_user'], ENT_QUOTES);

include('id.php');

if(isset($btn_commentaire) && !empty($id_user) && isset($commentaire) && !empty($commentaire) && isset($id_user) && !empty($id_user)) {
    if(!empty($commentaire)) {

        require("../models/insert_commentaire_user.php");

    } else if (empty($commentaire)) {

        require("../models/insert_commentaire_user_empty.php");

    }
} else {
    header('Location: ../views/profil_ed_backoffice.php?id_user=' . $id_user . '#infomanquantes');
}