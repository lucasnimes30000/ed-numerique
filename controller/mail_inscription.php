<?php

$id_user = htmlspecialchars($_POST['id_user']);
$mail_user = htmlspecialchars($_POST['mail_user']);
$nom_user = htmlspecialchars($_POST['nom_user']);
$prenom_user = htmlspecialchars($_POST['prenom_user']);

if(!empty($id_user) && !empty($mail_user) && !empty($prenom_user) && !empty($nom_user)) {

	include('id.php');

	try {
		$sql = "SELECT token_oublie FROM TBLuser WHERE id_user = :id_user";

		$req = $bdd->prepare($sql);
		$req->execute([
			':id_user' => $id_user
		]);
		$resultat = $req->fetch();
		
	} catch (PDOException $e) {

		echo "Erreur lors de l'extraction du token utilisateur " . $e->getMessage();
		
	}

	$to = $mail_user;

        $header = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

        $header .= 'To: ' . $to . "\r\n";
        $header .= 'From: contact@ed-numerique.fr' . "\r\n";

        $sujet = 'Ed numérique - création de compte';

        $message = "<html>
                <body>
                    <p>

						Bonjour " . $prenom_user . " " . $nom_user . ",<br><br>

						Merci d'avoir rejoint Ed numérique !<br><br>

						Nous avons bien pris en compte votre demande de création de compte. Pour créér votre mot de passe et activer votre compte, cliquez sur le lien ci-dessous. <br><br>

						https://ed-numerique.ovh?creation_mdp&token=" . $resultat['token_oublie'] . "&id_user=" . $id_user . "<br><br>

						Si vous rencontrez des problèmes lors de la connexion à votre compte, contactez-nous via contact@ed-numerique.fr.<br><br>

						À très vite,<br>
						L'équipe Ed numérique
                    </p>
                </body>
            </html>";

        mail($to, $sujet, $message, $header);
		header('Location: ../views/profil_ed_backoffice.php?id_user=' . $id_user . '#envoieOK');

} else {
	header('Location: ../views/inscription_suite.php?id_user=' . $id_user . '#envoiePasOK');
}