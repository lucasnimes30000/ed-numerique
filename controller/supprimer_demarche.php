<?php

$id_demarche = htmlspecialchars($_GET['id_demarche'], ENT_QUOTES);

include('id.php');

require("../models/select_id_demarche_with_id_demarche.php");

if (!empty($resultat_select)) {

    require("../models/delete_demarche_with_id_demarche.php");
    
} else {
    echo "Cet identifiant demarche n'existe pas";
}

