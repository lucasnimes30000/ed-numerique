<?php
require('id.php');

if (isset($_POST['username']) && isset($_POST['password'])) {

    $password = htmlspecialchars($_POST['password'], ENT_QUOTES);
    $username = htmlspecialchars($_POST['username'], ENT_QUOTES);

    require("../models/select_info_admin_with_username.php");

    $isPasswordCorrect = password_verify($password, $resultat_check['password']);

    if ($isPasswordCorrect && isset($password) && isset($username) && ($resultat_check['role'] === "admin")) {

        include("inc_session.php");    
        sessionInit();

        $_SESSION["check_admin"] = true;
        $_SESSION["username_admin"] = (string) $resultat_check['username'];
        $_SESSION["statut_admin"] = (int) $resultat_check["statut"];
        $_SESSION["id_admin_admin"] = (int) $resultat_check["id_admin"];

        header('Location: ../views/display_ed_annuaire.php');

    } else if(empty($username) && empty($password)) {
        header('Location: ../views/connexion_admin.php#empty');

    } else if (empty($username)) { 
        header('Location: ../views/connexion_admin.php#emptyMail');

    }  else if (empty($password)) {
        header('Location: ../views/connexion_admin.php#emptyPassword');

    } else if (!$resultat_check || $resultat_check['role'] === "user") {
        header('Location: ../views/connexion_admin.php#notFound');
        
    } else {
        header('Location: ../views/connexion_admin.php#notFound');
    }   
}
?>
