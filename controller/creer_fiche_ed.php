<?php

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);

include('id.php');

if(isset($id_user) && !empty($id_user)) {
        
    require("../models/select_assoc_id_fiche.php");

    if (empty($resultat_assoc)) {

        require("../models/select_id_fiche.php");
 
    } else {
        echo "Fiche déjà existante, impossible d'en créer une seconde !";
    }

    if (empty($resultat_second)) {

        require("../models/insert_new_fiche_ed.php"); 

        require("../models/select_fiche_ed_fresh.php");

        if(!empty($id_fiche_create[0]['id_fiche_ed'])) {

            require("../models/insert_fresh_assoc_fiche_ed.php");

        }
    }

}

