<?php

include('id.php');

$id_admin = htmlspecialchars($_GET['id_admin'], ENT_QUOTES);

if(isset($id_admin) && !empty($id_admin)) {

	try {
	
		$sql ="DELETE FROM TBLadmin WHERE id_admin = :id_admin";
		$req = $bdd->prepare($sql);
		$req->execute([
			':id_admin' => (int) $id_admin,
		]);
	
		header("Location: ../views/create_admin.php#delete_ok");
		exit;
	
	} catch (PDOException $e) {
	
	echo "Erreur lors de suppression d'un administrateur : " . $e->getMessage();
	
	}

}