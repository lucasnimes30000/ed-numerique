<?php

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);
$id_logiciel_systeme = htmlentities($_GET['id_logiciel_systeme'], ENT_QUOTES);

include('id.php');

if(!empty($id_user) && isset($id_user) && !empty($id_logiciel_systeme) && isset($id_logiciel_systeme)) {

    require("../models/select_logiciel_systeme_user_simple.php");

    if (empty($resultat_select_logiciel_systeme_user)) {

        require("../models/insert_logiciel_systeme_user.php");

    } else {
        echo "Logiciels & outils déjà à jour";
    }

}