<?php

$nom = htmlspecialchars($_GET['nom'], ENT_QUOTES);
$prenom = htmlspecialchars($_GET['prenom'], ENT_QUOTES);
$mail = htmlspecialchars($_GET['mail'], ENT_QUOTES);
$mail_confirmation = htmlspecialchars($_GET['mail_confirmation'], ENT_QUOTES);
$telephone = htmlspecialchars($_GET['telephone'], ENT_QUOTES);

$token_oublie = sha1('Phrase pour avoir un token aléatoire :D !' . $nom . $prenom . $mail . $telephone . 'mais on pourrait aussi rajouter ça il ne faut pas l\'oublier mdr');

include("id.php");

if(!empty($nom) && !empty($prenom) && !empty($mail_confirmation) && !empty($mail) && !empty($telephone)) {
    if($mail === $mail_confirmation) {
        try {

            $sql_select_user ="SELECT nom_user, prenom, mail
                            FROM TBLuser
                            WHERE nom_user = :nom
                            AND prenom = :prenom
                            AND mail = :mail";
            $req = $bdd->prepare($sql_select_user);
            $req->execute([
                ':nom'          => $nom,
                ':prenom'       => $prenom,
                ':mail'         => $mail,
            ]);

            $resultat_select_user = $req->fetchAll();

        } catch (PDOException $e) {

            echo "Erreur lors de la recherche de l'utilisateur pendant la première étape de l'inscription : " . $e->getMessage();

        }

        if (!empty($resultat_select_user)) {
            echo "Ce compte utilisateur existe déjà";
        } else {
            try {
                $sql_create_user ="INSERT INTO TBLuser(nom_user, prenom, mail, telephone, mot_de_passe, token_oublie, accord_condition, numero_siret, statut, url_photo) VALUES (:nom, :prenom, :mail, :telephone,'waiting' ,:token_oublie, 0, 0, 0, '0');";
                $req = $bdd->prepare($sql_create_user);
                $req->execute([
                    ':nom'          => $nom,
                    ':prenom'       => $prenom,
                    ':mail'         => $mail,
                    ':telephone'    => $telephone,
                    ':token_oublie' => $token_oublie
                ]);
        
                echo "Création effectuée avec succès";
            } catch (PDOException $e) {
                echo "Erreur lors de la première étape de l'inscription : " . $e->getMessage();
            }
        }
    } else {
        echo "Les adresses mail sont différentes";
    }
        

} else {
    echo "Tous les champs n'ont pas correctement été renseignés";
}



