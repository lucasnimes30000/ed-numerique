<?php

$username = htmlspecialchars($_POST['username'], ENT_QUOTES);
$password = htmlspecialchars($_POST['password'], ENT_QUOTES);

include("id.php");

if(!empty($username) && !empty($password)) {

	$password = password_hash($password, PASSWORD_DEFAULT);

	try {

		$sql ="INSERT INTO TBLadmin(username, password, statut, role) VALUES (:username, :password, 1, 'admin')";
		$req = $bdd->prepare($sql);
		$req->execute([
			':username'       => $username,
			':password'       => $password,
		]);

		header('Location: ../views/create_admin.php#OK');

	} catch (PDOException $e) {

		echo "Erreur lors de la création d'un administrateur : " . $e->getMessage();

	}

} else {
	header('Location: ../views/create_admin.php#empty');
}



