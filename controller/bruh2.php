<?php

include("../models/model.contact.php"); //inclu le model

if(empty($_POST['recaptcha-response'])){
	header('Location: ../index.php');
	exit;
}

$url = "https://www.google.com/recaptcha/api/siteverify?secret=6LfCxvgaAAAAAHvjmOL6ZPtv5fgT23JDAfypbj4A&response={$_POST['recaptcha-response']}";

if(function_exists('curl_version')){
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
}else{
    $response = file_get_contents($url);
}

if(empty($response) || is_null($response)){
    header('Location: ../index.php');
	exit;
}else{
    $data = json_decode($response);
    if($data->success){
        if(isset($_POST["Condition"])){ //vérif validation des conditions

			$nom = htmlspecialchars(trim($_POST["nom"])); //récupération infos formulaire
			$prenom = htmlspecialchars(trim($_POST["prenom"])); //récupération infos formulaire
			$mail = htmlspecialchars(trim($_POST["mail"])); //récupération infos formulaire
			$tel = htmlspecialchars(trim($_POST["telephone"])); //récupération infos formulaire
			$code_postal = htmlspecialchars(trim($_POST["cp"])); //récupération infos formulaire
			$message = htmlspecialchars(trim($_POST["message"])); //récupération infos formulaire
		
			if(!empty($message)){
		
				$reussite = envoieMessageUtilisateur($nom,$prenom,$mail,$tel,$code_postal,$message); //fonction d'envoi du mail avec une variable de vérification
		
				if($reussite){ //vérif de l'envoi du mail
					
					$reussite = envoieMessageDeConfirmation($nom,$mail,$message); //fonction d'envoi du mail de confirmation avec une variable de vérification
		
				}
			}
		}		
    }else{
        header('Location: ../index.php');
		exit;
    }
}