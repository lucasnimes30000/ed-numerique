<?php

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);
$prenom_user = htmlspecialchars($_GET['prenom_user'], ENT_QUOTES);

include('id.php');

if(!empty($prenom_user) && !empty($id_user)) {

    require("../models/select_prenom_user_with_id_user.php");
    
    if($resultat_select_prenom_user[0]['prenom'] === $prenom_user) {
        echo "Prénom déjà à jour !";
    } else if (!empty($resultat_select_prenom_user)) {

        require("../models/update_prenom_user_with_user_id.php");
    
    } else {
        // Nothing happens
    }

} else {
    echo "Informations manquantes";
}