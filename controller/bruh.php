<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include("../models/fiche_ed.php");

if(empty($_POST['recaptcha-response'])){
	header('Location: ../index.php');
	exit;
}

$url = "https://www.google.com/recaptcha/api/siteverify?secret=6LfCxvgaAAAAAHvjmOL6ZPtv5fgT23JDAfypbj4A&response={$_POST['recaptcha-response']}";

if(function_exists('curl_version')){
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($curl);
}else{
    $response = file_get_contents($url);
}

if(empty($response) || is_null($response)){
    header('Location: ../index.php');
	exit;
}else{
    $data = json_decode($response);
    if($data->success){
        $id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);

		$info_perso = infosEd($id_user);

		$mail_ed = $info_perso["mail"];
		$nom_ed = $info_perso["nom_user"];
		$prenom_ed = $info_perso["prenom"];



		$checkbox = $_POST["checkMod"];

		if(isset($checkbox)){

			$message = htmlspecialchars($_POST["message"],ENT_QUOTES);

			if(!empty($message)){

				$nom = htmlspecialchars($_POST["nom"],ENT_QUOTES);
				$prenom = htmlspecialchars($_POST["prenom"],ENT_QUOTES);
				$mail = htmlspecialchars($_POST["mail"],ENT_QUOTES);
				$telephone = htmlspecialchars($_POST["telephone"],ENT_QUOTES);
				$code_postal = htmlspecialchars($_POST["cp"],ENT_QUOTES);

				$reussite = envoiMailDemandeContact($mail_ed, $nom_ed, $prenom_ed, $nom, $prenom, $mail, $telephone, $code_postal, $message);

				if($reussite){

					$reussite = envoieMessageDeConfirmation($nom, $mail, $message, $nom_ed, $prenom_ed);

					$reussite = envoiMailContactEdNumerique($mail_ed, $nom_ed, $prenom_ed, $nom, $prenom, $mail, $telephone, $code_postal, $message);

					if($reussite){
						echo "1";
					}
				}
			}
		}
    }else{
        header('Location: ../index.php');
		exit;
    }
}