<?php 
require('controller/id.php');

if (isset($_POST['email']) && isset($_POST['password'])) {

    $password = htmlspecialchars($_POST['password'], ENT_QUOTES);
    $email = htmlspecialchars($_POST['email'], ENT_QUOTES);

    require("models/select_user_with_mail.php");

    $isPasswordCorrect = password_verify($password, $resultat_check["mot_de_passe"]);
    $status = $resultat_check["statut"];
    

    if ($isPasswordCorrect && isset($password) && isset($email) && $status != 2) {

        include("controller/inc_session.php");    
        sessionInit();

        $_SESSION["check"] = true;
        $_SESSION["email"] = $resultat_check['mail'];
        $_SESSION["id_user"] = $resultat_check['id_user'];

        header('Location: ?espace_pro');

    } else if ($resultat_check['statut'] === "0") {
        
        header('Location: ?connexion#notFound');

    } else {

        header('Location: ?connexion#notFound');
    }   
}
?>
