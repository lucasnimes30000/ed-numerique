<?php

include('id.php');

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);

if(isset($id_user) && !empty($id_user)) {

    require("../models/select_statut_fiche.php");

    if($resultat_fiche[0]['statut'] === "0") {
        echo "disabled";
    } else if($resultat_fiche[0]['statut'] === "1") {
        echo "enabled";
    } 

}