<?php

$content = htmlspecialchars($_GET['content'], ENT_QUOTES);
$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);

include('id.php');

require("../models/select_id_ficheEd_with_id_user.php");

if (!empty($id_user_select['id_fiche_ed']) && !empty($content) && !empty($id_user)) {

    require("../models/update_ficheEd_set_meConnaitre_with_id_ficheEd.php");
    
} else {
    // Nothing happens
}