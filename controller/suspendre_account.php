<?php

include('id.php');

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);
$statut = htmlspecialchars($_GET['statut'], ENT_QUOTES);

if(isset($id_user) && !empty($id_user)) {

	require("../models/update_statut_with_id_user.php");

	require("../models/select_statut_with_id_user.php");

}