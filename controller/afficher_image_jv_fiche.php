<?php

include('id.php');

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);

if(isset($id_user) && !empty($id_user)) {

    require("../models/select_image_jv_user.php");

    if(!empty($resultat_image_jv)) {
        echo "<h4 id='image_jv_titre'>Image, son et vidéo</h4>
            <ul>";

        require("../models/select_image_jv_liste.php");

        require("../models/select_image_jv_user.php");

        echo "</ul>";
    }
    
} else {
    echo "L'identifiant utilisateur est manquant";
}