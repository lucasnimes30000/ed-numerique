<?php

include('id.php');

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);

if(isset($id_user) && !empty($id_user)) {

    require("../models/select_demarche_user.php");

    if(!empty($resultat_demarche)) {
        echo "<h4 id='demarche_titre'>Ses démarches</h4>
            <ul>";

        require("../models/select_demarche.php");
        
        echo "</h4></ul>";
    }

} else {
    echo "L'identifiant utilisateur est manquant";
}