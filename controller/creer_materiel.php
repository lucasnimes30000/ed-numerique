<?php

$nom_materiel = htmlspecialchars($_GET['nom_materiel'], ENT_QUOTES);

include('id.php');

if (!empty($nom_materiel)) {

    require("../models/select_nom_materiel.php");

    if (empty($resultat_select)) {

        require("../models/insert_new_materiel.php");

    } else {
        echo "Ce materiel existe déjà";
    }
} else {
    echo "Nom de materiel invalide";
}
