<?php

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);
$id_statut = htmlentities($_GET['id_statut'], ENT_QUOTES);

include('id.php');

if(!empty($id_user) && isset($id_user) && !empty($id_statut) && isset($id_statut)) {

    require("../models/select_statut_user_simple.php");

    if (empty($resultat_select_statut_user)) {

        require("../models/insert_statut_user.php");

    } else {
        echo "Statut déjà à jour";
    }

}