<?php

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);
$id_demarche = htmlentities($_GET['id_demarche'], ENT_QUOTES);

include('id.php');

/**
 * 1) Checké si l'élement est attribué déjà à l'utilisateur
 * 2) Si déjà attribué alors on continue
 * 3) Si pas attribué alors on lui attribue
 * 4) On lui attribue et renvoie un message de confirmation ou d'echec
 */

if(!empty($id_user) && isset($id_user) && !empty($id_demarche) && isset($id_demarche)) {

    require("../models/select_demarche_user_bis.php");

    if(empty($resultat)) {

        require("../models/insert_demarche_user_spe.php");

    } else if(!empty($resultat)) {

        require("../models/update_demarche_spe_check.php");

    }

}