<?php

$nom_logiciel_systeme = htmlspecialchars($_GET['nom_logiciel_systeme'], ENT_QUOTES);

include('id.php');

if (!empty($nom_logiciel_systeme)) {

    require("../models/select_nom_logiciel_systeme.php");

    if (empty($resultat_select)) {

        require("../models/insert_new_logiciel_systeme.php");

    } else {
        echo "Ce logiciel_systeme existe déjà";
    }
} else {
    echo "Nom de logiciel & outil invalide";
}