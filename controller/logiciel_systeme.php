<?php

$id_logiciel_systeme = htmlspecialchars($_GET['id_logiciel_systeme'], ENT_QUOTES);

include('id.php');

if(isset($id_logiciel_systeme) && !empty($id_logiciel_systeme)) {

    require("../models/select_id_logiciel_systeme.php");

    if (!empty($resultat_select)) {

        require("../models/delete_logiciel_systeme.php");

    } else {
        echo "Cet identifiant logiciel_systeme n'existe pas";
    }

}

