<?php

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);
$id_materiel = htmlentities($_GET['id_materiel'], ENT_QUOTES);

include('id.php');

/**
 * 1) Checké si l'élement est attribué déjà à l'utilisateur
 * 2) Si déjà attribué alors on continue
 * 3) Si pas attribué alors on lui attribue
 * 4) On lui attribue et renvoie un message de confirmation ou d'echec
 */

if(!empty($id_user) && isset($id_user) && !empty($id_materiel) && isset($id_materiel)) {

    require("../models/select_materiel_user_simple_bis.php");

    if(empty($resultat)) {

        require("../models/insert_materiel_spe_check.php");

    } else if(!empty($resultat)) {

        require("../models/update_materiel_spe_check.php");

    }

}