<?php

$commentaire = htmlspecialchars($_POST['commentaire'], ENT_QUOTES);
$btn_commentaire = htmlspecialchars($_POST['commentaire'], ENT_QUOTES);
$id_user = htmlspecialchars($_POST['id_user'], ENT_QUOTES);

include('id.php');

if(isset($btn_commentaire) && !empty($id_user) && !empty($commentaire)) {

    require("../models/update_TBLpedago_set_commentaire_with_id_user.php");
    
} else {
    header("Location: ../views/profil_ed_backoffice.php?id_user=" . $id_user . "#infomanquantes");
}

