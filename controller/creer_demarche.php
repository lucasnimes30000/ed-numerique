<?php

$nom_demarche = htmlspecialchars($_GET['nom_demarche'], ENT_QUOTES);

include('id.php');

if(isset($nom_demarche) && !empty($nom_demarche)) {
    if(!empty($nom_demarche)) {

        require("../models/select_demarche_bis.php");

        if (empty($resultat_select)) {

            require("../models/insert_demarche_bis.php");

        } else {
            echo "Ce demarche existe déjà";
        }
    } else {
        echo "Nom de logiciel & outil invalide";
    }
}