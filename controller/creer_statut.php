<?php

$nom_statut = htmlspecialchars($_GET['nom_statut'], ENT_QUOTES);

include('id.php');

if (!empty($nom_statut)) {

    require("../models/select_nom_statut.php");

    if (empty($resultat_select)) {

        require("../models/insert_new_statut.php");

    } else {
        echo "Ce statut existe déjà";
    }
} else {
    echo "Nom de statut invalide";
}