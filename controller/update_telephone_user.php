<?php

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);

$telephone_user = htmlspecialchars($_GET['telephone_user'], ENT_QUOTES);

$telephone_user = str_replace('[ -\/:-@\[-\`{-~]', '', $telephone_user);
$telephone_user = str_replace('[A-Za-zÀ-ȕ ]', '', $telephone_user);

include('id.php');

if(strlen($telephone_user) === 10) {

    if(!empty($telephone_user) && !empty($id_user)) {

        require("../models/select_telephone_with_id_user.php");
        
        if($resultat_select_telephone_user[0]['telephone'] === $telephone_user) {
            echo "Numéro de téléphone déjà à jour !";
        } else if (!empty($resultat_select_telephone_user)) {

            require("../models/update_telephone_with_id_user.php");

        } else {
            // Nothing happens
        }

    } else {
        echo "Informations manquantes";
    }

} else {
    echo "Longueur obligatoire de 10 chiffres !";
}