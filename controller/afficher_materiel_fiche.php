<?php

include('id.php');

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);

if(isset($id_user) && !empty($id_user)) {

    require("../models/select_materiel_user.php");

    if(!empty($resultat_materiel)) {

        echo "<h4 id='materiel_titre'>Son matériel</h4>
            <ul>";
        
        require("../models/select_materiel_liste.php");
    
        echo "</h4></ul>";

    }

} else { 

    echo "L'identifiant utilisateur est manquant";

}

