<?php

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);
$adresse_mail_user = htmlspecialchars($_GET['adresse_mail_user'], ENT_QUOTES);

include('id.php');

if(!empty($adresse_mail_user) && !empty($id_user)) {

    require("../models/select_mail_with_id_user.php");
    
    if($resultat_select_adresse_mail_user[0]['mail'] === $adresse_mail_user) {
        echo "Adresse mail déjà à jour !";
    } else if (!empty($resultat_select_adresse_mail_user)) {

        require("../models/update_mail_with_id_user.php");
        
    } else {
        // Nothing happens
    }

} else {
    echo "Informations manquantes";
}