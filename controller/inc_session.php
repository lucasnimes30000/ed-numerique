<?php

  // =======================================

  function sessionInit() {

    @ini_set("session.cookie_httponly", 1);
    $userFingerprint = substr(sha1(@$_SERVER["HTTP_USER_AGENT"] . @$_SERVER["HTTP_ACCEPT"] . @$_SERVER["HTTP_ACCEPT_LANGUAGE"] . "Une phrase comme ça pour bien sécuriser et on est bon !! :)))"), 0, 30);
    session_name("ed-" . $userFingerprint);
    session_start();
    session_regenerate_id();

  }

  function sessionValid() {

    return (isset($_SESSION["check"]) AND !empty($_SESSION["check"]));

  }

  function sessionValidAdmin() {

    return (isset($_SESSION["check_admin"]) AND !empty($_SESSION["check_admin"]));

  }

  function sessionBye() {

    if (!empty($_SESSION)) $_SESSION = [];
    if (isset($_COOKIE[session_name()])) setcookie(session_name(), "", time()-1, "/");
    session_destroy();
    
  }