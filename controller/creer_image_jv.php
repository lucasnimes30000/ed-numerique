<?php

$nom_image_jv = htmlspecialchars($_GET['nom_image_jv'], ENT_QUOTES);

include('id.php');

if (!empty($nom_image_jv)) {

    require("../models/select_image_jv_nom.php");

    if (empty($resultat_select)) {

        require("../models/insert_image_jv_nom.php");

    } else {
        echo "Ce image_jv existe déjà";
    }
} else {
    echo "Nom de image_jv invalide";
}
