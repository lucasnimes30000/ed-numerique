<?php

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);
$id_demarche = htmlentities($_GET['id_demarche'], ENT_QUOTES);

include('id.php');

if(!empty($id_user) && isset($id_user) && !empty($id_demarche) && isset($id_demarche)) {

    require("../models/select_demarche_user_simple.php");

    if (empty($resultat_select_demarche_user)) {

        require("../models/insert_demarche_user.php");

    } else {
        echo "Démarches déjà à jour";
    }

} 