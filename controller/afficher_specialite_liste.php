<?php

include('id.php');
$b = 0;

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);


if(isset($id_user) && !empty($id_user)) {

    // LOGICIEL SYSTEME
    require("../models/select_specialite_logiciel_systeme_user.php");
    require("../models/select_specialite_logiciel_systeme_liste_user.php");

    // IMAGE JEUX VIDEOS
    require("../models/select_specialite_image_jv_user.php");
    require("../models/select_specialite_image_jv_liste_user.php");

    // INFRASTRUCTURE
    require("../models/select_specialite_infrastructure_user.php");
    require("../models/select_specialite_infrastructure_liste_user.php");

    // MATERIEL
    require("../models/select_specialite_materiel_user.php");
    require("../models/select_specialite_materiel_liste_user.php");

    // DEMARCHE
    require("../models/select_specialite_demarche_user.php");
    require("../models/select_specialite_demarche_liste_user.php");

} else {
    echo "L'identifiant utilisateur est manquant";
}
