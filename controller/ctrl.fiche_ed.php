<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    include("controller/inc_session.php");
    session_start();
    
    include("models/model.fiche_ed.php");

    $id_user = $_GET['id_user'];

    $info_perso = infosEd($id_user);
    $commentaire = commentaire($id_user);
    $displayCommentaire = array();

    $info_complementaire = $info_perso['info_complementaire'];
    $info_complementaire = explode(',', $info_complementaire);

    if(!empty($commentaire)) array_push($displayCommentaire, "<h1 style='color:#258989'>Avis</h1>");

    foreach($commentaire as $value){
        array_push($displayCommentaire, "<article>
                                            <div>
                                                <img src='views/assets/img/annuaire/user.svg' alt='Logo'>
                                                <p><b>". htmlentities($value["nom_com"], ENT_QUOTES) ."</b>
                                                ". htmlentities($value["prenom_com"], ENT_QUOTES) ."</p>
                                                <i>". htmlentities($value["date_com"], ENT_QUOTES) ."</i>
                                            </div>
                                            <div>
                                                <h3>". htmlentities($value["titre_com"], ENT_QUOTES) ."</h3>
                                                <p>". htmlentities($value["contenu_com"], ENT_QUOTES) ."</p>
                                            </div>
                                        </article>");
    }

    include("views/fiche_ed.php");