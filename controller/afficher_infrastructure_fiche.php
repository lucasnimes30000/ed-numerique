<?php

include('id.php');

$id_user = htmlentities($_GET['id_user'], ENT_QUOTES);

if(isset($id_user) && !empty($id_user)) {

    require("../models/select_infrastructure_user.php");
    
    if(!empty($resultat_infrastructure)) {
        echo "<h4 id='infrastructure_titre'>Ses infrastructures</h4>
            <ul>";

        require("../models/select_infrastructure_liste.php");

        echo "</h4></ul>";
    }    
} else {
    echo "L'identifiant utilisateur est manquant";
}
