<?php

try {
	$sql_demarche = "SELECT DISTINCT TBLdemarche.nom_demarche, TBLdemarche.id_demarche 
				FROM TBLassoc_demarche_user 
				INNER JOIN TBLdemarche 
				ON TBLassoc_demarche_user.id_demarche = TBLdemarche.id_demarche 
				INNER JOIN TBLuser 
				ON TBLassoc_demarche_user.id_user = TBLuser.id_user 
				WHERE TBLuser.id_user = :id_user AND TBLassoc_demarche_user.specialite = 1";

	$req_demarche = $bdd->prepare($sql_demarche);
	$req_demarche->execute([
		':id_user' => $id_user
	]);
	$resultat_demarche = $req_demarche->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans la vérification des demarches en lien avec l'utilisateur : " . $e->getMessage();
	
}