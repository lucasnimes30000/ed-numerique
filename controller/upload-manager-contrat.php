<?php

include ("id.php");

$id_user = htmlspecialchars($_POST['id_user'], ENT_QUOTES);

// Check if the form was submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if file was uploaded without errors
    if(isset($_FILES["contrat"]) && $_FILES["contrat"]["error"] == 0){
        $allowed = array("pdf" => "application/pdf");
        $filename = htmlspecialchars($_FILES["contrat"]["name"], ENT_QUOTES);
        $filetype = htmlspecialchars($_FILES["contrat"]["type"], ENT_QUOTES);
        $filesize = htmlspecialchars($_FILES["contrat"]["size"], ENT_QUOTES);
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: Merci de sélectionner un format de fichier accepté (.pdf)");
    
        // Verify MYME type of the file
        if(in_array($filetype, $allowed)){
            // Check whether file exists before uploading it
            if(file_exists("../views/assets/user/" . $id_user . "/contrat/" . $filename)){
                echo $filename . " existe déjà";
            } else{

                require("../models/select_contrat_with_id_user.php");

                if($url_contrat['chemin'] !== $filename) {
                    move_uploaded_file($_FILES["contrat"]["tmp_name"], "../views/assets/user/" . $id_user . "/contrat/" . $filename);

                    unlink("../views/assets/user/" . $id_user . "/contrat/" . $url_contrat['chemin']);

                    require("../models/insert_contrat.php");
    
                } else {
                    echo "erreur ce fichier existe déjà wsh";
                }

            } 
        } else{
            echo "Error: Il y a eu un problème avec le téléchargement du fichier, merci d'essayer à nouveau."; 
        }
    } else{
        header('Location: ../views/profil_ed_backoffice.php?id_user=' . $id_user . '#info_perso');
    }
}

