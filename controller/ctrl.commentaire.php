<?php
    include("../controller/inc_session.php");
    sessionInit();

    if (sessionValidAdmin() !== true) {
        sessionBye(); 
        header("Location: ../views/connexion_admin.php#sessionerror");
        exit;
    }

    include("../views/commentaire.php");