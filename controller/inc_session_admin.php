<?php

  function sessionInitAdmin() {

    @ini_set("session.cookie_httponly", 1);
    $userFingerprint = substr(sha1(@$_SERVER["HTTP_USER_AGENT"] . @$_SERVER["HTTP_ACCEPT"] . @$_SERVER["HTTP_ACCEPT_LANGUAGE"] . "Bon disons que l'on veut une autre phrase pour l'admin, ça parait normal, non?"), 0, 30);
    session_name("ed.admin-" . $userFingerprint);
    session_start();
    session_regenerate_id();

  }

  function sessionValidAdmin() {

    return (isset($_SESSION["check_admin"]) AND !empty($_SESSION["check_admin"]));

  }

  function sessionByeAdmin() {

    if (!empty($_SESSION)) $_SESSION = [];
    if (isset($_COOKIE[session_name()])) setcookie(session_name(), "", time()-1, "/");
    session_destroy();
    
  }  


  // ===========================