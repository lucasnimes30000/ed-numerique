<?php

$id_materiel = htmlspecialchars($_GET['id_materiel'], ENT_QUOTES);

include('id.php');

require("../models/select_id_materiel_with_id_materiel.php");

if (!empty($resultat_select)) {

    require("../models/delete_materiel.php");

} else {
    echo "Cet identifiant materiel n'existe pas";
}

