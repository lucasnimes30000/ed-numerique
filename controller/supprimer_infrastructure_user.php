<?php

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);
$id_infrastructure = htmlspecialchars($_GET['id_infrastructure'], ENT_QUOTES);

include('id.php');

require("../models/select_id_infrastructure_with_id_infrastructure.php");

if (!empty($resultat_select_infrastructure_user)) {

    require("../models/delete_assoc_infrastructure_user_with_id_infrastructure.php");
    
} else {
    // Nothing happens
}