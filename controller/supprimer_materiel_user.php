<?php

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);
$id_materiel = htmlspecialchars($_GET['id_materiel'], ENT_QUOTES);

include('id.php');

require("../models/select_id_materiel.php");

if (!empty($resultat_select_materiel_user)) {

    require("../models/delete_assoc_materiel_user.php");
    
} else {
    // Nothing happens
}