<?php

include('id.php');
$nom_ville = htmlspecialchars($_GET['nom_ville'], ENT_QUOTES);
$id_user  = htmlspecialchars($_GET['id_user'], ENT_QUOTES);

if(isset($id_user) && !empty($id_user) && isset($nom_ville) && !empty($nom_ville)) {

    require("../models/select_ville.php");

    if(!empty($id_ville)) {

        require("../models/select_ville_user_bis.php");
        
        if (empty($resultat_check_ville)) {

            require("../models/insert_ville_user.php");
            
        } else {
            echo "Cette ville est déjà attribuée à cet utilisateur";
        }
        
    } else {
        echo "Cette ville n'existe pas dans base de données";
    }

}