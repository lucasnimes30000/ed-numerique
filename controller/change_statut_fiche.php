<?php

include('id.php');

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);
$statut = htmlspecialchars($_GET['statut'], ENT_QUOTES);

if(isset($id_user) && !empty($id_user)) {
    
    try {
        $sql_fiche = "UPDATE TBLassoc_fiche_ed SET statut = :statut WHERE id_user = :id_user";

        $req_fiche = $bdd->prepare($sql_fiche);
        $req_fiche->execute([
            ':id_user' => $id_user,
            ':statut' => $statut
        ]);
        
    } catch (PDOException $e) {

        echo "Erreur dans la modification du statut de la fiche : " . $e->getMessage();
        
    }

} else {
    echo "L'identifiant utilisateur est manquant";
}