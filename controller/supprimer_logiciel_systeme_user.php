<?php

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);
$id_logiciel_systeme = htmlspecialchars($_GET['id_logiciel_systeme'], ENT_QUOTES);

include('id.php');

require("../models/select_id_logiciel_systeme_with_id_user.php");

if (!empty($resultat_select_logiciel_systeme_user)) {

    require("../models/delete_assoc_logiciel_systeme_user.php");
    
} else {
    // Nothing happens
}