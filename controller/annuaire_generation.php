<?php
/**
 * <! SCRIPT GENERATION DES VIGNETTES !>
**/

$id_ville = htmlspecialchars($_GET['id_ville'], ENT_QUOTES);
$arr_id_ville = [];
$arr_id_user = [];

$arr_specialite = [];

$arr_vignette = [];

require('id.php');

if(isset($id_ville) && !empty($id_ville)) {
        
    // Recherche des id ed qui sont associés à la ville choisie

    require("../models/select_ville_user.php");

    // On met toutes les id des ed assignées à la ville choisie dans un tableau en retirant tous les doublons d'id

    foreach($resultat as $key => $value) {
        $key_check = array_search($key, $arr_id_ville);

        if($key_check === false) {
            $info = htmlspecialchars($value['id_user'], ENT_QUOTES);
            
            $info = [
                "id_user" => $info,
                "nom" => "nom",
                "prenom" => "prenom",
                "img_profil" => "img_profil",
                "spe_1" => "",
                "spe_2" => "",
                "spe_3" => ""
            ];

            array_push($arr_vignette, $info);
        }
    };

    // Viens ajouter au tableau de chaque id_user les noms - prénoms - url image profil

    for($i = 0; $i < count($arr_vignette); $i++) {

        $b = 0;

        $id_user = htmlspecialchars($arr_vignette[$i]['id_user'], ENT_QUOTES);

        require("../models/select_fiche_ed_two.php");

        if(!empty($resultat_fiche) && $resultat_fiche[0]['statut'] === "1") {

            $id_user = $arr_vignette[$i]['id_user'];

            require("../models/select_user_info.php");

            // =========================== IMAGE_JV ===========================

            require("../models/select_vignette_image_jv.php");

            // =========================== LOGICIEL SYSTEME ===========================

            require("../models/select_vignette_logiciel_systeme.php");

            // =========================== MATERIEL ===========================

            require("../models/select_vignette_materiel.php");

            // =========================== DEMARCHE ===========================

            require("../models/select_vignette_demarche.php");

            // =========================== INFRASTRUCTURE ===========================

            require("../models/select_vignette_infrastructure.php");

            $vignette = $arr_vignette[$i];

            echo "<article class='vignette'>
            <section class='haut_vignette'>
                <img class='img_profil' src='views/assets/user/" . $vignette['id_user'] . "/PP/" . $vignette['img_profil'] . "' alt='img profil'>
                <h3 class='nom_prenom'>" . htmlspecialchars(strtoupper($vignette['nom'])) . " " . htmlspecialchars($vignette['prenom']) . "</h3>
            </section>
            <section class='bas_vignette'>
                <h4 class='ville'></h4>
                <ul class='spe_liste'>";
                
            if($key_check === false) {
                if(!empty($arr_vignette[$i]['spe_1'])) echo "<li class='spe_detail'>" . htmlspecialchars_decode($vignette['spe_1'], ENT_QUOTES) . "</li>";
                if(!empty($arr_vignette[$i]['spe_2'])) echo "<li class='spe_detail'>" . htmlspecialchars_decode($vignette['spe_2'], ENT_QUOTES) . "</li>";
                if(!empty($arr_vignette[$i]['spe_3'])) echo "<li class='spe_detail'>" . htmlspecialchars_decode($vignette['spe_3'], ENT_QUOTES) . "</li>";
            }


            echo "</ul>
                </section>
                <section class='bouttons'>
                    <button value='" . htmlspecialchars($vignette['id_user'], ENT_QUOTES) . "' type='button' class='hidden_number'>Voir n° de téléphone</button>
                    <button value='" . htmlspecialchars($vignette['id_user'], ENT_QUOTES) . "' type='button' class='see_profil'>Profil complet ➔</button>
                </section>
            
                <input type='hidden' name='' class='url_id' value='" . $vignette['id_user'] . "'>

            </article>";

            $arr_specialite = [];
        }
    }


    // Objectif de la requete : Avoir les noms de toutes les spécialités activées pour l'id user sélectionné
    // "SELECT i.nom_infrastructure, m.nom_materiel, ls.nom_logiciel_systeme, d.nom_demarche, ij.nom_image_jv ,u.id_user
    // FROM TBLassoc_infrastructure_user AS iu
    // JOIN TBLinfrastructure AS i ON iu.id_infrastructure = i.id_infrastructure
    // JOIN TBLuser AS u ON iu.id_user = u.id_user
    // JOIN TBLassoc_materiel_user AS mu ON u.id_user = mu.id_user AND mu.specialite = 1
    // JOIN TBLmateriel AS m ON m.id_materiel = mu.id_materiel
    // JOIN TBLassoc_logiciel_systeme_user AS lsu ON u.id_user = lsu.id_user AND lsu.specialite = 1
    // JOIN TBLlogiciel_systeme AS ls ON ls.id_logiciel_systeme = lsu.id_logiciel_systeme
    // JOIN TBLassoc_demarche_user AS du ON u.id_user = du.id_user AND du.specialite = 1
    // JOIN TBLdemarche AS d ON d.id_demarche = du.id_demarche
    // JOIN TBLassoc_image_jv_user AS iju ON u.id_user = iju.id_user AND iju.specialite = 1
    // JOIN TBLimage_jv AS ij ON ij.id_image_jv = iju.id_image_jv
    // WHERE u.id_user = 69 
    // AND iu.specialite = 1";

    // var_dump($arr_vignette[$i]['prenom']);

    // Requete SQL pour sélectionner toutes les spécialités de tous les ed
    // Note : INNER JOIN 6 tables entre elles

    /**
    * ----- Structure du tableau -----
    *     "id_user" => [
    *       "nom" => "nom_user",
    *       "prenom" => "prenom_user",
    *       "img_profil" => "img_profil_user",
    *       "spe_1" => "spe_1_user",
    *       "spe_2" => "spe_2_user",
    *       "spe_3" => "spe_3_user"
    *   
    */
}
?>
