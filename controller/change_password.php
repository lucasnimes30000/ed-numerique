<?php

$id_user = htmlspecialchars($_GET['id_user'], ENT_QUOTES);
$old = htmlspecialchars($_GET['old'], ENT_QUOTES);
$new_1 = htmlspecialchars($_GET['new_1'], ENT_QUOTES);
$new_2 = htmlspecialchars($_GET['new_2'], ENT_QUOTES);

include('id.php');

if(!empty($old) && !empty($new_1) && !empty($new_2)) {
    if($new_1 === $new_2) {

        require("../models/select_mdp_user.php");

        if(password_verify($old, $resultat_old['mot_de_passe'])) {
            if(strlen($new_1) > 7) {
                if(!password_verify($new_2, $resultat_old['mot_de_passe'])) {

                    $new_1 = password_hash($new_1, PASSWORD_DEFAULT);

                    require("../models/update_password_user.php");

                } else {
                    // Mot de passe ancien et nouveaux identique, refus de faire l'action
                    echo "4";
                }

            } else {
                // Longueur de 8 caractères demandées pas respectées
                echo "3";
            }

        } else {
            // L'ancien mot de passe renseigné n'est pas le bon
            echo "2";
        }
    } else {
        // Les nouveaux mot de passe renseignés sont différents
        echo "1";
    } 
} else {
    // Un des champs est vide, vérification poussée en JS
    echo "0";
}

