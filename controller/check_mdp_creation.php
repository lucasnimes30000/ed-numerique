<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// Liens rapide de test

//http://localhost/lamp/ed-numerique/../?creation_mdp&token=f0172d2e86dc119a19e3af523ab699c4c03a01d7&id_user=39
//http://localhost/lamp/ed-numerique/?edit_mdp&token=636b7a6c7fc0476b10dc83748a5be16ff7679cea&id_user=67

include('id.php');

$password = htmlspecialchars($_POST['password'], ENT_QUOTES);
$password2 = htmlspecialchars($_POST['password2'], ENT_QUOTES);
$accord_condition = htmlspecialchars($_POST['accord_condition'], ENT_QUOTES);
$token = htmlspecialchars($_POST['token'], ENT_QUOTES);
$id_user = htmlspecialchars($_POST['id_user'], ENT_QUOTES);

$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
$prenom = htmlspecialchars($_POST['prenom'], ENT_QUOTES);
$mail = htmlspecialchars($_POST['mail'], ENT_QUOTES);

if($accord_condition) {
    if(!empty($password) && !empty($password2)){
        if($password === $password2){
            if(strlen($password) >= 8) {
                if(isset($token) || isset($id_user)) {

                    require ("../models/select_user_info_plus.php");

                    if(!empty($check_match)) {
                        
                        $password = password_hash($password, PASSWORD_DEFAULT);

                        require("../models/update_mdp_user_creation.php");

                        $to = $mail;

                        $header = 'MIME-Version: 1.0' . "\r\n";
                        $header .= 'Content-Type: text/html; charset=utf_8' . "\r\n";
                
                        $header .= 'To: ' . $to . "\r\n";
                        $header .= 'From: contact@ed-numerique.fr' . "\r\n";
                
                        $sujet = 'Bienvenue chez Ed numérique';
                
                        $message = "<html>
                                <body>
                                    <p>
                                        Bonjour " . $prenom . " " . $nom . ",<br><br>

                                        Merci d'avoir rejoint Ed numérique !<br><br>
                                        
                                        Nous vous confirmons que votre compte a bien été créé. Pour accéder à votre espace, cliquez sur le lien ci-dessous. <br><br>
                                        
                                        https://ed-numerique.ovh?connexion<br><br>
                                        
                                        Si vous rencontrez des problèmes lors de la connexion à votre compte, contactez-nous via <b>contact@ed-numerique.fr</b>.<br><br>
                                        
                                        À très vite,<br>
                                        L'équipe Ed numérique
                                    </p>
                                </body>
                            </html>";
                
                        mail($to, $sujet, $message, $header);

                        header('Location: ../?connexion');
                        exit;

                    } else {
                        header('Location: ../?creation_mdp&token=' . $token . '&id_user=' . $id_user . '#no_match');
                        exit;
                    }
                } else {
                    header('Location: ../?creation_mdp&token=' . $token . '&id_user=' . $id_user . '#erreur_id_token');
                    exit;
                }
            } else {
                header('Location: ../?creation_mdp&token=' . $token . '&id_user=' . $id_user . '#nombre_carac_8');
                exit;
            }
        } else { 
            header('Location: ../?creation_mdp&token=' . $token . '&id_user=' . $id_user . '#champs_differents');
            exit;
        }
    } else {
        header('Location: ../?creation_mdp&token=' . $token . '&id_user=' . $id_user . '#champs_vide');
        exit;
    }
} else {
    header('Location: ../?creation_mdp&token=' . $token . '&id_user=' . $id_user . '#accord_pas_ok');
    exit;
}
