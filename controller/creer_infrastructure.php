<?php

$nom_infrastructure = htmlspecialchars($_GET['nom_infrastructure'], ENT_QUOTES);

include('id.php');

if (!empty($nom_infrastructure)) {

    require("../models/select_nom_infrastructure.php");

    if (empty($resultat_select)) {

        require("../models/insert_new_infrastructure.php");

    } else {
        echo "Ce infrastructure existe déjà";
    }
} else {
    echo "Nom de logiciel & outil invalide";
}