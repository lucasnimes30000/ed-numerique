<?php

    if(isset($_GET['token']) && isset($_GET['id_user'])) { //verif si le token est l'id sont présent dans l'url

        include("models/model.edit_mdp.php"); //inclu le model

        $id_user = htmlentities($_GET['id_user'], ENT_QUOTES); //récup de l'id
        $token = htmlentities($_GET['token'], ENT_QUOTES); //récup du token

        $array = checkTokenEtId($id_user, $token); //fonctoin de vérification de l'id et du token

        $test = $array[1]; //variable qui représente le nombre de ligne affecté (doit valoir 1) 
        
        if($test == 1) { //vérif qu'une ligne à été affecté

            $match = $array[0]; //informations sur l'utilisateur
            $mail = $match["mail"]; //récup du mail
            $nom = $match["nom_user"]; //récup du nom
            $prenom = $match["prenom"]; //récup du prénom

            if(isset($_POST["edit"])){ //si le formulaire à été validé

                $checkbox = $_POST["accord_condition"]; //variable pour la vérif des conditons

                if(isset($checkbox)){ //vérif des conditons

                    if(isset($_POST["password"]) && isset($_POST["password2"])){ //vérif si les 2 champs passwords sont remplis

                        if($_POST["password"] == $_POST["password2"]){ //vérif si ils sont identiques

                            $mot_de_passe = htmlspecialchars($_POST["password"]); //stocaque du mdp
                            $test = miseAJourMdp($mot_de_passe, $id_user, $token); // fonction d'update du mdp avec une variable de vérification
        
                            if($test == 1){ //vérif de la mise à jour du mdp
        
                                $reussite = MessageUtilisateur($nom,$prenom,$mail); //fonction d'envoi du mail avec une variable de vérification
        
                                if($reussite){ //vérif de l'envoie du mail
        
                                    header("Location: ?edit_mdp&token=". $token ."&id_user=". $id_user ."#mdp_modif"); //redirection vers la même page avec un message de réussite
                                    exit;
                                }

                            } else {

                                header("Location: ?edit_mdp&token=". $token ."&id_user=". $id_user ."#erreur_creation");
                                exit;
                            }

                        } else{
        
                            header("Location: ?edit_mdp&token=". $token ."&id_user=". $id_user ."#champs_differents");
                            exit;
                        }

                    } else {

                        header("Location: ?edit_mdp&token=". $token ."&id_user=". $id_user ."#champs_vide");
                        exit;
                    }

                } else {

                    header("Location: ?edit_mdp&token=". $token ."&id_user=". $id_user ."#accord_pas_ok");
                    exit;
                }
            }

        } else if(!isset($_GET["error"])){

            header("Location: ?edit_mdp&token=". $token ."&id_user=". $id_user ."&error=1#no_match");
            exit;
        }
        
    } else if(!isset($_GET["error"])){

        header("Location: ?edit_mdp&token=". $token ."&id_user=". $id_user ."&error=0#erreur_id_token");
        exit;
    } 

    include("views/edit_mdp.php");
