<?php


try {

    $sql_select_prenom ="SELECT prenom
    FROM TBLuser
    WHERE id_user = :id_user";
    $req = $bdd->prepare($sql_select_prenom);
    $req->execute([
        ':id_user' => $id_user,
    ]);

    $resultat_select_prenom = $req->fetchAll();
    
    echo htmlspecialchars($resultat_select_prenom[0]['prenom']);

} catch (PDOException $e) {

echo "Erreur lors de la recherche d'un prenom <> user : " . $e->getMessage();

}
 