<?php

try {

	$sql_ville= "SELECT id_ville FROM TBLville WHERE nom_ville = :nom_ville";

	$req_ville = $bdd->prepare($sql_ville);
	$req_ville->execute([
		':nom_ville' => $nom_ville    
	]);
	$id_ville = $req_ville->fetch();
	$id_ville = htmlspecialchars($id_ville['id_ville'], ENT_QUOTES);

} catch (PDOException $e) {

	echo "Erreur lors de la sélection de la ville, message à transmettre au développeur : " . $e->getMessage();

}