<?php

try {
	$sql = "INSERT INTO TBLassoc_infrastructure_user(id_infrastructure, id_user, specialite) VALUES (:id_infrastructure, :id_user, 1)";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_infrastructure' => $id_infrastructure
	]);

} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}