<?php

try {
	$sql = "INSERT INTO TBLclick_mail(nbr_click_mail, annee, mois, id_user) VALUES (1, :annee, :mois, :id_user)";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':annee' => $annee,
		':mois' => $mois 
	]);
	$resultat = $req->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans l'insert des clicks mail : " . $e->getMessage();
	
}