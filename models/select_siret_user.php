<?php

try {

	$sql_siret = "SELECT numero_siret FROM TBLuser WHERE id_user = :id_user";
	$req = $bdd->prepare($sql_siret);
	$req->execute([
		':id_user' => $id_user
	]);

	$numero_siret = $req->fetch();

	if($numero_siret['numero_siret'] === '0') {
		// Nothing happens..
	} else {
		echo htmlentities($numero_siret['numero_siret'], ENT_QUOTES);
	}

} catch (PDOException $e) {

	echo "Erreur : " . $e->getMessage();

}