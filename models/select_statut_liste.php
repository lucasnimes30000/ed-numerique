<?php
    
try {
	$sql_statut = "SELECT id_statut, nom_statut FROM TBLstatut ORDER BY lower(nom_statut);";
	$req_statut = $bdd->prepare($sql_statut);
	$req_statut->execute();
	$resultat = $req_statut->fetchAll();

	$checked = "";
	
	foreach ($resultat as $row) {

		foreach ($resultat_statut as $row_user) {
			if ($row_user['id_statut'] === $row['id_statut']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='statut_checkbox'><input " . htmlentities($checked, ENT_QUOTES) . "id='statut_" . $b . "' value='" . htmlentities($row['id_statut'], ENT_QUOTES) . "' name='statut_" . $b . "' type='checkbox' class='option_statut'><label for='statut_" . $b . "'>(id: " . htmlentities($row['id_statut'], ENT_QUOTES) . ") " . htmlentities($row['nom_statut'], ENT_QUOTES) . "</label></article>";

		$checked = "";
		$b++;
	}
} catch (PDOException $e) {

	echo "Erreur dans le chargement des statuts : " . $e->getMessage();

}
