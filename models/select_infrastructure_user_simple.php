<?php

try {
	$sql = "SELECT id_assoc_infrastructure_user FROM TBLassoc_infrastructure_user WHERE id_user = :id_user AND id_infrastructure = :id_infrastructure";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_infrastructure' => $id_infrastructure
	]);
	$resultat = $req->fetchAll();

} catch (PDOException $e) {

	echo "Erreur au moment de la vérification d'attribution de la spécialité : " . $e->getMessage();
}