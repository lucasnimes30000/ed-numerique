<?php

try {

    $sql_select_materiel_user ="SELECT id_materiel, id_user
    FROM TBLassoc_materiel_user
    WHERE id_user = :id_user
    AND id_materiel = :id_materiel";
    $req = $bdd->prepare($sql_select_materiel_user);
    $req->execute([
        ':id_user' => $id_user,
        ':id_materiel' => $id_materiel    
    ]);

    $resultat_select_materiel_user = $req->fetchAll();

} catch (PDOException $e) {

echo "Erreur lors de la recherche d'un materiel <> user : " . $e->getMessage();

}