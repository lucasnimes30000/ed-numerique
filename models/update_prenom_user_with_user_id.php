<?php

try {
    $sql_id_prenom = "UPDATE TBLuser SET prenom = :prenom_user WHERE id_user = :id_user";
    $req_id_prenom = $bdd->prepare($sql_id_prenom);
    $req_id_prenom->execute([
    ':id_user' => $id_user,
    ':prenom_user' => $prenom_user,
]);
echo "Prénom mis à jour avec succès !";    
} catch (PDOException $e) {
    echo "Erreur dans l'INSERT d'une nouvelle association user <-> prénom : " . $e->getMessage();
}