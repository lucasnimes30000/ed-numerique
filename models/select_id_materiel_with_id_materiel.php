<?php

try {
    $sql_select = "SELECT id_materiel FROM TBLmateriel WHERE id_materiel = :id_materiel";
    $req_select = $bdd->prepare($sql_select);
    $req_select->execute([
        ':id_materiel' => $id_materiel
    ]);
    $resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
    echo "Erreur dans la vérification de l'existance d'un materiel : " . $e->getMessage();
}