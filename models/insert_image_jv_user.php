<?php

try {
	$sql_image_jv = "INSERT INTO TBLassoc_image_jv_user(id_image_jv, id_user, specialite) VALUES (:id_image_jv, :id_user, 0)";
	$req_image_jv = $bdd->prepare($sql_image_jv);
	$req_image_jv->execute([
		':id_user' => $id_user,
		':id_image_jv' => $id_image_jv
	]);

	echo "Image, Son & Jeu vidéo mis à jour avec succès !";    
} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();

}