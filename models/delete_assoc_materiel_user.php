<?php

try {
    $sql_id_materiel = "DELETE FROM TBLassoc_materiel_user 
    WHERE id_user = :id_user
    AND id_materiel = :id_materiel";
    $req_id_materiel = $bdd->prepare($sql_id_materiel);
    $req_id_materiel->execute([
    ':id_user' => $id_user,
    ':id_materiel' => $id_materiel,
]);
echo "Terminaux mis à jour avec succès !";    
} catch (PDOException $e) {
    echo "Erreur dans l'INSERT d'une nouvelle association user <-> materiel : " . $e->getMessage();
}