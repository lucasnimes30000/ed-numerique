<?php

try{
    $sql_match = "UPDATE TBLuser SET url_photo = :filenameDB WHERE id_user = :id_user";
    $req_match = $bdd->prepare($sql_match);
    $req_match->execute([
        ':filenameDB' => (string) $filename,
        ':id_user' => (int) $id_user
    ]);


    header('Location: ../views/profil_ed_backoffice.php?id_user=' . $id_user . '#' . $filetype);
    exit;
} catch (PDOException $e) {
    echo "Erreur dans l'insert de l'image dans la DataBase " . $e->getMessage();
}