<?php

try {
    $sql_id_image_jv = "DELETE FROM TBLassoc_image_jv_user 
    WHERE id_user = :id_user
    AND id_image_jv = :id_image_jv";
    $req_id_image_jv = $bdd->prepare($sql_id_image_jv);
    $req_id_image_jv->execute([
    ':id_user' => $id_user,
    ':id_image_jv' => $id_image_jv,
]);
echo "image_jvs mis à jour avec succès !";    
} catch (PDOException $e) {
    echo "Erreur dans l'INSERT d'une nouvelle association user <-> image_jv : " . $e->getMessage();
}