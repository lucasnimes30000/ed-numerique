<?php

try {
	$sql = "INSERT INTO TBLclick_profil(nbr_click_profil, annee, mois, id_user) VALUES (1, :annee, :mois, :id_user)";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => (int) $id_user,
		':annee' => (int) $annee,
		':mois' => (int) $mois 
	]);
	$resultat = $req->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans l'insert des clicks profil: " . $e->getMessage();
	
}