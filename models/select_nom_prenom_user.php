<?php

try {

$sql_usernames = "SELECT nom_user, prenom FROM TBLuser WHERE id_user = :id_user";
$req = $bdd->prepare($sql_usernames);
$req->execute([
	':id_user' => $id_user
]);

$nom_prenom = $req->fetch();

if(!empty($nom_prenom)) {
	echo htmlspecialchars($nom_prenom['prenom'], ENT_QUOTES) . " " . htmlspecialchars($nom_prenom['nom_user'], ENT_QUOTES);
} else {
	echo "0";
}

} catch (PDOException $e) {

echo "Failed to usernames user's ville : " . $e->getMessage();

}
