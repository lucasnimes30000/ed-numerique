<?php

try {
	$sql_image_jv = "INSERT INTO TBLimage_jv(nom_image_jv) VALUES (:nom_image_jv)";
	$req_image_jv = $bdd->prepare($sql_image_jv);
	$req_image_jv->execute([
':nom_image_jv' => $nom_image_jv
	]);

	echo "Nouveau image_jv créé avec succès !";
} catch (PDOException $e) {
	echo "Failed to create image_jv: " . $e->getMessage();
}