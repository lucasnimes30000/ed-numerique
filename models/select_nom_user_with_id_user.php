<?php

try {

    $sql_select_nom_user ="SELECT nom_user
    FROM TBLuser
    WHERE id_user = :id_user";
    $req = $bdd->prepare($sql_select_nom_user);
    $req->execute([
        ':id_user' => $id_user,
    ]);

    $resultat_select_nom_user = $req->fetchAll();

} catch (PDOException $e) {

echo "Erreur lors de la recherche d'un nom <> user : " . $e->getMessage();

}