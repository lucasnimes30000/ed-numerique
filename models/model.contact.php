<?php

    function envoieMessageUtilisateur($nom,$prenom,$mail,$tel,$code_postal,$message){
        $to = "contact@ed-numerique.fr";

        $header = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

        $header .= 'To: ' . $to . "\r\n";
        $header .= 'From: contact@ed-numerique.fr' . "\r\n";

        $sujet = 'Nouveau formulaire contact ' . $mail;

        $message = "<html>
                <head>
                    <title>Contact</title>
                </head>
                <body>
                    <p>
                        " . $nom . ' ' . $prenom . " vous a envoyé un message, informations :<br>
                        Tél : " . $tel . "<br>
                        Email : " . $mail . "<br>
                        Code postal : " . $code_postal . "<br><br>
                        Message : " . $message . "
                    </p>
                </body>
            </html>";

            return mail($to, $sujet, $message, $header);
    }


    function envoieMessageDeConfirmation($nom,$mail,$message){
        $to = $mail;

        $header = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

        $header .= 'To: ' . $to . "\r\n";
        $header .= 'From: contact@ed-numerique.fr' . "\r\n";

        $sujet = 'Ed-numérique - votre demande de prise de contact';

        $message = "<html>
        <head>
            <title>Contact</title>
        </head>
        <body>
            <p>
                Bonjour " . $nom . ",<br><br>

                Nous avons bien reçu votre message, nos équipes vous répondrons dans les meilleurs délais.<br><br>
                
                À très vite,<br>
                L'équipe <b>Ed numérique<b>
            </p>
        </body>
        </html>";

        return mail($to, $sujet, $message, $header);
    }