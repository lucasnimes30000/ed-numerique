<?php

    $id_com = $_GET["id_com"];

    include("../controller/id.php");

    try {
        $sql_com = "DELETE FROM TBLcommentaire
            WHERE id_com = :id_com";
        $req_com = $bdd->prepare($sql_com);
        $req_com->execute(array(
            ":id_com" => $id_com
        ));

        $res = $req_com->rowCount();

        if($res == 1) echo"ok";

    } catch (PDOException $e) {
        echo "Failed to select informations: " . $e->getMessage();
    }

?>