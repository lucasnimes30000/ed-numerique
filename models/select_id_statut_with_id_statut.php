<?php

try {
    $sql_select = "SELECT id_statut FROM TBLstatut WHERE id_statut = :id_statut";
    $req_select = $bdd->prepare($sql_select);
    $req_select->execute([
        ':id_statut' => $id_statut
    ]);
    $resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
    echo "Erreur dans la vérification de l'existance d'un statut : " . $e->getMessage();
}