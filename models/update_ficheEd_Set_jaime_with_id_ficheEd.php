<?php

try {
    $sql_connaitre = "UPDATE TBLfiche_ed SET j_aime = :content WHERE id_fiche_ed = :id_fiche_ed";
    $req_connaitre = $bdd->prepare($sql_connaitre);
    $req_connaitre->execute([
        ':content' => $content,
        ':id_fiche_ed' => $id_user_select['id_fiche_ed']
    ]);
    echo "ok";
} catch (PDOException $e) {
    echo "Erreur dans l'INSERT d'une nouvelle association user <-> connaitre : " . $e->getMessage();
}