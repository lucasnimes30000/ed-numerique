<?php

try {
	$sql_statut = "INSERT INTO TBLstatut(nom_statut) VALUES (:nom_statut)";
	$req_statut = $bdd->prepare($sql_statut);
	$req_statut->execute([
	':nom_statut' => $nom_statut
	]);

	echo "Nouveau statut créé avec succès !";
} catch (PDOException $e) {
	echo "Failed to create statut: " . $e->getMessage();
}