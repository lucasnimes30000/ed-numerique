<?php

try {
	$sql = "UPDATE TBLassoc_materiel_user SET specialite = 1 WHERE id_user = :id_user AND id_materiel = :id_materiel";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_materiel' => $id_materiel
	]);
	
	echo "Spécialités matériaux mis à jour avec succès !";
} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}