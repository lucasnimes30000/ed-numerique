<?php

try {
	$sql_create_assoc = "INSERT INTO TBLassoc_fiche_ed(id_fiche_ed, id_user, statut) VALUES (:id_fiche_ed, :id_user, 0)";
	$req_create_assoc = $bdd->prepare($sql_create_assoc);
	$req_create_assoc->execute([
		':id_user' => $id_user,
		':id_fiche_ed' => htmlspecialchars($id_fiche_create[0]['id_fiche_ed'], ENT_QUOTES)
	]);
	echo "ok";

} catch (PDOException $e) {
	echo "Erreur lors de la création de l'association de la nouvelle fiche ED : " . $e->getMessage();
} 