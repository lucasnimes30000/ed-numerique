<?php

try {
	$sql_id_infrastructure = "INSERT INTO TBLassoc_infrastructure_user(id_user, id_infrastructure, specialite) VALUES (:id_user, :id_infrastructure, 0)";
	$req_id_infrastructure = $bdd->prepare($sql_id_infrastructure);
	$req_id_infrastructure->execute([
	':id_user' => $id_user,
	':id_infrastructure' => $id_infrastructure,
]);

} catch (PDOException $e) {
	echo "Erreur dans l'INSERT d'une nouvelle association user <-> infrastructure : " . $e->getMessage();
}