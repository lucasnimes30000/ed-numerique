<?php

try {
	$sql_infrastructure = "SELECT DISTINCT TBLinfrastructure.nom_infrastructure, TBLinfrastructure.id_infrastructure 
				FROM TBLassoc_infrastructure_user 
				INNER JOIN TBLinfrastructure 
				ON TBLassoc_infrastructure_user.id_infrastructure = TBLinfrastructure.id_infrastructure 
				INNER JOIN TBLuser 
				ON TBLassoc_infrastructure_user.id_user = TBLuser.id_user 
				WHERE TBLuser.id_user = :id_user AND TBLassoc_infrastructure_user.specialite = 1";

	$req_infrastructure = $bdd->prepare($sql_infrastructure);
	$req_infrastructure->execute([
		':id_user' => $id_user
	]);
	$resultat_infrastructure = $req_infrastructure->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans la vérification des infrastructures en lien avec l'utilisateur : " . $e->getMessage();
	
}