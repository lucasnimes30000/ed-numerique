<?php

try {
    $sql_select = "SELECT id_demarche FROM TBLdemarche WHERE id_demarche = :id_demarche";
    $req_select = $bdd->prepare($sql_select);
    $req_select->execute([
        ':id_demarche' => $id_demarche
    ]);
    $resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
    echo "Erreur dans la vérification de l'existance d'un demarche : " . $e->getMessage();
}