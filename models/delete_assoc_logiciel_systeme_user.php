<?php

try {
    $sql_id_logiciel_systeme = "DELETE FROM TBLassoc_logiciel_systeme_user 
    WHERE id_user = :id_user
    AND id_logiciel_systeme = :id_logiciel_systeme";
    $req_id_logiciel_systeme = $bdd->prepare($sql_id_logiciel_systeme);
    $req_id_logiciel_systeme->execute([
    ':id_user' => $id_user,
    ':id_logiciel_systeme' => $id_logiciel_systeme,
]);
echo "Logiciels & Outils mis à jour avec succès !";    
} catch (PDOException $e) {
    echo "Erreur dans l'INSERT d'une nouvelle association user <-> logiciel_systeme : " . $e->getMessage();
}