<?php

try {
	$sql_id_demarche = "DELETE FROM TBLassoc_demarche_user 
	WHERE id_user = :id_user
	AND id_demarche = :id_demarche";
	$req_id_demarche = $bdd->prepare($sql_id_demarche);
	$req_id_demarche->execute([
	':id_user' => $id_user,
	':id_demarche' => $id_demarche,
	]);
	echo "Logiciels & Outils mis à jour avec succès !";    
} catch (PDOException $e) {
	echo "Erreur dans l'INSERT d'une nouvelle association user <-> demarche : " . $e->getMessage();
}