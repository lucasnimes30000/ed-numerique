<?php

try {
	$sql_demarche = "   SELECT DISTINCT TBLdemarche.nom_demarche, TBLdemarche.id_demarche 
						FROM TBLassoc_demarche_user 
						INNER JOIN TBLdemarche 
						ON TBLassoc_demarche_user.id_demarche = TBLdemarche.id_demarche 
						INNER JOIN TBLuser 
						ON TBLassoc_demarche_user.id_user = TBLuser.id_user 
						WHERE TBLuser.id_user = :id_user 
						ORDER BY lower(nom_demarche);";
	$req_demarche = $bdd->prepare($sql_demarche);
	$req_demarche->execute([
		':id_user' => $id_user
	]);
	$resultat = $req_demarche->fetchAll();

	echo "<section class='specialite_individuel' id='demarche_specialite'>";
	echo "<h4 class='titre_specialite'>Ses démarches administratives</h4>";

	foreach ($resultat as $row) {

		foreach ($resultat_demarche as $row_user) {
			if ($row_user['id_demarche'] === $row['id_demarche']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='demarche_checkbox'>
		<input " . htmlspecialchars($checked, ENT_QUOTES) . "id='demarche_" . $b . "' value='" . htmlspecialchars($row['id_demarche'], ENT_QUOTES) . "' name='demarche_" . $b . "_spe' type='checkbox' class='option_demarche_specialite option_specialite'><label for='demarche_" . $b . "_spe'>(id: " . htmlspecialchars($row['id_demarche'], ENT_QUOTES) . ") " . htmlspecialchars($row['nom_demarche'], ENT_QUOTES) . "</label></article>";

		$checked = "";
		$b++;
	}

	echo "<button type='button' class='btn_specialite btn' id='btn_update_specialite_demarche'>Confirmer</button>";
	echo "</section>";
} catch (PDOException $e) {

	echo "Erreur dans le chargement des demarches : " . $e->getMessage();
}
