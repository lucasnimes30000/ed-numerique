<?php

try {
	$sql = "SELECT DISTINCT mois, annee, nbr_click_mail 
				FROM TBLclick_mail 
				WHERE id_user = :id_user
				AND annee = :annee
				AND mois = :mois";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':annee' => $annee,
		':mois' => $mois
	]);
	$resultat = $req->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans la vérification des clicks mail : " . $e->getMessage();
	
}