<?php

try {
    $sql_demarche = "DELETE FROM TBLdemarche WHERE id_demarche = :id_demarche";
    $req_demarche = $bdd->prepare($sql_demarche);
    $req_demarche->execute([
    ':id_demarche' => $id_demarche
]);

    echo "demarche supprimé avec succès !";
} catch (PDOException $e) {
    echo "Failed to create demarche: " . $e->getMessage();
}