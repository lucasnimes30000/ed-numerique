<?php

try {
	$sql_id_spe_infrastructure = "UPDATE TBLassoc_infrastructure_user SET specialite = 1 WHERE id_user = :id_user AND id_infrastructure = :id_infrastructure";
	$req_id_infrastructure = $bdd->prepare($sql_id_spe_infrastructure);
	$req_id_infrastructure->execute([
	':id_user' => $id_user,
	':id_infrastructure' => $id_infrastructure,
]);

	echo "infrastructures mis à jour avec succès";
} catch (PDOException $e) {
	echo "Erreur dans l'INSERT d'une nouvelle association user <-> infrastructure : " . $e->getMessage();
}