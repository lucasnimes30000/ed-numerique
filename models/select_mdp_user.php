<?php

// Vérification si l'ancien password renseigné est le bon (permet une authentification indirectement)
try {

	$sql_old = "SELECT mot_de_passe FROM TBLuser WHERE id_user = :id_user;";
	$req = $bdd->prepare($sql_old);
	$req->execute([
		':id_user' => $id_user
	]);

	$resultat_old = $req->fetch();

} catch (PDOException $e) {

	echo "Erreur dans la vérification de l'ancien mot de passe : " . $e->getMessage();

}