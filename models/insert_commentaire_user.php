<?php

try {
	$sql_select = "INSERT INTO TBLpedagogie (id_user, commentaire) VALUES (:id_user, :commentaire)";
	$req_select = $bdd->prepare($sql_select);
	$req_select->execute([
		':id_user' => $id_user,
		':commentaire' => $commentaire
	]);
	header('Location: ../views/profil_ed_backoffice.php?id_user=' . $id_user . '#updateok');
} catch (PDOException $e) {
	echo "Erreur dans la création du commentaire de l'utilisateur : " . $e->getMessage();
}        