<?php

try {

$sql_ville = "SELECT id_ville, nom_ville FROM `TBLville` WHERE statut_vignette = 1 ORDER BY lower(nom_ville) ";
$req_ville = $bdd->prepare($sql_ville);
$req_ville->execute();
$resultat = $req_ville->fetchAll();

foreach ($resultat as $row) { 
	echo "<a href='?villechoix&id_ville=" . htmlentities($row['id_ville'], ENT_QUOTES) . "&ville=" . htmlentities($row['nom_ville'], ENT_QUOTES) . "'>" . htmlentities(mb_strtoupper($row['nom_ville']), ENT_QUOTES) . "</a>";
} 

} catch (PDOException $e) {

echo "Failed to load ville: " . $e->getMessage();

}