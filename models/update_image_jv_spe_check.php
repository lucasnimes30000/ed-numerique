<?php

try {
	$sql = "UPDATE TBLassoc_image_jv_user SET specialite = 1 WHERE id_user = :id_user AND id_image_jv = :id_image_jv";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_image_jv' => $id_image_jv
	]);
	
	echo "Spécialités Image & Jv. mis à jour avec succès !";
} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}