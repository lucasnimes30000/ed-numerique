
<?php
try {
	$sql_demarche = "SELECT id_demarche, nom_demarche FROM TBLdemarche ORDER BY lower(nom_demarche);";
	$req_demarche = $bdd->prepare($sql_demarche);
	$req_demarche->execute();
	$resultat = $req_demarche->fetchAll();

	foreach ($resultat as $row) {

		foreach ($resultat_demarche as $row_user) {
			if ($row_user['id_demarche'] === $row['id_demarche']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='demarche_checkbox'>
	
		<input " . htmlentities($checked, ENT_QUOTES) . "id='demarche_" . $b . "' value='" . htmlentities($row['id_demarche'], ENT_QUOTES) . "' name='demarche_" . $b . "' type='checkbox' class='option_demarche'><label for='demarche_" . $b . "'>(id: " . htmlentities($row['id_demarche'], ENT_QUOTES) . ") " . htmlentities($row['nom_demarche'], ENT_QUOTES) . "</label></article>";

		$checked = "";
		$b++;
	}

} catch (PDOException $e) {

	echo "Erreur dans le chargement des demarches : " . $e->getMessage();
}
