<?php

try {

	$sql_select_demarche_user ="SELECT id_demarche, id_user	FROM TBLassoc_demarche_user	WHERE id_user = :id_user AND id_demarche = :id_demarche";
	$req = $bdd->prepare($sql_select_demarche_user);
	$req->execute([
		':id_user' => $id_user,
		':id_demarche' => $id_demarche    
	]);

	$resultat_select_demarche_user = $req->fetchAll();

} catch (PDOException $e) {

	echo "Erreur lors de la recherche d'un demarche <> user : " . $e->getMessage();

}
