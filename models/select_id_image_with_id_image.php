<?php

try {

    $sql_select_image_jv_user ="SELECT id_image_jv, id_user
    FROM TBLassoc_image_jv_user
    WHERE id_user = :id_user
    AND id_image_jv = :id_image_jv";
    $req = $bdd->prepare($sql_select_image_jv_user);
    $req->execute([
        ':id_user' => $id_user,
        ':id_image_jv' => $id_image_jv    
    ]);

    $resultat_select_image_jv_user = $req->fetchAll();

} catch (PDOException $e) {

echo "Erreur lors de la recherche d'un image_jv <> user : " . $e->getMessage();

}