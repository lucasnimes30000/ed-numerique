<?php

try {
	$sql = "SELECT id_assoc_demarche_user FROM TBLassoc_demarche_user WHERE id_user = :id_user AND id_demarche = :id_demarche";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_demarche' => $id_demarche
	]);
	$resultat = $req->fetchAll();

} catch (PDOException $e) {

	echo "Erreur au moment de la vérification d'attribution de la spécialité : " . $e->getMessage();
}