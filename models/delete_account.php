<?php

try {
	$sql_select =  "SELECT * FROM TBLassoc_fiche_ed
		WHERE id_user = :id_user";

	$req_select = $bdd->prepare($sql_select);
	$req_select->execute([
		':id_user' => $id_user
	]);

	$fiche_ed = $req_select->fetch();
	$id_fiche_ed = $fiche_ed["id_fiche_ed"];

} catch (PDOException $e) {

	echo "Erreur dans la vérification des users en lien avec l'utilisateur : " . $e->getMessage();
	
}


try {
	$sql_delete =  "DELETE FROM `TBLassoc_demarche_user` WHERE `id_user` = :id_user;
					DELETE FROM `TBLassoc_fiche_ed` WHERE `id_user` = :id_user;
					DELETE FROM `TBLassoc_image_jv_user` WHERE `id_user` = :id_user;
					DELETE FROM `TBLassoc_infrastructure_user` WHERE `id_user` = :id_user;
					DELETE FROM `TBLassoc_logiciel_systeme_user` WHERE `id_user` = :id_user;
					DELETE FROM `TBLassoc_materiel_user` WHERE `id_user` = :id_user;
					DELETE FROM `TBLassoc_statut_user` WHERE `id_user` = :id_user;
					DELETE FROM `TBLassoc_ville_user` WHERE `id_user` = :id_user;
					DELETE FROM `TBLclick_mail` WHERE `id_user` = :id_user;
					DELETE FROM `TBLclick_profil` WHERE `id_user` = :id_user;
					DELETE FROM `TBLclick_telephone` WHERE `id_user` = :id_user;
					DELETE FROM `TBLcontrat` WHERE `id_user` = :id_user;
					DELETE FROM `TBLpedagogie` WHERE `id_user` = :id_user;
					DELETE FROM `TBLfiche_ed` WHERE `id_fiche_ed` = :id_fiche_ed;
					DELETE FROM `TBLuser` WHERE `id_user` = :id_user;";


	$req_delete = $bdd->prepare($sql_delete);
	$req_delete->execute(array(
		':id_user' => $id_user,
		':id_fiche_ed' => $id_fiche_ed
	));

	$dir = "../views/assets/user/$id_user";
	$delete = delTree($dir);
	header("Location: ../views/display_ed_annuaire.php#DeleteOK");

} catch (PDOException $e) {

	echo "Erreur dans la vérification des users en lien avec l'utilisateur : " . $e->getMessage();
	
}

function delTree($dir) {
	$files = array_diff(scandir($dir), array('.','..'));
	 foreach ($files as $file) {
	   (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
	 }
	 return rmdir($dir);
   }