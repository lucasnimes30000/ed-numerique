<?php

try {

	$sql_select_infrastructure_user = "SELECT id_infrastructure, id_user
	FROM TBLassoc_infrastructure_user
	WHERE id_user = :id_user
	AND id_infrastructure = :id_infrastructure";
	$req = $bdd->prepare($sql_select_infrastructure_user);
	$req->execute([
		':id_user' => $id_user,
		':id_infrastructure' => $id_infrastructure    
	]);

	$resultat_select_infrastructure_user = $req->fetchAll();

} catch (PDOException $e) {

	echo "Erreur lors de la recherche d'un infrastructure <> user : " . $e->getMessage();

}