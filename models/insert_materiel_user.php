<?php

try {
	$sql_materiel = "INSERT INTO TBLassoc_materiel_user(id_materiel, id_user, specialite) VALUES (:id_materiel, :id_user, 0)";
	$req_materiel = $bdd->prepare($sql_materiel);
	$req_materiel->execute([
		':id_user' => $id_user,
		':id_materiel' => $id_materiel
	]);

	echo "Materiaux mis à jour avec succès !";    
} catch (PDOException $e) {

	echo "Failed to create materiel: " . $e->getMessage();

}
