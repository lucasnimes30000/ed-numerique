<?php

try {
    $sql_logiciel_systeme = "DELETE FROM TBLlogiciel_systeme WHERE id_logiciel_systeme = :id_logiciel_systeme";
    $req_logiciel_systeme = $bdd->prepare($sql_logiciel_systeme);
    $req_logiciel_systeme->execute([
    ':id_logiciel_systeme' => $id_logiciel_systeme
]);

    echo "logiciel_systeme supprimé avec succès !";
    
} catch (PDOException $e) {
	echo "Failed to create logiciel_systeme: " . $e->getMessage();
}