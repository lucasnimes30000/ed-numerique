<?php

try {
    $sql_id_statut = "DELETE FROM TBLassoc_statut_user 
    WHERE id_user = :id_user
    AND id_statut = :id_statut";
    $req_id_statut = $bdd->prepare($sql_id_statut);
    $req_id_statut->execute([
    ':id_user' => $id_user,
    ':id_statut' => $id_statut,
]);
echo "Statuts mis à jour avec succès";
} catch (PDOException $e) {
    echo "Erreur dans l'INSERT d'une nouvelle association user <-> statut : " . $e->getMessage();
}