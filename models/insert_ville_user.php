<?php

try {
	$sql_ville = "INSERT INTO TBLassoc_ville_user(id_ville, id_user) VALUES(:id_ville, :id_user)";

	$req_ville = $bdd->prepare($sql_ville);
	$req_ville->execute([
	':id_ville' => $id_ville,
	':id_user' => $id_user
]);

	echo "Ville attribuée avec succès";
} catch (PDOException $e) {
	echo "Erreur lors de l'attribuation de cette ville à l'utilisateur, message à transmettre au développeur : " . $e->getMessage();
}