<?php

try {
	$sql = "INSERT INTO TBLassoc_materiel_user(id_materiel, id_user, specialite) VALUES (:id_materiel, :id_user, 1)";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_materiel' => $id_materiel
	]);

} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}