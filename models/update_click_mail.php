<?php

try {
	$sql = "UPDATE TBLclick_mail SET nbr_click_mail = :nbr_click_mail WHERE id_user = :id_user AND annee = :annee AND mois = :mois";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':annee' => $annee,
		':mois' => $mois,
		':nbr_click_mail' => $nbr_click_plus_un
	]);
	$resultat = $req->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans l'update des clicks mail : " . $e->getMessage();
	
}