<?php


try {
	$sql_image_jv = "   SELECT DISTINCT TBLimage_jv.nom_image_jv, TBLimage_jv.id_image_jv 
						FROM TBLassoc_image_jv_user 
						INNER JOIN TBLimage_jv 
						ON TBLassoc_image_jv_user.id_image_jv = TBLimage_jv.id_image_jv 
						INNER JOIN TBLuser 
						ON TBLassoc_image_jv_user.id_user = TBLuser.id_user 
						WHERE TBLuser.id_user = :id_user 
						ORDER BY lower(nom_image_jv);";
	$req_image_jv = $bdd->prepare($sql_image_jv);
	$req_image_jv->execute([
		':id_user' => $id_user
	]);
	$resultat = $req_image_jv->fetchAll();

	echo "<section class='specialite_individuel' id='image_jv_specialite'>";
	echo "<h4 class='titre_specialite'>Image, Son et Jeux</h4>";

	foreach ($resultat as $row) {

		foreach ($resultat_image_jv as $row_user) {
			// Même fonctionnement mais avec les specialités
			if ($row_user['id_image_jv'] === $row['id_image_jv']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='image_jv_checkbox'>


		<input " . htmlspecialchars($checked, ENT_QUOTES) . "id='image_jv_" . $b . "' value='" . htmlspecialchars($row['id_image_jv'], ENT_QUOTES) . "' name='image_jv_" . $b . "_spe' type='checkbox' class='option_image_jv_specialite option_specialite'><label for='image_jv_" . $b . "_spe'>(id: " . htmlspecialchars($row['id_image_jv'], ENT_QUOTES) . ") " . htmlspecialchars($row['nom_image_jv'], ENT_QUOTES) . "</label>
		
		
		</article>";

		$checked = "";
		$b++;
	}

	echo "<button type='button' class='btn_specialite btn' id='btn_update_specialite_image_jv'>Confirmer</button>";
	echo "</section>";
} catch (PDOException $e) {

	echo "Erreur dans le chargement des image_jvs : " . $e->getMessage();

}
