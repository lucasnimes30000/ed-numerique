<?php

try {
	$sql = "INSERT INTO TBLassoc_logiciel_systeme_user(id_logiciel_systeme, id_user, specialite) VALUES (:id_logiciel_systeme, :id_user, 1)";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_logiciel_systeme' => $id_logiciel_systeme
	]);

} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}