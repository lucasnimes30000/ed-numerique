<?php

try {
	$sql = "UPDATE TBLassoc_logiciel_systeme_user SET specialite = 1 WHERE id_user = :id_user AND id_logiciel_systeme = :id_logiciel_systeme";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_logiciel_systeme' => $id_logiciel_systeme
	]);
	
	echo "Spécialités Logiciels & Systemes mis à jour avec succès !";
} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}