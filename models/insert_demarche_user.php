<?php

try {
	$sql_id_demarche = "INSERT INTO TBLassoc_demarche_user(id_user, id_demarche, specialite) VALUES (:id_user, :id_demarche, 0)";
	$req_id_demarche = $bdd->prepare($sql_id_demarche);
	$req_id_demarche->execute([
	':id_user' => $id_user,
	':id_demarche' => $id_demarche,
]);

	echo "demarches mis à jour avec succès";
} catch (PDOException $e) {
	echo "Erreur dans l'INSERT d'une nouvelle association user <-> demarche : " . $e->getMessage();
}
