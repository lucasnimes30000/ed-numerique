<?php

try {
	$sql_statut = "SELECT DISTINCT TBLstatut.nom_statut, TBLstatut.id_statut 
				FROM TBLassoc_statut_user 
				INNER JOIN TBLstatut 
				ON TBLassoc_statut_user.id_statut = TBLstatut.id_statut 
				INNER JOIN TBLuser 
				ON TBLassoc_statut_user.id_user = TBLuser.id_user 
				WHERE TBLuser.id_user = :id_user";

	$req_statut = $bdd->prepare($sql_statut);
	$req_statut->execute([
		':id_user' => $id_user
	]);
	$resultat_statut = $req_statut->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans la vérification des statuts en lien avec l'utilisateur : " . $e->getMessage();
	
}