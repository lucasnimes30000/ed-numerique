<?php

try {
    $sql_fiche = "SELECT * FROM TBLuser WHERE id_user = :id_user";

    $req_fiche = $bdd->prepare($sql_fiche);
    $req_fiche->execute([
        ':id_user' => $id_user
    ]);

    $user = $req_fiche->fetch();
    
} catch (PDOException $e) {

    echo "Erreur lors de la vérification du statut du compte de l'utilisateur : " . $e->getMessage();
    
}