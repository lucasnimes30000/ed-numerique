<?php

try {

	$sql_select_logiciel_systeme_user ="SELECT id_logiciel_systeme, id_user
	FROM TBLassoc_logiciel_systeme_user
	WHERE id_user = :id_user
	AND id_logiciel_systeme = :id_logiciel_systeme";
	$req = $bdd->prepare($sql_select_logiciel_systeme_user);
	$req->execute([
		':id_user' => $id_user,
		':id_logiciel_systeme' => $id_logiciel_systeme    
	]);

	$resultat_select_logiciel_systeme_user = $req->fetchAll();

} catch (PDOException $e) {

	echo "Erreur lors de la recherche d'un logiciel_systeme <> user : " . $e->getMessage();

}
