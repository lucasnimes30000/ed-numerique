<?php

try {
	$sql = "INSERT INTO TBLassoc_demarche_user(id_demarche, id_user, specialite) VALUES (:id_demarche, :id_user, 1)";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_demarche' => $id_demarche
	]);

} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}