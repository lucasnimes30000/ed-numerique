<?php

try {
    $sql_materiel = "DELETE FROM TBLmateriel WHERE id_materiel = :id_materiel";
    $req_materiel = $bdd->prepare($sql_materiel);
    $req_materiel->execute([
    ':id_materiel' => $id_materiel
]);

    echo "materiel supprimé avec succès !";
} catch (PDOException $e) {
    echo "Failed to create materiel: " . $e->getMessage();
}