<?php


try {
	$sql = "SELECT DISTINCT TBLmateriel.nom_materiel
			FROM TBLassoc_materiel_user 
			INNER JOIN TBLmateriel 
			ON TBLassoc_materiel_user.id_materiel = TBLmateriel.id_materiel 
			INNER JOIN TBLuser 
			ON TBLassoc_materiel_user.id_user = TBLuser.id_user 
			WHERE TBLuser.id_user = :id_user
			AND TBLassoc_materiel_user.specialite = 1 COLLATE utf8mb4_unicode_ci";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user
	]);
	$resultat = $req->fetchAll();

	if(!empty($resultat)) {

		foreach($resultat as $key => $value) {
			$key_check = array_search($value['nom_materiel'], $arr_specialite);

			if($key_check === false) {

				// echo count($arr_specialite);
				switch(count($arr_specialite)){
					case 0:
						$arr_vignette[$i]['spe_1'] = htmlspecialchars($value['nom_materiel'], ENT_QUOTES);
						break;
					case 1:
						$arr_vignette[$i]['spe_2'] = htmlspecialchars($value['nom_materiel'], ENT_QUOTES);
						break;
					case 2:
						$arr_vignette[$i]['spe_3'] = htmlspecialchars($value['nom_materiel'], ENT_QUOTES);
						break;
					case (count($arr_specialite) < 2):
						break;
					default:
						break;
				}
				array_push($arr_specialite, htmlspecialchars($value['nom_materiel']));
			}
		}
	}
} catch (PDOException $e) {
	
	echo "Erreur génération annuaire 92568 : " . $e->getMessage();
	
}
