<?php

    function infosEd($id_user){

        include("controller/id.php");

        try {
            $sql_user = "SELECT TBLuser.url_photo, TBLuser.mail, TBLuser.prenom, TBLuser.nom_user, TBLfiche_ed.savoir_faire, TBLfiche_ed.j_aime, TBLfiche_ed.me_connaitre, TBLfiche_ed.info_complementaire
                        FROM TBLfiche_ed
                        INNER JOIN TBLassoc_fiche_ed
                            ON TBLfiche_ed.id_fiche_ed = TBLassoc_fiche_ed.id_fiche_ed
                        INNER JOIN TBLuser
                            ON TBLuser.id_user = TBLassoc_fiche_ed.id_user
                        WHERE TBLuser.id_user = :id_user";
            $req_user = $bdd->prepare($sql_user);
            $req_user->execute(array(
                ':id_user' => $id_user
            ));

            $info_perso = $req_user->fetch();

            return $info_perso;

        } catch (PDOException $e) {
            echo "Failed to select informations: " . $e->getMessage();
        }
    }

    function commentaire($id_user){
        include("controller/id.php");

        try {
            $sql_user = "SELECT * FROM TBLcommentaire
                WHERE id_user = :id_user AND statut_com = 1";
            $req_user = $bdd->prepare($sql_user);
            $req_user->execute(array(
                ':id_user' => $id_user
            ));

            return $req_user->fetchAll();

        } catch (PDOException $e) {
            var_dump($id_user);
            echo "Failed to select informations: " . $e->getMessage();
        }
    }