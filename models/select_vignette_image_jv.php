<?php

try {
	$sql = "SELECT DISTINCT TBLimage_jv.nom_image_jv
			FROM TBLassoc_image_jv_user 
			INNER JOIN TBLimage_jv 
			ON TBLassoc_image_jv_user.id_image_jv = TBLimage_jv.id_image_jv 
			INNER JOIN TBLuser 
			ON TBLassoc_image_jv_user.id_user = TBLuser.id_user 
			WHERE TBLuser.id_user = :id_user
			AND TBLassoc_image_jv_user.specialite = 1";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user
	]);
	$resultat_image = $req->fetchAll();

	if(!empty($resultat_image)) {


		foreach($resultat_image as $key => $value) {
			if(isset($value['nom_logiciel_systeme'])) $key_check = array_search($value['nom_logiciel_systeme'], $arr_specialite);
			
			if($key_check === false) {
				switch(count($arr_specialite)){
					case 0:
						$arr_vignette[$i]['spe_1'] = htmlspecialchars($value['nom_image_jv'], ENT_QUOTES);
						break;
					case 1:
						$arr_vignette[$i]['spe_2'] = htmlspecialchars($value['nom_image_jv'], ENT_QUOTES);
						break;
					case 2:
						$arr_vignette[$i]['spe_3'] = htmlspecialchars($value['nom_image_jv'], ENT_QUOTES);
						break;
					case (count($arr_specialite) < 2):
						break;
					default:
						break;
				}
				array_push($arr_specialite, $value['nom_image_jv']);
			}
		}
	}
} catch (PDOException $e) {
	
	echo "Erreur génération annuaire 92568 : " . $e->getMessage();
	
}
