<?php

try {
	$sql_materiel = "INSERT INTO TBLmateriel(nom_materiel) VALUES (:nom_materiel)";
	$req_materiel = $bdd->prepare($sql_materiel);
	$req_materiel->execute([
	':nom_materiel' => $nom_materiel
	]);

	echo "Nouveau materiel créé avec succès !";
} catch (PDOException $e) {
	echo "Failed to create materiel: " . $e->getMessage();
}