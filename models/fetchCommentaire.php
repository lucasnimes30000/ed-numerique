<?php
    include("../controller/id.php");

    if(isset($_GET['id_user'])){
        $id_user = $_GET['id_user'];
        $statut_com = $_GET['statut_com'];

        try {
            $sql_com = "SELECT * FROM TBLcommentaire
                WHERE id_user = :id_user AND statut_com = :statut_com";
            $req_com = $bdd->prepare($sql_com);
            $req_com->execute(array(
                ":id_user" => $id_user,
                ":statut_com" => $statut_com
            ));
    
            $commentaire = $req_com->fetchAll();
            echo json_encode($commentaire);
    
        } catch (PDOException $e) {
            echo "Failed to select informations: " . $e->getMessage();
        }
        
    }elseif(isset($_GET["statut_com"])){
        $statut_com = $_GET['statut_com'];

        try {
            $sql_com = "SELECT * FROM TBLcommentaire
                INNER JOIN TBLuser ON TBLcommentaire.id_user = TBLuser.id_user
                WHERE statut_com = :statut_com";
            $req_com = $bdd->prepare($sql_com);
            $req_com->execute(array(
                ":statut_com" => $statut_com
            ));
    
            $commentaire = $req_com->fetchAll();
            echo json_encode($commentaire);
    
        } catch (PDOException $e) {
            echo "Failed to select informations: " . $e->getMessage();
        }

    } elseif(isset($_GET["nom_com"])){
        $statut_com = $_GET['nom_com'];

        try {
            $sql_com = "SELECT * FROM TBLcommentaire
                INNER JOIN TBLuser ON TBLcommentaire.id_user = TBLuser.id_user
                WHERE nom_com LIKE :nom_com OR prenom_com LIKE :prenom_com";
            $req_com = $bdd->prepare($sql_com);
            $req_com->execute(array(
                ":nom_com" => '%' . $statut_com . '%',
                ":prenom_com" => '%' . $statut_com . '%'
            ));
    
            $commentaire = $req_com->fetchAll();
            echo json_encode($commentaire);
    
        } catch (PDOException $e) {
            echo "Failed to select informations: " . $e->getMessage();
        }
    }elseif(isset($_GET["nom_ed"])){
        $statut_com = $_GET['nom_ed'];

        try {
            $sql_com = "SELECT * FROM TBLcommentaire
                INNER JOIN TBLuser ON TBLcommentaire.id_user = TBLuser.id_user
                WHERE nom_user LIKE :nom OR prenom LIKE :prenom";
            $req_com = $bdd->prepare($sql_com);
            $req_com->execute(array(
                ":nom" => '%' . $statut_com . '%',
                ":prenom" => '%' . $statut_com . '%'
            ));
    
            $commentaire = $req_com->fetchAll();
            echo json_encode($commentaire);
    
        } catch (PDOException $e) {
            echo "Failed to select informations: " . $e->getMessage();
        }
    }else{
        try {
            $sql_com = "SELECT * FROM TBLcommentaire
                INNER JOIN TBLuser ON TBLcommentaire.id_user = TBLuser.id_user";
            $req_com = $bdd->prepare($sql_com);
            $req_com->execute();
    
            $commentaire = $req_com->fetchAll();
            echo json_encode($commentaire);
    
        } catch (PDOException $e) {
            echo "Failed to select informations: " . $e->getMessage();
        }
    }
?>