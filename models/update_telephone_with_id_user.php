<?php

try {
    $sql_id_telephone = "UPDATE TBLuser SET telephone = :telephone_user WHERE id_user = :id_user";
    $req_id_telephone = $bdd->prepare($sql_id_telephone);
    $req_id_telephone->execute([
    ':id_user' => $id_user,
    ':telephone_user' => $telephone_user,
]);
echo "Numéro de téléphone mis à jour avec succès !";    
} catch (PDOException $e) {
    echo "Erreur dans l'INSERT d'une nouvelle association user <-> adresse telephone : " . $e->getMessage();
}