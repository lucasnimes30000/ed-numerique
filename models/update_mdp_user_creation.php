<?php

try {
	$sql_mdp = "UPDATE TBLuser SET mot_de_passe = :mdp, accord_condition = 1, statut = 1 WHERE id_user = :id_user AND token_oublie = :token";

	$req_mdp = $bdd->prepare($sql_mdp);
	$req_mdp->execute([
		':id_user' => $id_user,
		':token' => $token,
		':mdp' => $password
	]);
 
} catch (PDOException $e) {

	echo "(2) Erreur lors de la création du mot de passe, contactez au plus vite votre administrateur !  " . $e->getMessage();
	header('Location: ../?creation_mdp&token=' . $token . '&id_user=' . $id_user . '#erreur_creation'); 
}