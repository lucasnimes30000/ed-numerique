<?php

try {
	$sql_logiciel_systeme = "   SELECT DISTINCT TBLlogiciel_systeme.nom_logiciel_systeme, TBLlogiciel_systeme.id_logiciel_systeme 
								FROM TBLassoc_logiciel_systeme_user 
								INNER JOIN TBLlogiciel_systeme 
								ON TBLassoc_logiciel_systeme_user.id_logiciel_systeme = TBLlogiciel_systeme.id_logiciel_systeme 
								INNER JOIN TBLuser 
								ON TBLassoc_logiciel_systeme_user.id_user = TBLuser.id_user 
								WHERE TBLuser.id_user = :id_user 
								ORDER BY lower(nom_logiciel_systeme);";
	$req_logiciel_systeme = $bdd->prepare($sql_logiciel_systeme);
	$req_logiciel_systeme->execute([
		':id_user' => $id_user
	]);
	$resultat = $req_logiciel_systeme->fetchAll();

	echo "<section class='specialite_individuel' id='logiciel_systeme_specialite'>";
	echo "<h4 class='titre_specialite'>Ses logiciels et systemes</h4>";

	$checked = "";

	foreach ($resultat as $row) {

		foreach ($resultat_logiciel_systeme as $row_user) {
			if ($row_user['id_logiciel_systeme'] === $row['id_logiciel_systeme']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='logiciel_systeme_checkbox'>
		<input " . htmlspecialchars($checked, ENT_QUOTES) . "id='logiciel_systeme_" . $b . "' value='" . htmlspecialchars($row['id_logiciel_systeme'], ENT_QUOTES) . "' name='logiciel_systeme_" . $b . "_spe' type='checkbox' class='option_logiciel_systeme_specialite option_specialite'><label for='logiciel_systeme_" . $b . "_spe'>(id: " . htmlspecialchars($row['id_logiciel_systeme'], ENT_QUOTES) . ") " . htmlspecialchars($row['nom_logiciel_systeme'], ENT_QUOTES) . "</label></article>";

		$checked = "";
		$b++;
	}

	echo "<button type='button' class='btn_specialite btn' id='btn_update_specialite_logiciel_systeme'>Confirmer</button>";
	echo "</section>";
} catch (PDOException $e) {

	echo "Erreur dans le chargement des logiciel_systemes : " . $e->getMessage();

}
