<?php

try {
	$sql_logiciel_systeme = "SELECT DISTINCT TBLlogiciel_systeme.nom_logiciel_systeme, TBLlogiciel_systeme.id_logiciel_systeme 
				FROM TBLassoc_logiciel_systeme_user 
				INNER JOIN TBLlogiciel_systeme 
				ON TBLassoc_logiciel_systeme_user.id_logiciel_systeme = TBLlogiciel_systeme.id_logiciel_systeme 
				INNER JOIN TBLuser 
				ON TBLassoc_logiciel_systeme_user.id_user = TBLuser.id_user 
				WHERE TBLuser.id_user = :id_user";

	$req_logiciel_systeme = $bdd->prepare($sql_logiciel_systeme);
	$req_logiciel_systeme->execute([
		':id_user' => $id_user
	]);
	$resultat_logiciel_systeme = $req_logiciel_systeme->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans la vérification des logiciel_systemes en lien avec l'utilisateur : " . $e->getMessage();
	
}