<?php

    function checkTokenEtId($id_user, $token){

        include('controller/id.php');

        try{

            $sql_match = "SELECT * FROM TBLuser 
                WHERE id_user = :id_user AND token_oublie = :token";
            $req_match = $bdd->prepare($sql_match);
            $req_match->execute([
                ':id_user' => $id_user,
                ':token' => $token
            ]);

            $test = $req_match->rowCount();
            $match = $req_match->fetch();

            return [$match, $test];

        } catch (PDOException $e) {
            echo "Page inaccessible, les informations d'authentification sont incorrectes. Merci de vérifier avec votre administrateur. " . $e->getMessage();
        }
    }

    function miseAJourMdp($mot_de_passe, $id_user, $token){

        include('controller/id.php');

        $mot_de_passe = password_hash($mot_de_passe, PASSWORD_DEFAULT);

        try{

            $sql_match = "UPDATE TBLuser 
                SET mot_de_passe = :mot_de_passe 
                WHERE token_oublie = :token AND id_user = :id_user";
            $req_match = $bdd->prepare($sql_match);
            $req_match->execute([
                'mot_de_passe' => $mot_de_passe,
                ':id_user' => $id_user,
                ':token' => $token
            ]);

            $test = $req_match->rowCount();

            return $test;

        } catch (PDOException $e) {
            echo "Page inaccessible, les informations d'authentification sont incorrectes. Merci de vérifier avec votre administrateur. " . $e->getMessage();
        }

    }

    function MessageUtilisateur($nom,$prenom,$mail){
        $to = $mail;

        $header = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

        $header .= 'To: ' . $to . "\r\n";
        $header .= 'From: contact@ed-numerique.fr' . "\r\n";

        $sujet = "Ed numérique - votre mot de passe a été modifié";

        $message = "<html>
                <head>
                    <title>Contact</title>
                </head>
                <body>
                    <p>
                        Bonjour " . $nom . ' ' . $prenom . ",<br>
                        La modification de votre mot de passe a bien été prise en compte.<br><br>
                        Si ce n’est pas vous qui êtes à l'origine de la demande de changement de mot de passe, prenez contact immédiatement avec nous (contact@ed-numerique.fr)<br><br>
                        L'équipe <b>Ed numérique<b>.
                    </p>
                </body>
            </html>";

            return mail($to, $sujet, $message, $header);
    }