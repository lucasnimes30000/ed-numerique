<?php

try {
    $sql_select_prenom_user ="SELECT prenom
    FROM TBLuser
    WHERE id_user = :id_user";
    $req = $bdd->prepare($sql_select_prenom_user);
    $req->execute([
        ':id_user' => $id_user,
    ]);

    $resultat_select_prenom_user = $req->fetchAll();

} catch (PDOException $e) {

echo "Erreur lors de la recherche d'un prénom <> user : " . $e->getMessage();

}