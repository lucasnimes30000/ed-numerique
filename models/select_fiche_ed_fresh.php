<?php

try {
	$sql_select_id = "SELECT * FROM TBLfiche_ed WHERE id_fiche_ed = (SELECT max(id_fiche_ed) FROM TBLfiche_ed)";
	$req_select_id = $bdd->prepare($sql_select_id);
	$req_select_id->execute();
	$id_fiche_create = $req_select_id->fetchAll();

} catch (PDOException $e) {
	echo "Erreur lors de la vérification de l'existance de l'identifiant de la fiche Ed demandée : " . $e->getMessage();
} 