<?php

try {
	$sql = "UPDATE TBLclick_telephone SET nbr_click_telephone = :nbr_click_telephone WHERE id_user = :id_user AND annee = :annee AND mois = :mois";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':annee' => $annee,
		':mois' => $mois,
		':nbr_click_telephone' => $nbr_click_plus_un
	]);
	$resultat = $req->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans l'update des clicks telephone : " . $e->getMessage();
	
}