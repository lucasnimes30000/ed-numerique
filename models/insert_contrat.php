<?php

try{
    $sql_match = "INSERT INTO TBLcontrat(nom_contrat, chemin, id_user, statut) VALUES (:nom_contrat, :filenameDB, :id_user, 0)";
    $req_match = $bdd->prepare($sql_match);
    $req_match->execute([
        ':filenameDB' => (string) $filename,
        ':id_user' => (int) $id_user,
        ':nom_contrat' => (string) $filename
    ]);


    header('Location: ../views/profil_ed_backoffice.php?id_user=' . $id_user);
    exit;
} catch (PDOException $e) {
    echo "Erreur dans l'insert de l'image dans la DataBase " . $e->getMessage();
}