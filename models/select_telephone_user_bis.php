<?php

try {

	$sql_delete = "SELECT telephone FROM TBLuser WHERE id_user = :id_user";
	$req = $bdd->prepare($sql_delete);
	$req->execute([
		':id_user' => $id_user
	]);
	$resultat = $req->fetch();

	$telephone = htmlspecialchars($resultat['telephone'], ENT_QUOTES);

	$un = substr($telephone, 0, 2);
	$deux = substr($telephone, 2, 2);
	$trois = substr($telephone, 4, 2);
	$quatre = substr($telephone, 6, 2);
	$cinq = substr($telephone, 8, 2);

	$telephone = $un . '.' . $deux . '.' . $trois . '.' . $quatre . '.' . $cinq;

	echo "<b style='font-size:1.2rem'>" . $telephone . "</b>";

} catch (PDOException $e) {

	echo "Failed to delete user's ville : " . $e->getMessage();

}
