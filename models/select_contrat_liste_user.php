<?php 

try {


$sql_contrat = "SELECT DISTINCT id_contrat, date_ajout, nom_contrat FROM TBLcontrat WHERE id_user = :id_user
			ORDER BY lower(nom_contrat)";

$req_contrat = $bdd->prepare($sql_contrat);
$req_contrat->execute([
	':id_user' => $id_user
]);
$resultat_contrat = $req_contrat->fetchAll();

foreach ($resultat_contrat as $row) { ?>
	<tr>
		<td class="row"><?= htmlentities($row['date_ajout'], ENT_QUOTES) ?></td>
		<td class="row"><?= htmlentities($row['nom_contrat'], ENT_QUOTES); ?></td>
		<section>
			<input class="id_contrat" type="hidden" name="id_contrat" value="<?php echo htmlentities($row['id_contrat'], ENT_QUOTES); ?>">
			<td><a href= <?php echo "../views/assets/user/" .  $id_user  . "/contrat/" . htmlentities($row['nom_contrat'], ENT_QUOTES) ?> download>Télécharger</a></td>
		</section>
		<section>
			<input class="id_contrat" type="hidden" name="id_contrat" value="<?php echo htmlentities($row['id_contrat'], ENT_QUOTES); ?>">
			<td><button value="<?php echo htmlentities($row['id_contrat'], ENT_QUOTES); ?>" class="BTN_delete_contrat" type="button">🗑️</button></td>
		</section>
	</tr>

<?php } 

} catch (PDOException $e) {

echo "Failed to load user choices " . $e->getMessage();

}