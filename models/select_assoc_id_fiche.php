<?php

try {
	$sql_select = "SELECT id_fiche_ed FROM TBLassoc_fiche_ed WHERE id_user = :id_user";
	$req_select = $bdd->prepare($sql_select);
	$req_select->execute([
	':id_user' => $id_user
]);
	$resultat_assoc = $req_select->fetchAll();
	$id_fiche = htmlspecialchars($resultat_assoc[0]['id_fiche_ed'], ENT_QUOTES);
} catch (PDOException $e) {
	echo "Erreur dans la vérification de l'existance d'une fiche ed associée : " . $e->getMessage();
}