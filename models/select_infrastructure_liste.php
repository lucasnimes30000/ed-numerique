<?php

try {
	$sql_infrastructure = "SELECT id_infrastructure, nom_infrastructure FROM TBLinfrastructure ORDER BY lower(nom_infrastructure);";
	$req_infrastructure = $bdd->prepare($sql_infrastructure);
	$req_infrastructure->execute();
	$resultat = $req_infrastructure->fetchAll();

	foreach ($resultat as $row) {

		foreach ($resultat_infrastructure as $row_user) {
			if ($row_user['id_infrastructure'] === $row['id_infrastructure']) {
				
				echo "<li class='infrastructure'>" . htmlentities($row['nom_infrastructure'], ENT_QUOTES) . "</li>";

			}
		};
	}

} catch (PDOException $e) {

	echo "Erreur dans le chargement des infrastructures : " . $e->getMessage();

}