<?php


try {
	$sql = "SELECT DISTINCT TBLdemarche.nom_demarche
			FROM TBLassoc_demarche_user 
			INNER JOIN TBLdemarche 
			ON TBLassoc_demarche_user.id_demarche = TBLdemarche.id_demarche 
			INNER JOIN TBLuser 
			ON TBLassoc_demarche_user.id_user = TBLuser.id_user 
			WHERE TBLuser.id_user = :id_user
			AND TBLassoc_demarche_user.specialite = 1";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user
	]);
	$resultat = $req->fetchAll();

	if(!empty($resultat)) {

		foreach($resultat as $key => $value) {
			$key_check = array_search($value['nom_demarche'], $arr_specialite);

			if($key_check === false) {

				switch(count($arr_specialite)){
					case 0:
						$arr_vignette[$i]['spe_1'] = htmlspecialchars($value['nom_demarche'], ENT_QUOTES);
						break;
					case 1:
						$arr_vignette[$i]['spe_2'] = htmlspecialchars($value['nom_demarche'], ENT_QUOTES);
						break;
					case 2:
						$arr_vignette[$i]['spe_3'] = htmlspecialchars($value['nom_demarche'], ENT_QUOTES);
						break;
					case (count($arr_specialite) < 2):
						break;
					default:
						break;
				}
				array_push($arr_specialite, $value['nom_demarche']);
			}
		}
	}
} catch (PDOException $e) {
	
	echo "Erreur génération annuaire 92568 : " . $e->getMessage();
	
}