<?php

try {
	$sql = "SELECT id_assoc_image_jv_user FROM TBLassoc_image_jv_user WHERE id_user = :id_user AND id_image_jv = :id_image_jv";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_image_jv' => $id_image_jv
	]);
	$resultat = $req->fetchAll();

} catch (PDOException $e) {

	echo "Erreur au moment de la vérification d'attribution de la spécialité : " . $e->getMessage();
}