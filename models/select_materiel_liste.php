<?php

try {
	$sql_materiel = "SELECT id_materiel, nom_materiel FROM TBLmateriel ORDER BY lower(nom_materiel);";
	$req_materiel = $bdd->prepare($sql_materiel);
	$req_materiel->execute();
	$resultat = $req_materiel->fetchAll();

	foreach ($resultat as $row) {

		foreach ($resultat_materiel as $row_user) {
			if ($row_user['id_materiel'] === $row['id_materiel']) {
				
				echo "<li class='materiel'>" . htmlentities($row['nom_materiel'], ENT_QUOTES) . "</li>";

			}
		};
	}

} catch (PDOException $e) {

	echo "Erreur dans le chargement des materiels : " . $e->getMessage();

}