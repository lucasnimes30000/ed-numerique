<?php


try {
	$sql_infrastructure = " SELECT DISTINCT TBLinfrastructure.nom_infrastructure, TBLinfrastructure.id_infrastructure 
							FROM TBLassoc_infrastructure_user 
							INNER JOIN TBLinfrastructure 
							ON TBLassoc_infrastructure_user.id_infrastructure = TBLinfrastructure.id_infrastructure 
							INNER JOIN TBLuser 
							ON TBLassoc_infrastructure_user.id_user = TBLuser.id_user 
							WHERE TBLuser.id_user = :id_user 
							ORDER BY lower(nom_infrastructure);";
	$req_infrastructure = $bdd->prepare($sql_infrastructure);
	$req_infrastructure->execute([
		':id_user' => $id_user
	]);
	$resultat = $req_infrastructure->fetchAll();

	echo "<section class='specialite_individuel' id='infrastructure_specialite'>";
	echo "<h4 class='titre_specialite'>Ses infrastructures</h4>";

	foreach ($resultat as $row) {

		foreach ($resultat_infrastructure as $row_user) {
			if ($row_user['id_infrastructure'] === $row['id_infrastructure']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='infrastructure_checkbox'>       
		
		<input " . htmlspecialchars($checked, ENT_QUOTES) . "id='infrastructure_" . $b . "' value='" . htmlspecialchars($row['id_infrastructure'], ENT_QUOTES) . "' name='infrastructure_" . $b . "_spe' type='checkbox' class='option_infrastructure_specialite'><label for='infrastructure_" . $b . "_spe'>(id: " . htmlspecialchars($row['id_infrastructure'], ENT_QUOTES) . ") " . htmlspecialchars($row['nom_infrastructure'], ENT_QUOTES) . "</label></article>";

		$checked = "";
		$b++;
	}

	echo "<button type='button' class='btn_specialite btn' id='btn_update_specialite_infrastructure'>Confirmer</button>";
	echo "</section>";
} catch (PDOException $e) {

	echo "Erreur dans le chargement des infrastructures : " . $e->getMessage();
}
