<?php

try {
	$sql_demarche = "INSERT INTO TBLdemarche(nom_demarche) VALUES (:nom_demarche)";
	$req_demarche = $bdd->prepare($sql_demarche);
	$req_demarche->execute([
':nom_demarche' => $nom_demarche
]);

	echo "Nouveau demarche créé avec succès !";
} catch (PDOException $e) {
	echo "Failed to create demarche: " . $e->getMessage();
}