<?php

try {
    $sql_fiche = "SELECT statut FROM TBLuser WHERE id_user = :id_user";

    $req_fiche = $bdd->prepare($sql_fiche);
    $req_fiche->execute([
        ':id_user' => $id_user
    ]);

    $statut_user = $req_fiche->fetch();
    echo $statut_user['statut'];
    
} catch (PDOException $e) {

    echo "Erreur lors de la vérification du statut du compte de l'utilisateur : " . $e->getMessage();
    
}