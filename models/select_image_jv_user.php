<?php


try {
	$sql_image_jv = "SELECT DISTINCT TBLimage_jv.nom_image_jv, TBLimage_jv.id_image_jv 
				FROM TBLassoc_image_jv_user 
				INNER JOIN TBLimage_jv 
				ON TBLassoc_image_jv_user.id_image_jv = TBLimage_jv.id_image_jv 
				INNER JOIN TBLuser 
				ON TBLassoc_image_jv_user.id_user = TBLuser.id_user 
				WHERE TBLuser.id_user = :id_user";

	$req_image_jv = $bdd->prepare($sql_image_jv);
	$req_image_jv->execute([
		':id_user' => $id_user
	]);
	$resultat_image_jv = $req_image_jv->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans la vérification des image_jvs en lien avec l'utilisateur : " . $e->getMessage();
	
}