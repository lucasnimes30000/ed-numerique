<?php

try {
	$sql_select = "SELECT nom_image_jv FROM TBLimage_jv WHERE nom_image_jv = :nom_image_jv";
	$req_select = $bdd->prepare($sql_select);
	$req_select->execute([
	':nom_image_jv' => $nom_image_jv
]);
	$resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
	echo "Erreur dans la vérification de l'existance d'un image_jv : " . $e->getMessage();
}
