<?php

try {
	$sql_select = "UPDATE TBLville SET statut_vignette = 1 WHERE id_ville = :id_ville";
	$req_select = $bdd->prepare($sql_select);
	$req_select->execute([
		':id_ville' => $id_ville
	]);
	echo "ok";
} catch (PDOException $e) {
	echo "Erreur dans la modification du statut de la ville [ ID : " . $id_ville . " ] : " . $e->getMessage();
}
