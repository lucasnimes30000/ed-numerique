<?php

try {
    $sql_select = "SELECT id_infrastructure FROM TBLinfrastructure WHERE id_infrastructure = :id_infrastructure";
    $req_select = $bdd->prepare($sql_select);
    $req_select->execute([
        ':id_infrastructure' => $id_infrastructure
    ]);
    $resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
    echo "Erreur dans la vérification de l'existance d'un infrastructure : " . $e->getMessage();
}