<?php

include ("id.php");

$id_user = htmlspecialchars($_POST['id_user'], ENT_QUOTES);

// Check if the form was submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if file was uploaded without errors
    if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png", "JPG" => "image/JPG", "JPEG" => "image/JPEG", "PNG" => "image/PNG");
        $filename = htmlspecialchars($_FILES["photo"]["name"], ENT_QUOTES);
        $filetype = htmlspecialchars($_FILES["photo"]["type"], ENT_QUOTES);
        $filesize = htmlspecialchars($_FILES["photo"]["size"], ENT_QUOTES);
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: Merci de sélectionner un format de fichier accepté (jpg, jpeg, png.");
    
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: Poids du fichier plus lourd que ce qui est supporté (5 MB maximum).");
    
        // Verify MYME type of the file
        if(in_array($filetype, $allowed)){
            // Check whether file exists before uploading it
            if(file_exists("../views/assets/user/" . $id_user . "/PP/" . $filename)){
                echo $filename . " existe déjà";
            } else{

                require("../models/select_userimg_with_id.php");

                if($url_photo['url_photo'] !== $filename) {
                    move_uploaded_file($_FILES["photo"]["tmp_name"], "../views/assets/user/" . $id_user . "/PP/" . $filename);

                    unlink("../views/assets/user/" . $id_user . "/PP/" . $url_photo['url_photo']);

                    require("../models/update_userImg.php");
                }

            } 
        } else{
            echo "Error: Il y a eu un problème avec le téléchargement du fichier, merci d'essayer à nouveau."; 
        }
    } else{
        header('Location: ../views/profil_ed_backoffice.php?id_user=' . $id_user . '#error');
    }
}

