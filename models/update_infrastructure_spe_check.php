<?php

try {
	$sql = "UPDATE TBLassoc_infrastructure_user SET specialite = 1 WHERE id_user = :id_user AND id_infrastructure = :id_infrastructure";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_infrastructure' => $id_infrastructure
	]);
	
	echo "Spécialités infrastructure mis à jour avec succès !";
	
} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}