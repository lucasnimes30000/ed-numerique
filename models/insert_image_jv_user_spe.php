<?php

try {
	$sql = "INSERT INTO TBLassoc_image_jv_user(id_image_jv, id_user, specialite) VALUES (:id_image_jv, :id_user, 1)";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_image_jv' => $id_image_jv
	]);

} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}
