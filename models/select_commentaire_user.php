<?php

try {
    $sql_select = "SELECT commentaire FROM TBLpedagogie WHERE id_user = :id_user";
    $req_select = $bdd->prepare($sql_select);
    $req_select->execute([
        ':id_user' => $id_user
    ]);

    $retour = $req_select->fetch();
    echo $retour['commentaire'];
} catch (PDOException $e) {
    echo "Erreur dans la vérification du commentaire de l'utilisateur : " . $e->getMessage();
}
