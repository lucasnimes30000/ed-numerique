<?php

try {
    $sql_statut = "DELETE FROM TBLstatut WHERE id_statut = :id_statut";
    $req_statut = $bdd->prepare($sql_statut);
    $req_statut->execute([
    ':id_statut' => $id_statut
]);

    echo "Statut supprimé avec succès !";
} catch (PDOException $e) {
    echo "Failed to create statut: " . $e->getMessage();
}