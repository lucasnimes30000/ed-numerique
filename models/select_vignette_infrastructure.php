<?php


try {
	$sql = "SELECT DISTINCT TBLinfrastructure.nom_infrastructure
			FROM TBLassoc_infrastructure_user 
			INNER JOIN TBLinfrastructure 
			ON TBLassoc_infrastructure_user.id_infrastructure = TBLinfrastructure.id_infrastructure 
			INNER JOIN TBLuser 
			ON TBLassoc_infrastructure_user.id_user = TBLuser.id_user 
			WHERE TBLuser.id_user = :id_user
			AND TBLassoc_infrastructure_user.specialite = 1";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user
	]);
	$resultat = $req->fetchAll();

	if(!empty($resultat)) {

		foreach($resultat as $key => $value) {
			$key_check = array_search($value['nom_infrastructure'], $arr_specialite);

			if($key_check === false) {

				switch(count($arr_specialite)){
					case 0:
						$arr_vignette[$i]['spe_1'] = htmlspecialchars($value['nom_infrastructure'], ENT_QUOTES);
						break;
					case 1:
						$arr_vignette[$i]['spe_2'] = htmlspecialchars($value['nom_infrastructure'], ENT_QUOTES);
						break;
					case 2:
						$arr_vignette[$i]['spe_3'] = htmlspecialchars($value['nom_infrastructure'], ENT_QUOTES);
						break;
					case (count($arr_specialite) < 2):
						break;
					default:
						break;
				}
				array_push($arr_specialite, $value['nom_infrastructure']);
			}
		}
	}
} catch (PDOException $e) {
	
	echo "Erreur génération annuaire 92568 : " . $e->getMessage();
	
}