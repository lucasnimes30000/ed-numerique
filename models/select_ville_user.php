<?php

try {
	$sql = "SELECT DISTINCT id_user FROM TBLassoc_ville_user WHERE id_ville = :id_ville ORDER BY RAND()";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_ville' => $id_ville
	]);
	$resultat = $req->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans la vérification des infrastructures en lien avec l'utilisateur : " . $e->getMessage();
	
}