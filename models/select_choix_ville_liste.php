<?php
try {

$sql_ville = "SELECT id_ville, nom_ville FROM `TBLville` ORDER BY lower(nom_ville)";
$req_ville = $bdd->prepare($sql_ville);
$req_ville->execute();
$resultat = $req_ville->fetchAll();

foreach ($resultat as $row) { 
	echo "<option class='option_ville'>" . $row['nom_ville'] . "</option>";
} 

} catch (PDOException $e) {

echo "Failed to load ville: " . $e->getMessage();

}