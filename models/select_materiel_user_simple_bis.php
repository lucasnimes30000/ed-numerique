<?php

try {
	$sql = "SELECT id_assoc_materiel_user FROM TBLassoc_materiel_user WHERE id_user = :id_user AND id_materiel = :id_materiel";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_materiel' => $id_materiel
	]);
	$resultat = $req->fetchAll();

} catch (PDOException $e) {

	echo "Erreur au moment de la vérification d'attribution de la spécialité : " . $e->getMessage();
}
