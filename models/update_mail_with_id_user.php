<?php

try {
    $sql_id_adresse_mail = "UPDATE TBLuser SET mail = :adresse_mail_user WHERE id_user = :id_user";
    $req_id_adresse_mail = $bdd->prepare($sql_id_adresse_mail);
    $req_id_adresse_mail->execute([
    ':id_user' => $id_user,
    ':adresse_mail_user' => $adresse_mail_user,
]);
echo "Adresse mail mis à jour avec succès !";    
} catch (PDOException $e) {
    echo "Erreur dans l'INSERT d'une nouvelle association user <-> adresse mail : " . $e->getMessage();
}