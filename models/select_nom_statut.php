<?php

try {
	$sql_select = "SELECT nom_statut FROM TBLstatut WHERE nom_statut = :nom_statut";
	$req_select = $bdd->prepare($sql_select);
	$req_select->execute([
	':nom_statut' => $nom_statut
]);
	$resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
	echo "Erreur dans la vérification de l'existance d'un statut : " . $e->getMessage();
}