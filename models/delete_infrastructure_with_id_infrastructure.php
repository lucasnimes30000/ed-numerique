<?php

try {
    $sql_infrastructure = "DELETE FROM TBLinfrastructure WHERE id_infrastructure = :id_infrastructure";
    $req_infrastructure = $bdd->prepare($sql_infrastructure);
    $req_infrastructure->execute([
    ':id_infrastructure' => $id_infrastructure
]);

    echo "infrastructure supprimé avec succès !";
} catch (PDOException $e) {
    echo "Failed to create infrastructure: " . $e->getMessage();
}