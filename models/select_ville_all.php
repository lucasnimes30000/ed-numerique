<?php

try {

    $sql_ville = "SELECT nom_ville, id_ville, image, statut_vignette FROM TBLville;";
    $req_ville = $bdd->prepare($sql_ville);
    $req_ville->execute();
    $resultat_ville = $req_ville->fetchAll();
    foreach($resultat_ville as $element) {
        echo "<article class='ville_button' id='box_ville_" . $n . "'><img class='img_vignette' src='../" .htmlspecialchars( $element['image'], ENT_QUOTES) . "'>";
        if($element['statut_vignette'] == 1) {
            $phrase = "Retirer " . htmlspecialchars($element['nom_ville'], ENT_QUOTES) . " de l'accueil";
            echo "<button class='btn_ville' type='button' value='" . htmlspecialchars($element['statut_vignette'], ENT_QUOTES) . " " . htmlspecialchars($element['id_ville'], ENT_QUOTES) . "' id='btn_" . htmlspecialchars($element['id_ville'], ENT_QUOTES) . "'>" . $phrase . "</button>";
        } else if($element['statut_vignette'] == 0) {
            $phrase = "Ajouter " . htmlspecialchars($element['nom_ville'], ENT_QUOTES) . " à l'accueil";
            echo "<button class='btn_ville' type='button' value='" . htmlspecialchars($element['statut_vignette'], ENT_QUOTES) . " " . htmlspecialchars($element['id_ville'], ENT_QUOTES) . "' id='btn_" . htmlspecialchars($element['id_ville'], ENT_QUOTES) . "'>" . $phrase . "</button>";
        }
        echo "</article>";
        $n++;
    }
} catch (PDOException $e) {

    echo "Failed to load user choices " . $e->getMessage();
    
}
