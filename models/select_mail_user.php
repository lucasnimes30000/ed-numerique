<?php
try {
	$sql_select_nom_user ="SELECT mail
	FROM TBLuser
	WHERE id_user = :id_user";
	$req = $bdd->prepare($sql_select_nom_user);
	$req->execute([
		':id_user' => $id_user,
	]);

	$resultat_select_nom_user = $req->fetchAll();

	echo htmlentities($resultat_select_nom_user[0]['mail'], ENT_QUOTES);

} catch (PDOException $e) {

echo "Erreur lors de la recherche d'un nom <> user : " . $e->getMessage();

}
