<?php

try {
	$sql_select = "SELECT nom_materiel FROM TBLmateriel WHERE nom_materiel = :nom_materiel";
	$req_select = $bdd->prepare($sql_select);
	$req_select->execute([
	':nom_materiel' => $nom_materiel
	]);
	$resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
	echo "Erreur dans la vérification de l'existance d'un materiel : " . $e->getMessage();
}