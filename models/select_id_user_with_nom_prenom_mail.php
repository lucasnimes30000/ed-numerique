<?php

try {

    $sql_ville ="SELECT id_user FROM TBLuser WHERE nom_user = :nom AND prenom = :prenom AND mail = :mail";
    $req = $bdd->prepare($sql_ville);
    $req->execute([
        ':nom'          => $nom,
        ':prenom'       => $prenom,
        ':mail'         => $mail
    ]);

    $id_user = $req->fetch();
    $id_user = $id_user['id_user'];

    mkdir("../views/assets/user/" . $id_user, 0775);
    mkdir("../views/assets/user/" . $id_user . "/PP", 0775);
    mkdir("../views/assets/user/" . $id_user . "/contrat", 0775);
    mkdir("../views/assets/user/" . $id_user . "/facture", 0775);

    header('Location: ../views/inscription_suite.php?id_user=' . $id_user);
    exit;

} catch (PDOException $e) {

    echo "Erreur lors de la seconde étape de l'inscription : " . $e->getMessage();

}