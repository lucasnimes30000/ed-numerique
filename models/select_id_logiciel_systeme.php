<?php

try {
    $sql_select = "SELECT id_logiciel_systeme FROM TBLlogiciel_systeme WHERE id_logiciel_systeme = :id_logiciel_systeme";
    $req_select = $bdd->prepare($sql_select);
    $req_select->execute([
        ':id_logiciel_systeme' => $id_logiciel_systeme
    ]);
    $resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
    echo "Erreur dans la vérification de l'existance d'un logiciel_systeme : " . $e->getMessage();
}
//kek
