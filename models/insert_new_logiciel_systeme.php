<?php

try {
	$sql_logiciel_systeme = "INSERT INTO TBLlogiciel_systeme(nom_logiciel_systeme) VALUES (:nom_logiciel_systeme)";
	$req_logiciel_systeme = $bdd->prepare($sql_logiciel_systeme);
	$req_logiciel_systeme->execute([
':nom_logiciel_systeme' => $nom_logiciel_systeme
]);

	echo "Nouveau logiciel_systeme créé avec succès !";
} catch (PDOException $e) {
	echo "Failed to create logiciel_systeme: " . $e->getMessage();
}