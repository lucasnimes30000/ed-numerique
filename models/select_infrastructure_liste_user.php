<?php
try {
	$sql_infrastructure = "SELECT id_infrastructure, nom_infrastructure FROM TBLinfrastructure ORDER BY lower(nom_infrastructure);";
	$req_infrastructure = $bdd->prepare($sql_infrastructure);
	$req_infrastructure->execute();
	$resultat = $req_infrastructure->fetchAll();

	foreach ($resultat as $row) {
		$checked = "";

		foreach ($resultat_infrastructure as $row_user) {
			if ($row_user['id_infrastructure'] === $row['id_infrastructure']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='infrastructure_checkbox'>       

		<input " . htmlentities($checked, ENT_QUOTES) . "id='infrastructure_" . $b . "' value='" . htmlentities($row['id_infrastructure'], ENT_QUOTES) . "' name='infrastructure_" . $b . "' type='checkbox' class='option_infrastructure'><label for='infrastructure_" . $b . "'>(id: " . htmlentities($row['id_infrastructure'], ENT_QUOTES) . ") " . htmlentities($row['nom_infrastructure'], ENT_QUOTES) . "</label></article>";

		$b++;
	}
} catch (PDOException $e) {

	echo "Erreur dans le chargement des infrastructures : " . $e->getMessage();

}