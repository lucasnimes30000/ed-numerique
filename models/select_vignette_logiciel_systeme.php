<?php


try {
	$sql = "SELECT DISTINCT TBLlogiciel_systeme.nom_logiciel_systeme
			FROM TBLassoc_logiciel_systeme_user 
			INNER JOIN TBLlogiciel_systeme 
			ON TBLassoc_logiciel_systeme_user.id_logiciel_systeme = TBLlogiciel_systeme.id_logiciel_systeme 
			INNER JOIN TBLuser 
			ON TBLassoc_logiciel_systeme_user.id_user = TBLuser.id_user 
			WHERE TBLuser.id_user = :id_user
			AND TBLassoc_logiciel_systeme_user.specialite = 1";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user
	]);
	$resultat = $req->fetchAll();

	if(!empty($resultat)) {

		foreach($resultat as $key => $value) {
			$key_check = array_search($value['nom_logiciel_systeme'], $arr_specialite);

			if($key_check === false) {

				// echo count($arr_specialite);
				switch(count($arr_specialite)){
					case 0:
						$arr_vignette[$i]['spe_1'] = htmlspecialchars($value['nom_logiciel_systeme'], ENT_QUOTES);
						break;
					case 1:
						$arr_vignette[$i]['spe_2'] = htmlspecialchars($value['nom_logiciel_systeme'], ENT_QUOTES);
						break;
					case 2:
						$arr_vignette[$i]['spe_3'] = htmlspecialchars($value['nom_logiciel_systeme'], ENT_QUOTES);
						break;
					case (count($arr_specialite) < 2):
						break;
					default:
						break;
				}
				array_push($arr_specialite, $value['nom_logiciel_systeme']);
			}
		}
	}
} catch (PDOException $e) {
	
	echo "Erreur génération annuaire 92568 : " . $e->getMessage();
	
}
