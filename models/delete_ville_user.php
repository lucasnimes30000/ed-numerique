<?php

try {

	$sql_delete = "DELETE FROM TBLassoc_ville_user
				   WHERE id_ville = :id_ville
				   AND id_user = :id_user";
	$req = $bdd->prepare($sql_delete);
	$req->execute([
		':id_ville' => $id_ville,
		':id_user' => $id_user
	]);

	echo "Ville supprimée avec succès";

} catch (PDOException $e) {

	echo "Failed to delete user's ville : " . $e->getMessage();

}