<?php

    function infosEd($id_user){

        include("../controller/id.php");

        try {
            $sql_user = "SELECT TBLuser.url_photo, TBLuser.mail, TBLuser.prenom, TBLuser.nom_user, TBLfiche_ed.savoir_faire, TBLfiche_ed.j_aime, TBLfiche_ed.me_connaitre, TBLfiche_ed.info_complementaire
                        FROM TBLfiche_ed
                        INNER JOIN TBLassoc_fiche_ed
                            ON TBLfiche_ed.id_fiche_ed = TBLassoc_fiche_ed.id_fiche_ed
                        INNER JOIN TBLuser
                            ON TBLuser.id_user = TBLassoc_fiche_ed.id_user
                        WHERE TBLuser.id_user = :id_user";
            $req_user = $bdd->prepare($sql_user);
            $req_user->execute(array(
                ':id_user' => $id_user
            ));

            $info_perso = $req_user->fetch();

            return $info_perso;

        } catch (PDOException $e) {
            echo "Failed to select informations: " . $e->getMessage();
        }
    }

    function envoiMailDemandeContact($mail_ed, $nom_ed, $prenom_ed, $nom, $prenom, $mail, $tel, $code_postal, $message){
        
        $to = $mail_ed;

        $header = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

        $header .= 'To: ' . $to . "\r\n";

        $header .= 'From: contact@ed-numerique.fr' . "\r\n";

        $sujet = $prenom_ed . ' , nouvelle demande d\'un particulier sur ed-numerique.fr';

        $message = "<html>
                <head>
                    <title>Contact</title>
                </head>
                <body>
                    <p>
                        " . $nom . ' ' . $prenom . " vous a envoyé un message, informations :<br>
                        Tél : " . $tel . "<br>
                        Email : " . $mail . "<br>
                        Code postal : " . $code_postal . "<br><br>
                        Message : " . $message . "
                    </p>
                </body>
            </html>";

        return mail($to, $sujet, $message, $header);
    }

    function envoiMailContactEdNumerique($mail_ed, $nom_ed, $prenom_ed, $nom, $prenom, $mail, $tel, $code_postal, $message){
        
        $to = "contact@ed-numerique.fr";

        $header = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

        $header .= 'To: ' . $to . "\r\n";

        $header .= 'From: contact@ed-numerique.fr' . "\r\n";

        $sujet = $nom_ed . ' ' .$prenom_ed . ' a reçu une nouvelle demande';

        $message = "<html>
                <head>
                    <title>Contact</title>
                </head>
                <body>
                    <p>
                        " . $nom . ' ' . $prenom . " vous a envoyé un message, informations :<br>
                        Tél : " . $tel . "<br>
                        Email : " . $mail . "<br>
                        Code postal : " . $code_postal . "<br><br>
                        Message : " . $message . "
                    </p>
                </body>
            </html>";

        return mail($to, $sujet, $message, $header);
    }



    function envoieMessageDeConfirmation($nom, $mail, $message, $nom_ed, $prenom_ed){
        
        $to = $mail;

        $header = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

        $header .= 'To: ' . $to . "\r\n";
        $header .= 'From: contact@ed-numerique.fr' . "\r\n";

        $sujet = 'Ed-numérique - votre message a bien été envoyé';

        $message = "<html>
        <head>
            <title>Contact</title>
        </head>
        <body>
            <p>
                Bonjour " . $nom . ",<br><br>

                Votre message a bien été envoyé à ". $prenom_ed ." ". $nom_ed .", il vous recontactera dès que possible.<br><br>
                
                À très vite,<br>
                L'équipe <b>Ed numérique<b>
            </p>
        </body>
        </html>";

        return mail($to, $sujet, $message, $header);
    }
