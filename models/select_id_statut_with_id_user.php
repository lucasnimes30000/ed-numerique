<?php

try {

    $sql_select_statut_user ="SELECT id_statut, id_user
    FROM TBLassoc_statut_user
    WHERE id_user = :id_user
    AND id_statut = :id_statut";
    $req = $bdd->prepare($sql_select_statut_user);
    $req->execute([
        ':id_user' => $id_user,
        ':id_statut' => $id_statut    
    ]);

    $resultat_select_statut_user = $req->fetchAll();

} catch (PDOException $e) {

echo "Erreur lors de la recherche d'un statut <> user : " . $e->getMessage();

}