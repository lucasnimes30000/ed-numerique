<?php


try {
	$sql_materiel = "   SELECT DISTINCT TBLmateriel.nom_materiel, TBLmateriel.id_materiel 
						FROM TBLassoc_materiel_user 
						INNER JOIN TBLmateriel 
						ON TBLassoc_materiel_user.id_materiel = TBLmateriel.id_materiel 
						INNER JOIN TBLuser 
						ON TBLassoc_materiel_user.id_user = TBLuser.id_user 
						WHERE TBLuser.id_user = :id_user 
						ORDER BY lower(nom_materiel);";
	$req_materiel = $bdd->prepare($sql_materiel);
	$req_materiel->execute([
		':id_user' => $id_user
	]);
	$resultat = $req_materiel->fetchAll();

	echo "<section class='specialite_individuel' id='materiel_specialite'>";
	echo "<h4 class='titre_specialite'>Divers matériaux</h4>";

	foreach ($resultat as $row) {

		foreach ($resultat_materiel as $row_user) {
			if ($row_user['id_materiel'] === $row['id_materiel']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='materiel_checkbox'>
			
		<input " . htmlspecialchars($checked, ENT_QUOTES) . "id='materiel_" . $b . "' value='" . htmlspecialchars($row['id_materiel'], ENT_QUOTES) . "' name='materiel_" . $b . "_spe' type='checkbox' class='option_materiel_specialite option_specialite'><label for='materiel_" . $b . "_spe'>(id: " . htmlspecialchars($row['id_materiel'], ENT_QUOTES) . ") " . htmlspecialchars($row['nom_materiel'], ENT_QUOTES) . "</label></article>";

		$checked = "";
		$b++;
	}

	echo "<button type='button' class='btn_specialite btn' id='btn_update_specialite_materiel'>Confirmer</button>";
	echo "</section>";
	
} catch (PDOException $e) {

	echo "Erreur dans le chargement des materiels : " . $e->getMessage();

}
