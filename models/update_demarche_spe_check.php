<?php

try {
	$sql = "UPDATE TBLassoc_demarche_user SET specialite = 1 WHERE id_user = :id_user AND id_demarche = :id_demarche";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_demarche' => $id_demarche
	]);
	
	echo "Spécialités demarches mis à jour avec succès !";
} catch (PDOException $e) {

	echo "Erreur dans la création d'un nouveau label: " . $e->getMessage();
}