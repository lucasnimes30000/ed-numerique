<?php

try {
    $sql_id_nom = "UPDATE TBLuser SET nom_user = :nom_user WHERE id_user = :id_user";
    $req_id_nom = $bdd->prepare($sql_id_nom);
    $req_id_nom->execute([
    ':id_user' => $id_user,
    ':nom_user' => $nom_user,
]);
echo "Nom mis à jour avec succès !";    
} catch (PDOException $e) {
    echo "Erreur dans l'INSERT d'une nouvelle association user <-> nom : " . $e->getMessage();
}