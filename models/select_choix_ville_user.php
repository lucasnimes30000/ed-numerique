<?php

try {


$sql_ville = "SELECT DISTINCT TBLville.nom_ville, TBLville.id_ville, TBLville.code_postal
			FROM TBLassoc_ville_user 
			INNER JOIN TBLville
			ON TBLassoc_ville_user.id_ville = TBLville.id_ville
			INNER JOIN TBLuser
			ON TBLassoc_ville_user.id_user = TBLuser.id_user
			WHERE TBLuser.id_user = :id_user
			ORDER BY lower(nom_ville)";

$req_ville = $bdd->prepare($sql_ville);
$req_ville->execute([
	':id_user' => $id_user
]);
$resultat_ville = $req_ville->fetchAll();

foreach ($resultat_ville as $row) { ?>
	<tr>
		<td><?= htmlentities($row['nom_ville'], ENT_QUOTES); ?> <?= htmlentities($row['code_postal'], ENT_QUOTES) ?></td>
		<section>
			<input class="id_ville" type="hidden" name="id_ville" value="<?php echo htmlentities($row['id_ville'], ENT_QUOTES); ?>">
			<td><button value="<?php echo htmlentities($row['id_ville'], ENT_QUOTES); ?>" class="BTN_delete" type="button">🗑️</button></td>
		</section>
	</tr>
<?php } 

} catch (PDOException $e) {

echo "Failed to load user choices " . $e->getMessage();

}