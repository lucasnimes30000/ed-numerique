<?php

try {
    $sql_select = "SELECT id_fiche_ed FROM TBLassoc_fiche_ed WHERE id_user = :id_user";
    $req_select = $bdd->prepare($sql_select);
    $req_select->execute([
        ':id_user' => $id_user
    ]);
    $id_user_select = $req_select->fetch();
} catch (PDOException $e) {
    echo "Erreur dans la sélection d'une nouvelle association user <-> select : " . $e->getMessage();
}