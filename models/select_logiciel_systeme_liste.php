<?php

try {
	$sql_logiciel_systeme = "SELECT id_logiciel_systeme, nom_logiciel_systeme FROM TBLlogiciel_systeme ORDER BY lower(nom_logiciel_systeme);";
	$req_logiciel_systeme = $bdd->prepare($sql_logiciel_systeme);
	$req_logiciel_systeme->execute();
	$resultat = $req_logiciel_systeme->fetchAll();

	foreach ($resultat as $row) {

		foreach ($resultat_logiciel_systeme as $row_user) {
			if ($row_user['id_logiciel_systeme'] === $row['id_logiciel_systeme']) {
				
				echo "<li class='logiciel_systeme'>" . htmlspecialchars_decode($row['nom_logiciel_systeme'], ENT_QUOTES) . "</li>";

			}
		};
	}

} catch (PDOException $e) {

	echo "Erreur dans le chargement des logiciel_systemes : " . $e->getMessage();

}