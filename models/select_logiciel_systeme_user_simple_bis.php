<?php

try {
	$sql = "SELECT id_assoc_logiciel_systeme_user FROM TBLassoc_logiciel_systeme_user WHERE id_user = :id_user AND id_logiciel_systeme = :id_logiciel_systeme";
	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':id_logiciel_systeme' => $id_logiciel_systeme
	]);
	$resultat = $req->fetchAll();

} catch (PDOException $e) {

	echo "Erreur au moment de la vérification d'attribution de la spécialité : " . $e->getMessage();
}
