<?php

try {
	$sql_match = "SELECT nom_user, prenom, mail FROM TBLuser WHERE id_user = :id_user AND token_oublie = :token";

	$req_match = $bdd->prepare($sql_match);
	$req_match->execute([
		':id_user' => $id_user,
		':token' => $token
	]);
	$check_match = $req_match->fetchAll();
} catch (PDOException $e) {
	echo "(1) Erreur lors de la création du mot de passe, contactez au plus vite votre administrateur !  " . $e->getMessage();
}