<?php

try {
	$sql_materiel = "SELECT id_materiel, nom_materiel FROM TBLmateriel ORDER BY lower(nom_materiel);";
	$req_materiel = $bdd->prepare($sql_materiel);
	$req_materiel->execute();
	$resultat = $req_materiel->fetchAll();

	foreach ($resultat as $row) {
		$checked = "";

		foreach ($resultat_materiel as $row_user) {
			if ($row_user['id_materiel'] === $row['id_materiel']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='materiel_checkbox'>

		<input " . htmlentities($checked, ENT_QUOTES) . "id='materiel_" . $b . "' value='" . htmlentities($row['id_materiel'], ENT_QUOTES) . "' name='materiel_" . $b . "' type='checkbox' class='option_materiel'><label for='materiel_" . $b . "'>(id: " . htmlentities($row['id_materiel'], ENT_QUOTES) . ") " . htmlentities($row['nom_materiel'], ENT_QUOTES) . "</label></article>";

		$b++;
	}
} catch (PDOException $e) {

	echo "Erreur dans le chargement des materiels : " . $e->getMessage();

}