<?php

try {
	$sql_check_ville = "SELECT id_ville, id_user FROM TBLassoc_ville_user WHERE id_ville = :id_ville AND id_user = :id_user";
	$req_check_ville = $bdd->prepare($sql_check_ville);
	$req_check_ville->execute([
		':id_user' => $id_user,
		':id_ville' => $id_ville   
	]);
	$resultat_check_ville = $req_check_ville->fetch();
} catch (PDOException $e) {
	echo "Erreur lors de la vérification du lien entre ville et utilisateur, message à transmettre au développeur : " . $e->getMessage();
}