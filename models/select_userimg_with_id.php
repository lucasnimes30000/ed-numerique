<?php

try{
    $sql_url = "SELECT url_photo FROM TBLuser WHERE id_user = :id_user";
    $req_url = $bdd->prepare($sql_url);
    $req_url->execute([
        ':id_user' => (int) $id_user
    ]);

    $url_photo = $req_url->fetch();


} catch (PDOException $e) {
    echo "Erreur dans l'insert de l'image dans la DataBase " . $e->getMessage();
}