<?php

try {
	$sql = "SELECT nom_user, prenom, url_photo FROM TBLuser WHERE id_user = :id_user";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user
	]);
	$resultat = $req->fetchAll();

	$arr_vignette[$i]['nom'] = htmlspecialchars($resultat[0]['nom_user'], ENT_QUOTES);
	$arr_vignette[$i]['prenom'] = htmlspecialchars($resultat[0]['prenom'], ENT_QUOTES);
	$arr_vignette[$i]['img_profil'] = htmlspecialchars($resultat[0]['url_photo'], ENT_QUOTES);
	
} catch (PDOException $e) {

	echo "Erreur génération annuaire 92568 : " . $e->getMessage();
	
}
