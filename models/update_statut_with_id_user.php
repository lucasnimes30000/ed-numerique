<?php

try {
    $sql_suspendre = "UPDATE TBLuser SET statut = :statut WHERE id_user = :id_user";

    $req_suspendre = $bdd->prepare($sql_suspendre);
    $req_suspendre->execute([
        ':id_user' => $id_user,
        ':statut' => $statut
    ]);
    
} catch (PDOException $e) {

    echo "Erreur lors de la modification du statut de l'utilisateur : " . $e->getMessage();
    
}