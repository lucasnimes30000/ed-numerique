<?php

    function VerificationMailDB($mail){

        include("controller/id.php");

        try {
            $requete = "SELECT * FROM TBLuser
                WHERE mail = :mail;";
            $prepare = $bdd->prepare($requete);
            $prepare->execute(array(
                ":mail" => $mail
            ));

            $test = $prepare->rowCount();
            $infos = $prepare->fetch();

        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        return [$test, $infos];
    }

    function EnvoiMailDeRecuperation($mail, $id_user, $token_oublie, $nom){

        $to = $mail;

        $header = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

        $header .= 'To: ' . $to . "\r\n";
        $header .= 'From: contact@ed-numerique.fr' . "\r\n";

        $sujet = 'Ed numérique - votre changement de mot de passe';

        $message = "<html>
        <head>
            <title>Mot de passe oublié</title>
        </head>
        <body>
            <p>
                Bonjour " . $nom . ",<br><br>

                Vous avez oublié votre mot de passe ? Pas d’inquiétude ! Cela arrive à tout le monde.<br><br>

                <a href='http://ed-numerique.ovh/?edit_mdp&token=". $token_oublie ."&id_user=". $id_user ."'>Modifiez votre mot de passe</a><br><br>
                
                À très vite,<br>
                L'équipe <b>Ed numérique<b>
            </p>
        </body>
        </html>";

        return mail($to, $sujet, $message, $header);
    }