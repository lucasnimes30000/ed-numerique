<?php

try {
	$sql_logiciel_systeme = "SELECT id_logiciel_systeme, nom_logiciel_systeme FROM TBLlogiciel_systeme ORDER BY lower(nom_logiciel_systeme);";
	$req_logiciel_systeme = $bdd->prepare($sql_logiciel_systeme);
	$req_logiciel_systeme->execute();
	$resultat = $req_logiciel_systeme->fetchAll();

	foreach ($resultat as $row) {

		foreach ($resultat_logiciel_systeme as $row_user) {
			if ($row_user['id_logiciel_systeme'] === $row['id_logiciel_systeme']) {
				$checked = " checked=\"true\" ";
			}
		};

		echo "<article class='logiciel_systeme_checkbox'>

		<input " . htmlentities($checked, ENT_QUOTES) . "id='logiciel_systeme_" . $b . "' value='" . htmlentities($row['id_logiciel_systeme'], ENT_QUOTES) . "' name='logiciel_systeme_" . $b . "' type='checkbox' class='option_logiciel_systeme'><label for='logiciel_systeme_" . $b . "'>(id: " . htmlentities($row['id_logiciel_systeme'], ENT_QUOTES) . ") " . htmlentities($row['nom_logiciel_systeme'], ENT_QUOTES) . "</label></article>";

		$checked = "";
		$b++;
	}
} catch (PDOException $e) {

	echo "Erreur dans le chargement des logiciel_systemes : " . $e->getMessage();

}