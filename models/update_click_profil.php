<?php

try {
	$sql = "UPDATE TBLclick_profil SET nbr_click_profil= :nbr_click_profil WHERE id_user = :id_user AND annee = :annee AND mois = :mois";

	$req = $bdd->prepare($sql);
	$req->execute([
		':id_user' => $id_user,
		':annee' => $annee,
		':mois' => $mois,
		':nbr_click_profil' => $nbr_click_plus_un
	]);
	$resultat = $req->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans l'update des clicks profil: " . $e->getMessage();
	
}