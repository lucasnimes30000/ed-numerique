<?php

try {

	$sql_contrat = "SELECT DISTINCT id_contrat, date_ajout, nom_contrat FROM TBLcontrat WHERE id_user = :id_user
				ORDER BY lower(nom_contrat)";

	$req_contrat = $bdd->prepare($sql_contrat);
	$req_contrat->execute([
		':id_user' => $id_user
	]);
	$resultat_contrat = $req_contrat->fetchAll();

	foreach ($resultat_contrat as $row) { ?>
		<tr>
			<td class="row date_row"><?= $row['date_ajout'] ?></td>
			<td class="row contrat_row"><?= $row['nom_contrat']; ?></td>
			<td><a class="row nom_contrat_row" href="<?php echo "../views/assets/user/" .  $id_user  . "/contrat/" . htmlentities($row['nom_contrat'], ENT_QUOTES) ?>" download>Télécharger</a></td>
		</tr>

	<?php } 
	
} catch (PDOException $e) {

	echo "Failed to load user choices " . $e->getMessage();
	
}