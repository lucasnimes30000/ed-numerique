<?php

try {
	$sql_fiche = "SELECT DISTINCT TBLfiche_ed.id_fiche_ed 
				FROM TBLassoc_fiche_ed 
				INNER JOIN TBLfiche_ed 
				ON TBLassoc_fiche_ed.id_fiche_ed = TBLfiche_ed.id_fiche_ed 
				INNER JOIN TBLuser 
				ON TBLassoc_fiche_ed.id_user = TBLuser.id_user 
				WHERE TBLuser.id_user = :id_user";

	$req_fiche = $bdd->prepare($sql_fiche);
	$req_fiche->execute([
		':id_user' => $id_user
	]);
	$resultat_fiche = $req_fiche->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans la vérification des fiches en lien avec l'utilisateur : " . $e->getMessage();
	
}

if(!empty($resultat_fiche)) {
	echo "existe";
} else {
	echo "n'existe pas";
}