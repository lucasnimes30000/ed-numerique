<?php

try {
    $sql_image_jv = "DELETE FROM TBLimage_jv WHERE id_image_jv = :id_image_jv";
    $req_image_jv = $bdd->prepare($sql_image_jv);
    $req_image_jv->execute([
    ':id_image_jv' => $id_image_jv
]);

    echo "image_jv supprimé avec succès !";
} catch (PDOException $e) {
    echo "Failed to create image_jv: " . $e->getMessage();
}