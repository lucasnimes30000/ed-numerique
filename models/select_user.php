<?php

try {
	$sql_user = "SELECT nom_user, prenom, mail
				FROM TBLuser
				WHERE id_user = :id_user";

	$req_user = $bdd->prepare($sql_user);
	$req_user->execute([
		':id_user' => $id_user
	]);
	$resultat_user = $req_user->fetchAll();
	
} catch (PDOException $e) {

	echo "Erreur dans la vérification de l'existance de l'utilisateur : " . $e->getMessage();
	
}