<?php

try {
	$sql_select = "SELECT nom_infrastructure FROM TBLinfrastructure WHERE nom_infrastructure = :nom_infrastructure";
	$req_select = $bdd->prepare($sql_select);
	$req_select->execute([
	':nom_infrastructure' => $nom_infrastructure
	]);
	$resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
	echo "Erreur dans la vérification de l'existance d'un infrastructure : " . $e->getMessage();
}