<?php

try {
	$sql_infrastructure = "INSERT INTO TBLinfrastructure(nom_infrastructure) VALUES (:nom_infrastructure)";
	$req_infrastructure = $bdd->prepare($sql_infrastructure);
	$req_infrastructure->execute([
	':nom_infrastructure' => $nom_infrastructure
	]);

	echo "Nouveau infrastructure créé avec succès !";
} catch (PDOException $e) {
	echo "Failed to create infrastructure: " . $e->getMessage();
}