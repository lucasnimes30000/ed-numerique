<?php

try {
	$sql_demarche = "SELECT id_demarche, nom_demarche FROM TBLdemarche ORDER BY lower(nom_demarche);";
	$req_demarche = $bdd->prepare($sql_demarche);
	$req_demarche->execute();
	$resultat = $req_demarche->fetchAll();

	foreach ($resultat as $row) {

		foreach ($resultat_demarche as $row_user) {
			if ($row_user['id_demarche'] === $row['id_demarche']) {
				
				echo "<li class='demarche'>" . htmlentities($row['nom_demarche'], ENT_QUOTES) . "</li>";

			}
		};
	}

} catch (PDOException $e) {

	echo "Erreur dans le chargement des demarches : " . $e->getMessage();

}