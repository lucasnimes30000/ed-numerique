<?php


try {
	$sql_materiel = "SELECT DISTINCT TBLmateriel.nom_materiel, TBLmateriel.id_materiel 
				FROM TBLassoc_materiel_user 
				INNER JOIN TBLmateriel 
				ON TBLassoc_materiel_user.id_materiel = TBLmateriel.id_materiel 
				INNER JOIN TBLuser 
				ON TBLassoc_materiel_user.id_user = TBLuser.id_user 
				WHERE TBLuser.id_user = :id_user AND TBLassoc_materiel_user.specialite = 1";

	$req_materiel = $bdd->prepare($sql_materiel);
	$req_materiel->execute([
		':id_user' => $id_user
	]);
	$resultat_materiel = $req_materiel->fetchAll();
	


} catch (PDOException $e) {

	echo "Erreur dans la vérification des materiaux en lien avec l'utilisateur : " . $e->getMessage();
	
}