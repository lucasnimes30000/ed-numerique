<?php

try {
	$sql_select = "SELECT nom_demarche FROM TBLdemarche WHERE nom_demarche = :nom_demarche";
	$req_select = $bdd->prepare($sql_select);
	$req_select->execute([
	':nom_demarche' => $nom_demarche
]);
	$resultat_select = $req_select->fetchAll();
} catch (PDOException $e) {
	echo "Erreur dans la vérification de l'existance d'un demarche : " . $e->getMessage();
}
