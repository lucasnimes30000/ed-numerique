<?php
        try {
            $sql_image_jv = "SELECT id_image_jv, nom_image_jv FROM TBLimage_jv ORDER BY lower(nom_image_jv);";
            $req_image_jv = $bdd->prepare($sql_image_jv);
            $req_image_jv->execute();
            $resultat = $req_image_jv->fetchAll();
        
            foreach ($resultat as $row) {
        
                foreach ($resultat_image_jv as $row_user) {
                    if ($row_user['id_image_jv'] === $row['id_image_jv']) {
                        $checked = " checked=\"true\" ";
                    }
                };
        
                echo "<article class='image_jv_checkbox'>
        
                <input " . htmlentities($checked, ENT_QUOTES) . "id='image_jv_" . $b . "' value='" . htmlentities($row['id_image_jv'], ENT_QUOTES) . "' name='image_jv_" . $b . "' type='checkbox' class='option_image_jv'><label for='image_jv_" . $b . "'>(id: " . htmlentities($row['id_image_jv'], ENT_QUOTES) . ") " . htmlentities($row['nom_image_jv'], ENT_QUOTES) . "</label></article>";
        
                $checked = "";
                $b++;
            }
        } catch (PDOException $e) {
        
            echo "Erreur dans le chargement des image_jvs : " . $e->getMessage();
        
        }