<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Formulaire de contact par email : posez-nous toutes vos questions ici.">
    <?php include("views/metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/contact.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <title>Contact</title>
</head>
<body>

<?php include("views/header_annuaire.php") ?>

<main>  
    <section class="blocTOUT">

        <article class="blocGauche">

            <p id="blocTitreGauche">Nous contacter par message</p>

            <section id="globalErreur">
                <p>Les champs en surbrillances n'ont pas été correctement renseignés</p>
            </section>

            <section id="globalOK">
                <p>Message envoyé avec succès !</p>
            </section>

            <form action="controller/bruh2.php" method="POST" class="formBlocGauche">

                <section class="formFlex">

                    <article class="nom">
                        <label id="labelForm" for="inputNomTitre">Nom *</label><br>
                        <input name="nom" type="text" class="formInput globalInput nomInput" id="nom" value="">
                        <small class="emptyTxt">Champs obligatoire</small>

                    </article>

                    <article class="prenom">
                        <label id="labelForm" for="inputPrenomTitre">Prénom *</label><br>
                        <input name="prenom" type="text" class="formInput globalInput prenomInput" id="prenom" value="">
                        <small class="emptyTxt">Champs obligatoire</small>

                    </article>
                </section>
                <section class="formFlex">                    

                    <article class="mail">
                        <label id="labelForm" for="inputMailTitre">Adresse mail *</label><br>
                        <input name="mail" type="email" class="formInput globalInput mailInput" id="mail" value="">
                        <small class="emptyTxt">Champs obligatoire</small>

                    </article>

                    <article class="telephone">
                        <label id="labelForm" for="inputTelephoneTitre">Numéro de téléphone *</label><br>
                        <input name="telephone" type="text" class="formInput2 globalInput formInput1 formInput formInputNumber" id="telephone" value="">
                        <small class="emptyTxt">Champs obligatoire</small>

                    </article>

                    <article class="postal">
                        <label id="labelForm" for="inputCodePostalTitre">Code postal *</label><br>
                        <input name="cp" type="text" class="cp cpInput formInput2 globalInput formInput" id="cp" value="">
                        <small class="emptyTxt">Champs obligatoire</small>

                    </article>
                </section>
                
                <section class="message">
                    <label id="labelForm" for="inputMessageTitre">Votre message *</label><br>
                    <textarea name="message" class="messageInput globalInput formInput" maxlength="10000" id="message"></textarea>
                    <small class="emptyTxt">Champs obligatoire</small>

                </section>
                <small>(*) Ces champs sont obligatoires</small><br><br>
                <section>                
                    <a class="BTNdonnees" href="?poldeconf" style="text-decoration: none;">Comment mes données sont-elles utilisées ?</a>
                </section>
                <section class="checkBox" id="Condition">
                    <input name="Condition" type="checkbox" class="globalInput checkBoxForm">
                    <small class="emptyTxt">Champs obligatoire</small>
                    <label for="inputCheckboxTitre" class="textCheckBox">J'ai lu, compris et accepté. J’autorise Ed-Numérique à utiliser les données mentionnées ci-dessus, <a href="?poldeconf">dans les conditions prévues</a>. *</label>
                </section><br>
                <small class="test"></small><br>
                <input type="hidden" id="recaptchaResponse" name="recaptcha-response">
                <button type="submit" class="BTNcontactForm" id="envoiContact" name="envoyer">Envoyer ma demande</button>
            </form>

            <script src="https://www.google.com/recaptcha/api.js?render=6LfCxvgaAAAAAPn2k1k60rqRFUhhO9eBi8QEVsP2"></script>
            <script>
            grecaptcha.ready(function() {
                grecaptcha.execute('6LfCxvgaAAAAAPn2k1k60rqRFUhhO9eBi8QEVsP2', {action: 'homepage'}).then(function(token) {
                    document.getElementById('recaptchaResponse').value = token
                });
            });
            </script>
            
        </article>
    </section>

</main>

<a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>

<?php include("views/footer.php");
include("views/popup.php"); ?>
<script type="text/javascript" src="views/assets/js/contact.js"></script>
<script type="text/javascript" src="views/assets/js/header_annuaire.js"></script>
</body>
</html>

