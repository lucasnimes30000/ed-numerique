<?php
    include("controller/inc_session.php");
    sessionInit();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Page d’accueil de la plateforme Ed numérique : sélectionnez votre ville et contactez votre aidant
numérique.">
    <?php include("views/metadonnee.php"); ?>
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/accueil.css">
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <link rel="stylesheet" href="views/assets/css/swdslideshow.css">
    <title>Accueil</title>    
</head>
<body>
    <section class="presentation">
        <article>
            <img src="views/assets/img/accueil/Ed_Numérique_Logo_Vertical_Monochrome_Green.svg" alt="Logo">
            <h4>Un problème informatique ?</h4>
            <h2>Votre assistance à domicile avec des passionnés du numérique</h2>
        </article>

        
        <div id="swd-slideshow">
            <div style="background-image: url('views/assets/img/accueil/daughter-teaching-technology-min.jpg')"></div>
            <div style="background-image: url('views/assets/img/accueil/Cours-informatique-senior (1)-min.jpg')"></div>
            <div style="background-image: url('views/assets/img/accueil/Cours-ordinateur-personne-agee (1)-min.jpg')"></div>
            <div style="background-image: url('views/assets/img/accueil/family-using-computer_4460x4460 (1)-min.jpg')"></div>
            <div style="background-image: url('views/assets/img/accueil/General Mac-min.jpeg')"></div>
            <div style="background-image: url('views/assets/img/accueil/helping-senior-on-computer_4460x4460 (1)-min.jpg')"></div>
            <div style="background-image: url('views/assets/img/accueil/Personna 1 -min.jpg')"></div>
            <div style="background-image: url('views/assets/img/accueil/Personna 1 bis-min.jpg')"></div>
            <div style="background-image: url('views/assets/img/accueil/Personna 3 bis-min.jpg')"></div>
            <div style="background-image: url('views/assets/img/accueil/pexels-andrea-piacquadio-3860805-min.jpg')"></div>
            <div style="background-image: url('views/assets/img/accueil/reparation ordi-min.jpg')"></div>
        </div>

        <article>
            <h2>Choisissez votre ville</h2>

            <?php
                include("controller/model.accueil.php");

                foreach($resultat_ville as $ville){
                    echo("<a href='?villechoix&id_ville=" . htmlentities($ville['id_ville'], ENT_QUOTES) . "&ville=" . htmlentities($ville['nom_ville'], ENT_QUOTES) . "'><img src='" . htmlentities($ville['image'], ENT_QUOTES) . "' alt='photo de la ville de " . htmlentities($ville['nom_ville'], ENT_QUOTES) . "'></a>");
                }
            ?>
        </article>
    </section>

    <section class="fonctionnement">
        <h2>Comment ça marche ?</h2>

        <article>
            <h4>1. Sélectionnez votre ville ?</h4>
            <img src="views/assets/img/accueil/Cityscapes - Downtown-min.png" alt="dessin d'une ville imaginaire">
            <p>Cliquez sur la vignette de votre localité pour trouver un accompagnement informatique</p>
        </article>

        <article>
            <h4>2. Contactez votre aidant au numérique</h4>
            <img src="views/assets/img/accueil/Hands - Exchange-min-turned.png" alt="image de deux personnes échangant leur contact téléphonique">
            <p>Nous vous assistons en cas de problème, question ou suggestion</p>
        </article>

        <article>
            <h4>3. Profitez !</h4>
            <img src="views/assets/img/accueil/Hands - Phone-min.png" alt="image d'une personne utilisant son téléphone avec succés">
            <p>Ed numérique vous aide à profiter à 100% de la révolution numérique</p>
        </article>
    </section>

    <section class="info_contact">
        <article>
            <h2>En résumé ...</h2>

            <iframe class="youtube_video" width="960" height="480" src="https://www.youtube.com/embed/ainMuWvZFYQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        </article>

        <article>
            <h2>Vous souhaitez avoir plus d’informations ?</h2>

            <nav>
                <a href="tel:09 72 50 75 54"><svg width="134" height="130" viewBox="0 0 134 130" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="134" height="130" rx="65" fill="#1B7272"/>
                    <path d="M62.1717 70.8533C66.1727 75.1475 71.0138 78.4628 76.355 80.5664L83.5808 74.4831C83.7951 74.327 84.049 74.2434 84.3092 74.2434C84.5693 74.2434 84.8233 74.327 85.0375 74.4831L98.4542 83.6283C98.9635 83.9523 99.3947 84.3972 99.7136 84.9277C100.033 85.4583 100.231 86.0601 100.292 86.6855C100.353 87.3109 100.276 87.9428 100.067 88.5314C99.8579 89.1199 99.5223 89.6489 99.0867 90.0767L92.8 96.6467C91.8997 97.5878 90.7931 98.2785 89.5791 98.6571C88.3651 99.0357 87.0814 99.0904 85.8425 98.8164C73.4842 96.1161 62.0933 89.7918 52.9717 80.5664C44.0524 71.2493 37.8803 59.3941 35.2042 46.4389C34.9398 45.1485 34.995 43.8077 35.3643 42.5465C35.7337 41.2853 36.4048 40.1465 37.3125 39.2403L43.8292 32.5892C44.2324 32.1508 44.7242 31.8145 45.2677 31.6057C45.8112 31.3968 46.3923 31.3207 46.9675 31.3832C47.5427 31.4456 48.0972 31.6449 48.5893 31.9661C49.0814 32.2873 49.4984 32.7222 49.8092 33.2381L58.7408 47.25C58.8949 47.47 58.978 47.7366 58.978 48.0104C58.978 48.2843 58.8949 48.5508 58.7408 48.7709L52.8567 56.2533C54.899 61.7911 58.081 66.7784 62.1717 70.8533Z" fill="#E6E6E6"/>
                </svg></a>

                <a href="tel:09 72 50 75 54"><h4>09 72 50 75 54</h4></a>

            </nav>

            <nav>
            <a href="?contact"><svg width="134" height="130" viewBox="0 0 134 130" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="134" height="130" rx="65" fill="#1B7272"/>
                    <path d="M104.333 36.8333H29.6666C28.2522 36.8333 26.8956 37.3542 25.8954 38.2815C24.8952 39.2088 24.3333 40.4664 24.3333 41.7778V91.2222C24.3333 92.5335 24.8952 93.7912 25.8954 94.7184C26.8956 95.6457 28.2522 96.1666 29.6666 96.1666H104.333C105.748 96.1666 107.104 95.6457 108.105 94.7184C109.105 93.7912 109.667 92.5335 109.667 91.2222V41.7778C109.667 40.4664 109.105 39.2088 108.105 38.2815C107.104 37.3542 105.748 36.8333 104.333 36.8333ZM100.227 91.2222H34.0933L52.76 73.3233L48.92 69.8869L29.6666 88.3544V45.5355L62.8133 76.1169C63.8126 77.0378 65.1643 77.5547 66.5733 77.5547C67.9823 77.5547 69.334 77.0378 70.3333 76.1169L104.333 44.7691V88.033L84.7066 69.8375L80.9466 73.3233L100.227 91.2222ZM33.16 41.7778H100.013L66.5733 72.6064L33.16 41.7778Z" fill="white"/>
                </svg></a>

                <a href="?contact"><h4>Nous écrire</h4></a>
            </nav>
        </article>
    </section>

    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>

    <?php
        include("views/popup.php");
        include("views/footer.php");
    ?>
    <script src="views/assets/js/swdslideshow.js"></script>
</body>
</html>
