<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include("metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/connexion.css">
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="stylesheet" href="views/assets/css/creation_mdp.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>Modification MDP</title>
</head>
<body>
    <?php include("views/header_annuaire.php") ?>  
    <main>
        <section class="mainBlock">
        <input type="hidden" id="id_user" name="" value="<?= $id_user ?>">
            <article class="blockDroit">
                <p class="alert_mdp" id="accord_pas_ok">Merci d'accepter les conditions générales d'utilisation</p>
                <p class="alert_mdp" id="champs_vide">Il est obligatoire de remplir les deux champs</p>
                <p class="alert_mdp" id="champs_differents">Les mots de passe sont différents</p>
                <p class="alert_mdp" id="nombre_carac_8">Un mot de passe d'au moins 8 caractères est requis</p>
                <p class="alert_mdp" id="erreur_id_token">Les informations d'authentification ne sont pas bien renseignées</p>
                <p class="alert_mdp" id="no_match">Les information d'authentification ne sont pas reconnues</p>
                <p class="alert_mdp" id="erreur_creation">Erreur dans la finalisation de la création de votre mot de passe, contactez au plus vite un administrateur</p>
                <p class="alert_mdp" id="mdp_modif">La modification du mot de passe a été prise en compte</p>

                <section class="txtBlock">
                    <h2 class="titreDroit">Bonjour,</h2>
                    <p>Choisissez un nouveau mot de passe</p>
                    <hr><br>
                    <form method="POST" action="" class="formBlocGauche">
                        <div>
                            <img src="views/assets/img/formulaires/mdp_icon_svg.svg" alt="Logo clef" class="logoProfil_1">
                            <input required id = "input_mdp_log_2" type="password" class="mail1Input formInput" name="password" placeholder="Mot de passe">
                        </div>
                        <div>
                            <img src="views/assets/img/formulaires/mdp_icon_svg.svg" alt="Logo clef" class="logoProfil_2">
                            <input required name="password2" type="password" id = "input_mdp_log" class="MDPinput formInput" placeholder="Confirmation">
                        </div>    
                        <br>

                        <input required type="checkbox" name="accord_condition" id="accord_condition">
                        <label id="accord_text" for="accord_condition">J'accepte les <a href="">conditions générales d'utilisation</a></label>

                        <input type="hidden" name="token" value="<?= $token ?>">
                        <input type="hidden" name="id_user" value="<?= $id_user ?>">

                        <div class="BTNs">
                            <button type="submit" name="edit" class="BTNconnexion" id="btn_submit">Modifier</button>
                        </div>
                    </form>
                </section>
            </article>
        </section>
    </main>
    <?php include("views/footer.php") ?>    
    <script type="text/javascript" src="views/assets/js/header_annuaire.js"></script>
</body>
</html>