<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Liste des aidants de votre ville <?= htmlentities($_GET['ville'], ENT_QUOTES) ?> : choisissez un intervenant proche de chez vous !">
    <?php include("views/metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/annuaire.css">
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>Annuaire</title>
</head>
<body>
    <?php include("views/header_annuaire.php"); ?>
    <main>
        <input type="hidden" class="ville_get" value="<?= htmlentities($_GET['ville'], ENT_QUOTES) ?>" class="ville_get">
        <input type="hidden" class="id_ville" value="<?= htmlentities($_GET['id_ville'], ENT_QUOTES) ?>" class="id_ville">

        <h1 id="phrase_1">Trouvez les professionnels de l'assistance informatique de votre ville</h1>
        <!-- echo $ville_choisie -->
        <h2 id="phrase_2">LES EDS DE <?= htmlentities(mb_strtoupper($_GET['ville']), ENT_QUOTES) ?></h2>

        <section id="vignettes">

            
        </section>

    </main>

    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>
    
    <?php include("views/footer.php");
    	  include("views/popup.php"); ?>
    <script src="views/assets/js/annuaire.js"></script>
    <script src="views/assets/js/header_annuaire.js"></script>

</body>
</html>
