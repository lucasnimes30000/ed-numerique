<?php

    include("controller/inc_session.php");
    sessionInit();

    if (sessionValid() !== true) {
        
        sessionBye(); 
        header("Location: ?connexion#sessionerror");
        exit;
        
    }

    include("controller/id.php");
    try {
        $sql_user = "SELECT * FROM TBLuser
                    WHERE id_user = :id_user";
        $req_user = $bdd->prepare($sql_user);
        $req_user->execute([
            ':id_user' => htmlspecialchars($_SESSION["id_user"], ENT_QUOTES)
        ]);

        $info_perso = $req_user->fetch();

    } catch (PDOException $e) {
        echo "Failed to select informations: " . $e->getMessage();
    }

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include("metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/espace_pro.css">
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/footer.css">

    <title>Mon espace Professionnels</title>
</head>
<body>
    <input id="id_user" type="hidden" value="<?= htmlspecialchars($_SESSION["id_user"], ENT_QUOTES); ?>">
    <?php
        include("views/header_annuaire.php");
    ?>
    <main>
        <section id="top">
            <article id="block_image">
                <img placeholder="views/assets/img/annuaire/icone_ed.png" id="PP" src="views/assets/user/<?= htmlspecialchars($_SESSION["id_user"], ENT_QUOTES) ?>/PP/<?= htmlspecialchars($info_perso['url_photo'], ENT_QUOTES) ?>" alt="Image Profil">
                <p id="nom_prenom"><?= $info_perso['nom_user'] ?> <?= htmlspecialchars($info_perso['prenom'], ENT_QUOTES) ?></p>
            </article>

            <input type="hidden" name="" id="url_id" value="/views/assets/user/<?= htmlspecialchars($_SESSION["id_user"], ENT_QUOTES) ?>/PP/<?= htmlspecialchars($info_perso['url_photo'], ENT_QUOTES) ?>">

            
            <article id="btn_section">
                <button id="btn_contrat" class="btn">Mes contrats</button>
                <button id="btn_mdp" class="btn">Changer mon mot de passe</button>
                <button id="btn_fiche" class="btn">Voir ma fiche</button>
                <button id="btn_deconnexion" class="btn">Déconnexion</button>
            </article>
        </section>

        <section id="titre">
            <h2 id="h2_page"></h2>
        </section>

        <section id="content">
        </section>


    </main>

    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>
    
    <?php
        include("views/footer.php");
        include("views/popup.php");
    ?>
    <script type="text/javascript" src="views/assets/js/header_annuaire.js"></script>
    <script type="text/javascript" src="views/assets/js/espace_pro.js"></script>
</body>
</html>
