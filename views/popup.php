<html lang="fr">
<body>

	<section id="blur"></section>
	<section id="popup_cookie">

		<h1 id="titre_popup">Aperçu de confidentialité</h1>

		<article id="contenu_1" style="display: flex;">
			<p id="p_1">Ed numérique a choisi de ne pas utiliser de cookies publicitaires. Nous utilisons seulement des cookies nécessaires aux fonctionnalités de base et à l’amélioration de votre expérience lorsque vous naviguez sur notre site.</p>
			<section id="buttons_1">
				<button id="btn_switch" type="button">PLUS D'INFORMATIONS</button>
				<button id="btn_accepter" type="button">ACCEPTER</button>
			</section>
		</article>

		<article id="contenu_2" style="display: none;">
			<section id="point_contenu">
					<ul class="point_point">
						<li>Cookie de mémorisation lorsqu'un utilisateur clic pour voir le numéro de téléphone d'un ed. Il permet à l'utilisateur de n'avoir à cliquer qu'une seule fois pour le visualiser. Son utilisation secondaire est de n'envoyer qu'une seule fois la statistique du clic.<b> Nécessaire</b></li>
						<li>Cookie de mémorisation lorsqu'un utilisateur clic sur le bouton "envoyer un mail" sur une fiche ed. Le but de ce cookie est de bloquer l'envoi d'une statistique si ce bouton a déjà été cliqué une fois.<b> Nécessaire</b></li>
						<li>Cookie de mémorisation lorsqu'un utilisateur clic sur le bouton "profil complet" sur une vignette ed. Le but de ce cookie est de bloquer l'envoi d'une statistique si ce bouton a déjà été cliqué une fois.<b> Nécessaire</b></li>
						<li>Cookie de mémorisation lors d'une recherche dans l'annuaire. Il permet de garder en mémoire la ville ciblée sur le moment afin d'assurer un bon affichage sur la fiche ed.<b> Nécessaire</b></li>
						<li>Un cookie de session est utilisé afin d'assurer le service permettant aux ed d'accéder à leur espace privé sur notre plateforme en toute sécurité.<b> Nécessaire</b></li>
					</ul>
			</section>
			<section id="buttons_2">
				<button id="btn_switchey" type="button">RETOUR</button>
				<button id="btn_acceptey" type="button">TOUT ACCEPTER</button>
			</section>
		</article>

	</section>

    <script src="views/assets/js/popup.js"></script>
</body>
</html>