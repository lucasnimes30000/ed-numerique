<?php
include("controller/inc_session.php");
    sessionInit();

    if (sessionValid() == true) {
        header("Location: ?espace_pro");
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bienvenue sur l’écran de connexion de votre espace Ed, vous pouvez mettre à jour votre
profil ainsi que vos informations personnelles.">
    <?php include("views/metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/connexion.css">
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>Connexion</title>
</head>
<body>
    <?php include("views/header_annuaire.php") ?>  
    <main>
        <section class="mainBlock">
            <article class="blockDroit">
                <p id="emptyMail">Aucun mail renseigné</p>
                <p id="emptyPassword">Aucun mot de passe renseigné</p>
                <p id="empty">Aucune information renseignée</p>
                <p id="notFound">Identifiants incorrects</p>
                <p id="sessionerror">identifiants incorrects</p>

                <section class="txtBlock">
                    <h2 class="titreDroit">Espace Ed</h2>
                    <hr><br>
                    <form action="?verif_login" method="POST" class="formBlocGauche">
                        <div>
                            <img src="views/assets/img/formulaires/mail_icon_svg.svg" alt="Logo enveloppe" class="logoProfil_1">
                            <input required name="email" type="email" class="mail1Input formInput" placeholder="Adresse mail">
                        </div>
                        <div>
                            <img src="views/assets/img/formulaires/mdp_icon_svg.svg" alt="Logo clef" class="logoProfil_2">
                            <input required name="password" type="password" id = "input_mdp_log" class="MDPinput formInput" placeholder="Mot de passe">

                        </div>    
                        <br>
                        <div class="MDPoublie">
                            <a class="LINKmdpOublie" href="?mdpoublie">J'ai oublié mon mot de passe</a>
                        </div>
                        <div class="dialogueCheckFalse">Vos identifiants sont incorrects</div>
                        <div class="BTNs">
                            <button type="sumbit" class="BTNconnexion">Connexion</button>
                        </div>
                    </form>
                </section>
            </article>
        </section>
    </main>

    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>
    
    <?php include("views/footer.php");
    include("views/popup.php"); ?>  
    <script type="text/javascript" src="views/assets/js/header_annuaire.js"></script>
</body>
</html>
