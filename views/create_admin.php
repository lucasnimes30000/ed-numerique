<?php

	include("../controller/inc_session.php");
	sessionInit();

	if (sessionValidAdmin() !== true) {
		
		sessionBye(); 
		header("Location: connexion_admin.php#sessionerror");
		exit;
		
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" href="../views/assets/css/structure_commune.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="../views/assets/img/logo.svg"/>
	<title>Creation Admin</title>
</head>
<body>

	<?php include("../views/header_admin.php") ?>

	<form style="margin-top: 5rem" action="../controller/inscription_admin.php" method="POST">
		<label for="username">Nom d'utilisateur : </label>
		<input type="text" name="username">
		<label for="password">Mot de passe : </label>
		<input type="password" name="password">
		<button type="submit">Créer</button>
	</form>

	<?php

		include("../controller/id.php");

		try {

			$sql ="SELECT * FROM TBLadmin";
			$req = $bdd->prepare($sql);
			$req->execute([
				':username'       => $username,
				':password'       => $password,
			]);

			$resultat = $req->fetchAll();

		} catch (PDOException $e) {

			echo "Erreur lors de la création d'un administrateur : " . $e->getMessage();

		}

	?>

	<h3>Liste des comptes administrateur :</h3>
	
	<?php
		foreach($resultat as $value) {
			echo $value['username'] . "---<a href='../controller/delete_admin.php?id_admin=" . $value['id_admin'] . "'>Supprimer</a><br><br>";
		}

	?>
</body>
</html>