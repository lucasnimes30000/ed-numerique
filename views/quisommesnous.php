<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include("metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/quisommesnous.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <title>Qui sommes nous ?</title>
</head>
<body>
    <?php include("views/header_annuaire.php"); ?>
    <main>
        <section id="equipeEd">
            <h1>L'ÉQUIPE ED NUMÉRIQUE</h1>

            <article>
                <img src="views/assets/img/quisommesnous/Nicola.png" alt="Photo de Nicola Goubet">
                <h3>Nicola</h3>
            </article>

            <article>
                <img src="views/assets/img/quisommesnous/Cédric.jpg" alt="Photo de Cédric Gasq">
                <h3>Cédric</h3>
            </article>

            <article>
                <img src="views/assets/img/quisommesnous/Aurélien.png" alt="Photo de Aurélien Bournonville">
                <h3>Aurélien</h3>
            </article>
        </section>

        <section id="projetEd">
        <h2>POURQUOI AVOIR CRÉÉ ED NUMÉRIQUE</h2>

        <p>Nous sommes 3 amis d’enfance qui nous sommes réunis autour d’un projet regroupant nos valeurs et notre passion pour le monde du digital.</p>

        <p>S’attaquer à la problématique de la fracture numérique et des conséquences sur nos concitoyens nous paraissait évident car nous y étions confrontés tous les jours, dans notre entourage familial ou professionnel.</p>

        <p>Il y a aujourd’hui dans le contexte de la révolution numérique une majorité silencieuse composée de catégories de personnes aux profils très divers, à qui nous souhaiterions donner la possibilité d’acquérir une réelle autonomie dans l’utilisation des leurs outils numériques.</p> 
        </section>
    </main>

    <?php include("views/footer.php");
    include("views/popup.php"); ?>

    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>
    
    <script src="views/assets/js/header_annuaire.js"></script>
</body>
</html>
