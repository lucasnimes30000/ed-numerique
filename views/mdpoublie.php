<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include("views/metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/mdpoublie.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/footer.css">

    <title>Mot de passe oublié</title>
</head>
<body>

    <?php include("views/header_annuaire.php") ?>   

    <main>
        <section class="mainBlock">
                    
            <section class="txtBlock"> 
                <h2 class="titreDroit">Mot de passe oublié</h2>
                <hr><br>
                <form method="POST" action="" class="formBlocGauche">

                    <div class="mail1 mail">
                        <img src="views/assets/img/formulaires/mail_icon_svg.svg" alt="Logo enveloppe" class="logoMail logomail1">
                        <input required name="mail_1" type="email" class="mail1Input mailInput1 formInput" placeholder="Adresse mail">
                    </div><br>
                    <div class="checkBox">
                        <input name="checkbox" required type="checkbox" class="checkBoxForm formInput">
                        <label for="inputCheckboxTitre" class="textCheckBox">J'ai lu, compris et accepté. J’autorise Ed-Numérique à utiliser les données mentionnées ci-dessus, <a href="?poldeconf">dans les conditions prévues</a>.</label>
                    </div><br><br>

                    <button name="sendmail" type="submit" class="BTNcontactForm BTNmdpOublie">Envoyer</button>
                </form>
            </section>
        </section>
    </main>

    <?php include("views/footer.php") ?>
    <script type="text/javascript" src="views/assets/js/header_annuaire.js"></script>
</body>
</html>
