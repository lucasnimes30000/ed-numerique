<?php

include('controller/id.php');

if(!isset($_GET['token']) || !isset($_GET['id_user'])) {
    if(!isset($_GET['erreur'])) {
        header('Location: ?creation_mdp&token=0&id_user=0&erreur=0#erreur');
    }
} else {

    try{
        $sql_match = "SELECT statut, nom_user, prenom, mail FROM TBLuser WHERE id_user = :id_user AND token_oublie = :token";

        $req_match = $bdd->prepare($sql_match);

        $req_match->execute([
            ':id_user' => htmlspecialchars($_GET['id_user'], ENT_QUOTES),
            ':token' => htmlspecialchars($_GET['token'], ENT_QUOTES)
        ]);
        $check_match = $req_match->fetchAll();
    } catch (PDOException $e) {
        echo "Page inaccessible, les informations d'authentification sont incorrectes. Merci de vérifier avec votre administrateur. " . $e->getMessage();
        header('Location: ?creation_mdp#erreur_id_token');
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include("metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/connexion.css">
    <link rel="stylesheet" href="views/assets/css/creation_mdp.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <title>Création MDP</title>
</head>
<body>
    <?php include("views/header_annuaire.php") ?>  
    <main>
        <section class="mainBlock">
        <input type="hidden" id="id_user" name="" value="<?= htmlspecialchars($_GET['id_user'], ENT_QUOTES); ?>">
            <article class="blockDroit">
                <p class="alert_mdp" id="accord_pas_ok">Merci d'accepter les conditions générales d'utilisation</p>
                <p class="alert_mdp" id="champs_vide">Il est obligatoire de remplir les deux champs</p>
                <p class="alert_mdp" id="champs_differents">Les mots de passe sont différents</p>
                <p class="alert_mdp" id="nombre_carac_8">Un mot de passe d'au moins 8 caractères est requis</p>
                <p class="alert_mdp" id="erreur_id_token">Les informations d'authentification ne sont pas bien renseignées</p>
                <?php if(!empty($check_match) && $check_match[0]['statut'] == 0) { ?>

                <section class="txtBlock">
                    <h2 class="titreDroit">Bonjour,</h2>
                    <p>Nous vous remercions de votre inscription chez <strong>Ed-Numérique.</strong></p>
                    <p>Pour la terminer, merci de créer votre mot de passe :</p>
                    <hr><br>
                    <form method="POST" action="controller/check_mdp_creation.php" class="formBlocGauche">
                        <div>
                            <img src="views/assets/img/formulaires/mdp_icon_svg.svg" alt="Logo clef" class="logoProfil_1">
                            <input id = "input_mdp_log_2" type="password" class="mail1Input formInput" name="password" placeholder="Mot de passe">
                        </div>
                        <div>
                            <img src="views/assets/img/formulaires/mdp_icon_svg.svg" alt="Logo clef" class="logoProfil_2">
                            <input name="password2" type="password" id = "input_mdp_log" class="MDPinput formInput" placeholder="Confirmation">
                        </div>    
                        <br>

                        <input type="checkbox" name="accord_condition" id="accord_condition">
                        <label id="accord_text" for="accord_condition">J'accepte les <a href="?poldeconf">conditions générales d'utilisation</a></label>

                        <input type="hidden" name="token" value="<?= htmlspecialchars($_GET['token'], ENT_QUOTES) ?>">
                        <input type="hidden" name="id_user" value="<?= htmlspecialchars($_GET['id_user'], ENT_QUOTES) ?>">
                        <input type="hidden" name="nom" value="<?= htmlspecialchars($check_match[0]['nom_user'], ENT_QUOTES) ?>">
                        <input type="hidden" name="prenom" value="<?= htmlspecialchars($check_match[0]['prenom'], ENT_QUOTES) ?>">
                        <input type="hidden" name="mail" value="<?= htmlspecialchars($check_match[0]['mail'], ENT_QUOTES) ?>">

                        <div class="BTNs">
                            <button type="submit" class="BTNconnexion" id="btn_submit">Créer</button>
                        </div>
                    </form>
                </section>
                <?php } else { ?>
                    <p id="erreur">Les information d'authentification ne sont pas reconnues</p>
                <?php } ?>
            </article>
        </section>
    </main>
    <?php include("views/footer.php") ?>

    <script type="text/javascript" src="views/assets/js/creation_mdp.js"></script>
    <script type="text/javascript" src="views/assets/js/header_annuaire.js"></script>
</body>
</html>
<?php 

    
} 
?>