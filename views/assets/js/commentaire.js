let barreDeRechercheParticulier = document.querySelector("#recherche"); // LA BARRE DE RECHERCHE NOM/PRENOM PARTICULIER
let barreDeRechercheED = document.querySelector("#recherche2"); // LA BARRE DE RECHERCHE NOM/PRENOM ED
let choixAttenteDeValidation = document.querySelector("#choixAttenteDeValidation"); // BOUTON AFFICHAGE EN ATTENTE DE VALIDATION
let choixSuspendu = document.querySelector("#choixSuspendu"); // BOUTON AFFICHAGE SUSPENDU
let choixVisible = document.querySelector("#choixVisible"); // BOUTON AFFICHAGE VISIBLE
let choixTout = document.querySelector("#choixTout"); // BOUTON AFFICHAGE TOUT

window.onload = onload()

// FONCTION DE RECHERCHE NOM/PRENOM PARTICULIER
let searchQuerySelector = async function () {
  FetchcomV2(barreDeRechercheParticulier.value, 1);
}

// FONCTION DE RECHERCHE NOM/PRENOM ED
let searchQuerySelector2 = async function () {
  FetchcomV2(barreDeRechercheED.value, 2);
}

//FONCTION DE FETCH DES COMMENTAIRE EN FONCTION DU STATUT OU DE LA RECHERCHE
async function FetchcomV2(statut_com, recherche){
  let test = parseInt(statut_com);
  if(isNaN(test)){
    if(statut_com == "Tout"){
      const reponse = await fetch('../models/fetchCommentaire.php', {
        method: "GET"
      });
  
      if(reponse.ok){
        let answer = await reponse.json();
        HTMLComV2(answer, statut_com, recherche);
      }
    } else if(recherche == 1) {
      const reponse = await fetch('../models/fetchCommentaire.php?nom_com=' + statut_com, {
        method: "GET"
      });
  
      if(reponse.ok){
        let answer = await reponse.json();
        HTMLComV2(answer, statut_com, recherche);
      }
    } else if(recherche == 2){
      const reponse = await fetch('../models/fetchCommentaire.php?nom_ed=' + statut_com, {
        method: "GET"
      });
  
      if(reponse.ok){
        let answer = await reponse.json();
        HTMLComV2(answer, statut_com, recherche);
      }
    }
  } else {
    const reponse = await fetch('../models/fetchCommentaire.php?statut_com=' + statut_com, {
      method: "GET"
    });

    if(reponse.ok){
      let answer = await reponse.json();
      HTMLComV2(answer, statut_com, recherche);
    }
  }
}

//FONCTION DE CRÉATION DU TABLEAU EN FONCTION DU RÉSULTAT DE LA FCT FETCH
function HTMLComV2(answer, statut_com, recherche){

    document.querySelector("#table").innerHTML = "";

    answer.forEach(element => {
      if(element["statut_com"] == 0){
        statut = "En attente de validation";
      } else if(element["statut_com"] == 2){
        statut = "Suspendu";
      } else {
        statut = "Visible";
      }

      document.querySelector("#table").innerHTML += "<tr id='" + element["nom_com"].toUpperCase() + element["prenom_com"].toLowerCase() + element["statut_com"] + "'><td><p>" + element["date_com"] + "</p></td><td><p><b>" + element["nom_com"] + "</b> " + element["prenom_com"] + "</p></br>" + element["mail_com"] + "</td><td><h3>Titre: " + element["titre_com"] + "</h3></br><h4>Commentaire:</h4><p>" + element["contenu_com"] + "</p></br><button class='publier_com' value='" + element["id_com"] + "'>Publier</button> <button class='suspendre_com' value='" + element["id_com"] + "'>Suspendre</button> <button class='supprimer_com' value='" + element["id_com"] + "'>Supprimer</button> <b>" + statut + "</b></td><td id='" + element["nom_user"].toUpperCase() + element["prenom"].toLowerCase() + "'>" + element["nom_user"] + " " + element["prenom"] + "</tr>"
  });

  let supprimer_com = document.querySelectorAll(".supprimer_com"); // TOUT LES BOUTONS SUPPRIMER
  let suspendre_com = document.querySelectorAll(".suspendre_com"); // TOUT LES BOUTONS SUSPENDRE

  let publier_com = document.querySelectorAll(".publier_com"); // TOUT LES BOUTONS PUBLIER

  // ÉCOUTEUR BOUTONS SUSPENDRE
  suspendre_com.forEach(element => { // FONCTION SUSPENDRE
    element.addEventListener("click", async function(){
      const reponse = await fetch('../models/suspendreCommentaire.php?id_com=' + element.value, {
        method: "GET"
      });
  
      if (reponse.ok) {
        let answer = await reponse.text();
        if(answer === "ok") {
          let answer = await FetchcomV2(statut_com, recherche);
        }
      }
    });
  });

  // ÉCOUTEUR BOUTONS SUPPRIMER
  supprimer_com.forEach(element => {
    element.addEventListener("click", async function(){ // FONCTION SUPPRIMER
      const reponse = await fetch('../models/supprimerCommentaire.php?id_com=' + element.value, {
        method: "GET"
      });
  
      if (reponse.ok) {
        let answer = await reponse.text();
        if(answer === "ok") {
          let answer = await FetchcomV2(statut_com, recherche);
        }
      }    
    });
  });

  // ÉCOUTEUR BOUTONS PUBLIER
  publier_com.forEach(element => { // FONCTION PUBLIER
    element.addEventListener("click", async function(){
      const reponse = await fetch('../models/publierCommentaire.php?id_com=' + element.value, {
        method: "GET"
      });
  
      if (reponse.ok) {
        let answer = await reponse.text();
        if(answer === "ok") {
          let answer = await FetchcomV2(statut_com, recherche);       
        }
      }
    });
  });
}

// ÉCOUTEUR BARRE DE RECHERCHE
barreDeRechercheParticulier.addEventListener("input", searchQuerySelector);
barreDeRechercheED.addEventListener("input", searchQuerySelector2);

// ÉCOUTEUR BOUTONS D'AFFICHAGE
choixAttenteDeValidation.addEventListener("click",async function(){
  FetchcomV2(choixAttenteDeValidation.value, 0);
});

choixSuspendu.addEventListener("click",async function(){
  FetchcomV2(choixSuspendu.value, 0);
});

choixVisible.addEventListener("click",async function(){
  FetchcomV2(choixVisible.value, 0);
});

choixTout.addEventListener("click", async function(){
  FetchcomV2(choixTout.value, 0);
});

function onload() {
  FetchcomV2("Tout", 0);
}