/*

    Script JS . ed-numérique
    Gère la modification des différents textarea (côté back-office)
    + La partie dynamique de la page

*/

// Divers

let user_id_choix = document.querySelector("#user_id_choix")
let alert_box = document.querySelector("#alert")
let btn_retour = document.querySelector("#btn_retour")

// Bouttons de modification

let btn_update_connaitre = document.querySelector("#update_connaitre")
let btn_update_j_aime = document.querySelector("#update_j_aime")
let btn_update_informations = document.querySelector("#update_informations")

// Contenu des champs (textarea)

let text_connaitre = document.querySelector("#text_connaitre")
let text_j_aime = document.querySelector("#text_aime")
let text_informations = document.querySelector("#text_informations")

// Ecouteurs d'évennements des différents boutons de modification

btn_update_connaitre.addEventListener("click", update_connaitre)
btn_update_j_aime.addEventListener("click", update_j_aime)
btn_update_informations.addEventListener("click", update_informations)
btn_retour.addEventListener("click", () => { history.go(-1) })

// COMPETENCES - SAVOIR FAIRE

let liste_competence = document.querySelector("#liste_competence")

window.onload = onload()

// CONNAITRE

async function update_connaitre(){

    const reponse = await fetch('../controller/update_connaitre.php?content=' + text_connaitre.value + '&id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        if(answer === "ok") {
            alert_box.style.display = "flex"
            alert_box.innerHTML = "Me connaître : modification effectuée avec succès !"
        } else {
            alert_box.innerHTML = "Me connaître : erreur dans la modification !"
        }
    }
}

// J'AIME


async function update_j_aime() {

    const reponse = await fetch('../controller/update_j_aime.php?content=' + text_j_aime.value + '&id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        if(answer === "ok") {
            alert_box.style.display = "flex"
            alert_box.innerHTML = "J'aime : modification effectuée avec succès !"
        } else {
            alert_box.innerHTML = "J'aime : erreur dans la modification !"
        }
    }

}

// SAVOIR FAIRE


async function update_informations() {

    content_infos = text_informations.value.replace(/(\r\n|\n|\r)/gm, "");

    const reponse = await fetch('../controller/update_informations.php?content=' + text_informations.value + '&id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {
        console.log("bruh")
        let answer = await reponse.text();
        if(answer === "ok") {
            alert_box.style.display = "flex"
            alert_box.innerHTML = "Infos : modification effectuée avec succès !"
        } else {
            alert_box.innerHTML = "Infos : erreur dans la modification !"
        }
    }

}

// AFFICHER LES MATERIAUX

async function fetch_display_materiel() {

    const reponse = await fetch('../controller/afficher_materiel_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer
    

    }

}

// AFFICHER LES MATERIAUX

async function fetch_display_image_jv() {

    const reponse = await fetch('../controller/afficher_image_jv_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer
    

    }

}

// AFFICHER LES INFRASTRUCTURES

async function fetch_display_infrastructure() {

    const reponse = await fetch('../controller/afficher_infrastructure_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer
    

    }

}

// AFFICHER LES LOGICIELS ET SYSTEMES

async function fetch_display_logiciel_systeme() {

    const reponse = await fetch('../controller/afficher_logiciel_systeme_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer
    

    }

}

// AFFICHER LES DEMARCHES

async function fetch_display_demarche() {

    const reponse = await fetch('../controller/afficher_demarche_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer
    

    }

}

// FETCH COMMENTAIRE & SUPPRIMER COMMENTAIRE & SUSPENDRE COMMENTAIRE

async function fetchCommentaire(){
    const reponse = await fetch('../models/fetchCommentaire.php?id_user=' + user_id_choix.value + '&statut_com=1', {
        method: "GET"
    });

    if (reponse.ok) {
        let answer = await reponse.json();
        document.querySelector(".commentaireEd").innerHTML = "";
        answer.forEach(element => {
            document.querySelector(".commentaireEd").innerHTML += "<article><div><img src='../views/assets/img/logo.svg' alt='Logo'><b>" + element["nom_com"] + "</b><p>" + element['prenom_com'] + "</p><button value='" + element['id_com'] + "' class='supprimer_com'>Supprimer</button> <button value='" + element['id_com'] + "' class='suspendre_com'>Suspendre</button></div><div><h3>" + element['titre_com'] + "</h3><i>" + element['date_com'] + "</i><p>" + element['contenu_com'] + "</p></div></article>";
        });

        //Commentaire

        let supprimer_com = document.querySelectorAll(".supprimer_com");
        let suspendre_com = document.querySelectorAll(".suspendre_com");

        // SUPPRIMER COMMENTAIRE

        supprimer_com.forEach(element => {
            element.addEventListener("click", async function(){
                const reponse = await fetch('../models/supprimerCommentaire.php?id_com=' + element.value, {
                    method: "GET"
                });
            
                if (reponse.ok) {
                    let answer = await reponse.text();
                    if(answer === "ok") {
                        alert_box.style.display = "flex"
                        alert_box.innerHTML = "Commentaire supprimé avec succès !"
                        fetchCommentaire();
                    } else {
                        alert_box.innerHTML = "Erreur dans la suppression du commentaire !"
                    }
                }    
            });
        });

        // SUSPENDRE COMMENTAIRE

        suspendre_com.forEach(element => {
            element.addEventListener("click", async function(){
                const reponse = await fetch('../models/suspendreCommentaire.php?id_com=' + element.value, {
                    method: "GET"
                });
            
                if (reponse.ok) {
                    let answer = await reponse.text();
                    if(answer === "ok") {
                        alert_box.style.display = "flex"
                        alert_box.innerHTML = "Commentaire suspendu avec succès !"
                        fetchCommentaire()
                    } else {
                        alert_box.innerHTML = "Erreur dans la suspenssion du commentaire !"
                    }
                }
            });
        });
    }
}


//////////////////////////

// URL IMAGE ERROR GESTION

let path_url = document.querySelector("#url_id");
let PP = document.querySelector("#photo_profil_ed")

fetch('https://ed-numerique.ovh' + path_url.value, { method: 'HEAD' })
    .then(res => {
        if (res.ok) {
            console.log('Image exists.');
        } else {
            console.log("bruh")
            PP.src = "../views/assets/img/annuaire/icone_ed.png"
        }
    }).catch(err => console.log('Error:', err));


// AU DEMARRAGE

function onload() {
    fetch_display_materiel()
    fetch_display_image_jv()
    fetch_display_logiciel_systeme()
    fetch_display_demarche()
    fetch_display_infrastructure()
    fetchCommentaire()
}