let BTN_accueil = document.querySelector("#BTN_accueil")
let svg_accueil = document.querySelector("#svg_accueil")
let svg_ville = document.querySelector(".svg_ville")
let BTN_ville = document.querySelector("#BTN_ville")
let VILLES = document.querySelector(".VILLES")
let dropdown = document.querySelector(".dropdown")
let dropdowncontent = document.querySelector(".dropdown-content")
let header_logo = document.querySelector("#header_logo")
let ACCUEIL = document.querySelector("#ACCUEIL")
let accueil_link_svg = document.querySelector("#accueil_link")

window.onload = onload();

function retour_btn() {
	ACCUEIL.innerHTML = "RETOUR"
	accueil_link_svg.style.display = "none"
}

BTN_ville.addEventListener("click", () => {
	VILLES.classList.toggle("VILLES_blue");
	svg_ville.classList.toggle("svg_ville_blue");
	dropdown.classList.toggle("dropdown_white")
	dropdowncontent.classList.toggle("dropdown-content-block")
})

BTN_accueil.addEventListener("mouseover", () => {
	svg_accueil.setAttribute("fill", "white")
})

BTN_accueil.addEventListener("mouseout", () => {
	svg_accueil.setAttribute("fill", "#258989")
})

BTN_accueil.addEventListener("click", () => {
	if(document.title === "Fiche ed") {
	  history.go(-1);
	} else {
	  window.location.href = "?accueil"
	}
})

header_logo.addEventListener("click", () => {
	window.location.href = "?accueil"
})


async function fetch_afficher_header_ville() {
    const reponse = await fetch('controller/afficher_header_ville.php', {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
		dropdowncontent.innerHTML = answer;
    }
}

// AU DEMARRAGE

function onload() {
	fetch_afficher_header_ville()
	if(document.title === "Fiche ed") retour_btn()
}
