let vignette = document.querySelector("#vignettes")
let id_ville = document.querySelector(".id_ville")
let ville = document.querySelector(".ville")
let ville_get = document.querySelector(".ville_get")

localStorage.setItem("current_ville", ville_get.value)

window.onload = onload();

async function fetch_display_annuaire() {
    const reponse = await fetch('controller/annuaire_generation.php?id_ville=' + id_ville.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
		vignette.innerHTML = answer;
		let ville = document.querySelectorAll(".ville")
		ville.forEach(element => {
			element.innerHTML = ville_get.value
		})


		// URL IMAGE ERROR GESTION

		let PP = document.querySelectorAll(".img_profil")

		PP.forEach(element => {
			element.addEventListener("error", () => {
				element.src = "views/assets/img/annuaire/icone_ed.png";
			})
		})


		/////////////////////////////
		
		let btn_telephone = document.querySelectorAll(".hidden_number")

		btn_telephone.forEach(element => {
			if(localStorage.getItem(element.value + "_phone") === "phone_locked") {
				fetch_number(element.value, element)
				element.style.color = "#258989"
				element.style.background = "white"
			}
			element.addEventListener('click', () => {
				if(localStorage.getItem(element.value + "_phone") !== "phone_locked") {
					fetch_number(element.value, element)
					fetch_click_tracking_telephone(element.value)
					localStorage.setItem(element.value + "_phone", 'phone_locked')
				}
			})
		})

		let selected_ville = document.querySelectorAll(".selected_ville")

		selected_ville.forEach(element => {
			// Une fois l'accueil terminé il faudra que la value de la ville soit son ID
			element.value = ville_get.value
		})

		let display_profil = document.querySelectorAll(".see_profil")

		display_profil.forEach(element => {
			element.addEventListener("click", () => {
				if(localStorage.getItem(element.value + "_profil") !== "profil_locked") {
					fetch_click_tracking_profil(element.value)
					localStorage.setItem(element.value + "_profil", 'profil_locked')
				}
				window.location.href = "?fiche_ed&id_user=" + element.value
			})
		})


		//////////////////////////


		// fetch('http://localhost/lamp/ed-numerique' + element.value, { method: 'HEAD' })
		// .then(res => {
		// 	if (res.ok) {
		// 		console.log('Image exists.');
		// 	} else {
		// 		PP.src = "../views/assets/img/annuaire/icone_ed.png"
		// 	}
		// }).catch(err => console.log('Error:', err));
    }
	
}

async function fetch_number(id_user, element) {
    const reponse = await fetch('controller/display_number.php?id_user=' + id_user, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
		element.innerHTML = answer
		element.style.color = "#258989"
		element.style.background = "white"
    }
}

let bruh = new Date();

async function fetch_click_tracking_telephone(id_user) {
    const reponse = await fetch('controller/click_tracking_telephone.php?id_user=' + id_user + '&mois=' + bruh.getMonth() + '&annee=' + bruh.getFullYear(), {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
		console.log(answer)
    }
}

async function fetch_click_tracking_profil(id_user) {
    const reponse = await fetch('controller/click_tracking_profil.php?id_user=' + id_user + '&mois=' + bruh.getMonth() + '&annee=' + bruh.getFullYear(), {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
		console.log(answer)
    }
}

//////////////////////////


function onload() {
	fetch_display_annuaire()
}