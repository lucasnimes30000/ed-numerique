let BtnClient = document.querySelector("#BtnClient");
let BtnEd = document.querySelector("#BtnEd");
let coll = document.getElementsByClassName("collapsible");
let client = document.querySelector("#client");
let intervenantEd = document.querySelector("#intervenantEd");
let i;

BtnClient.addEventListener("click", function() {
  client.style.display = "flex";
  BtnClient.style.backgroundColor = "#E7E7E7";
  intervenantEd.style.display = "none";
  BtnEd.style.backgroundColor = "white";
})

BtnEd.addEventListener("click", function() {
  client.style.display = "none";
  BtnClient.style.backgroundColor = "white";
  intervenantEd.style.display = "flex";
  BtnEd.style.backgroundColor = "#E7E7E7";
})

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    let content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}