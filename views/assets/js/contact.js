let btn_envoie_modal = document.querySelector("#envoiContact")
let input_mod = document.querySelectorAll(".globalInput")
let label_mod = document.querySelector(".textCheckBox")
let contact_form_ed = document.querySelector(".formBlocGauche")
let check_form = []

function isMail(email){
    let mailRegex = /^([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/i;
    let regex = new RegExp(mailRegex);
    return email.match(regex);
}
function isFrenchNumber(number){
    let phoneNumberRegex = /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/g;
    let regex = new RegExp(phoneNumberRegex);
    return number.match(regex);
}

function isCodePostal(cp) {
    let codePostalRegex = /[0-9]{5}/g;
    let regex = new RegExp(codePostalRegex);
    return cp.match(regex);
}

function error_input(element, event) {
    element.style.border = "2px solid #Ec1d1d"
    check_form.push("empty")
    element.nextElementSibling.style.display = "block"
    element.nextElementSibling.style.marginBottom = "1rem"
    element.style.marginBottom = "0.5rem"
	event.preventDefault();
}

function send(e,form) {
    fetch(form.action, {
    	method:'post', body: new FormData(form)
    });    
    e.preventDefault();
}

btn_envoie_modal.addEventListener("click", (event) => {

    check_form = []

    input_mod.forEach(element => {

        let valueInput = element.value

        switch (element.type) {
            case "text":
                if(element.id === "cp"){      
                    let checkCodePostal = isCodePostal(valueInput);           
                    if(checkCodePostal == null){
                        error_input(element, event)
                    } else {
                        element.style.border = "1px solid #35353550"
                        element.nextElementSibling.style.display = "none"
                    }
                } else if(element.id === "prenom"){               
                    if(valueInput === ""){
                        error_input(element, event)
                    } else {
                        element.style.border = "1px solid #35353550"
                        element.nextElementSibling.style.display = "none"
                    }
                } else if(element.id === "nom"){               
                    if(valueInput === ""){
                        error_input(element, event)
                    } else {
                        element.style.border = "1px solid #35353550"
                        element.nextElementSibling.style.display = "none"
                    }
                } else if(element.id === "telephone"){      
                    let checkFrenchNumber = isFrenchNumber(valueInput);           
                    if(checkFrenchNumber == null){
                        error_input(element, event)
                    } else {
                        element.style.border = "1px solid #35353550"
                        element.nextElementSibling.style.display = "none"
                    }
                }
                break;
            case "email":
                let check_mail = isMail(valueInput)
                if(valueInput === "" || check_mail == null) {
                    error_input(element, event)
                } else {
                    element.style.border = "1px solid #35353550"
                    element.nextElementSibling.style.display = "none"
                }
                break;
            case "textarea":
                if(valueInput === "") {
                    element.style.border = "2px solid #Ec1d1d"
                    check_form.push("empty")
                    element.nextElementSibling.style.display = "block"
                    element.nextElementSibling.style.marginBottom = "1rem"
                    element.style.marginBottom = "0.5rem"
					event.preventDefault();
                } else {
                    element.style.border = "1px solid #35353550"
                    element.nextElementSibling.style.display = "none"
                }
                break;
            case "checkbox":
                if(element.checked == false) {
                    label_mod.style.color = "#Ec1d1d"
                    check_form.push("unchecked")
					event.preventDefault();
                } else if (element.checked == true) {
                    label_mod.style.color = "#35353599"
                }
                break;
            default:
                break;
        }
    })


    if(check_form.length === 0) {
		send(event, contact_form_ed)
        input_mod.forEach(element => { element.value = "" })
		document.querySelector("#globalOK").style.display = "block"
		document.querySelector("#globalErreur").style.display = "none"
    } else {
		document.querySelector("#globalOK").style.display = "none"
		document.querySelector("#globalErreur").style.display = "block"
		return;
    }

});
