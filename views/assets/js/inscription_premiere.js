// window.onload(console.log("bruh"));

let nom = document.querySelector("#nom");
let prenom = document.querySelector("#prenom");
let mail = document.querySelector("#mail");
let mail_confirmation = document.querySelector("#mail_confirmation")
let telephone = document.querySelector("#telephone");

let deuxieme_partie = document.querySelector("#deuxieme_partie");
let premiere_partie = document.querySelector(".premiere_partie");
let premiere_partie_id = document.querySelector("#premiere_partie");

let premiere_OK = document.querySelector("#premiere_OK")

let BTN_display_second_part = document.querySelector("#BTN_display_second_part");

let alerte_box = document.querySelector("#alerte-box")
let label_mail = document.querySelector("#label_mail")
let label_confirmation_mail = document.querySelector("#label_confirmation_mail")



// Première partie inscription

BTN_display_second_part.addEventListener("click", fetch_first_part)

async function fetch_first_part() {

    const reponse = await fetch('../controller/inscription_first.php?nom=' + nom.value + '&prenom=' + prenom.value + '&telephone=' + telephone.value + '&mail=' + mail.value + '&mail_confirmation=' + mail_confirmation.value, {
        method: "GET"
    });

    if (reponse.ok) {

        // premiere_partie.style.display = "none";
        // premiere_partie_id.style.display = "none";

        // requête fetch pour créer le nouvel utilisateur
        // puis affichage de la seconde partie du formulaire d 'inscription

        // deuxieme_partie.style.display = "block";

        let answer = await reponse.text()

        if (answer === "Création effectuée avec succès") {
            window.location.href = '../controller/transition_second.php?nom=' + nom.value + '&prenom=' + prenom.value + '&mail=' + mail.value
        } else if (answer === "Tous les champs n'ont pas correctement été renseignés") {
            alerte_box.innerHTML = answer
            alerte_box.style.display = "flex"
            label_mail.style.display = "none"
            label_confirmation_mail.style.display = "none"
            mail.style.border = "1px solid black"
            mail_confirmation.style.border = "1px solid black"

        } else if (answer === "Ce compte utilisateur existe déjà") {
            alerte_box.innerHTML = answer
            alerte_box.style.display = "flex"
            label_mail.style.display = "none"
            label_confirmation_mail.style.display = "none"
            mail.style.border = "1px solid black"
            mail_confirmation.style.border = "1px solid black"

        } else if (answer === "Les adresses mail sont différentes") {
            alerte_box.innerHTML = answer
            alerte_box.style.display = "flex"

            label_mail.style.display = "block"
            label_mail.style.color = "red"
            label_mail.style.fontweight = "bold"
            label_confirmation_mail.style.display = "block"
            label_confirmation_mail.style.color = "red"
            label_confirmation_mail.style.fontweight = "bold"
            mail.style.border = "2.5px solid red"
            mail_confirmation.style.border = "2.5px solid red"

        } else {
            alerte_box.innerHTML = answer
            alerte_box.style.display = "flex"

            label_mail.style.display = "none"
            label_confirmation_mail.style.display = "none"
            mail.style.border = "1px solid black"
            mail_confirmation.style.border = "1px solid black"
        }



    }
}