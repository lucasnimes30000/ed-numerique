/*

    Script JS . ed-numérique
    Gère la modification des différents textarea (côté back-office)
    + La partie dynamique de la page

*/

// Divers

let phone_number = document.querySelector("#phone_number")
let btn_see_number = document.querySelector("#see_number")
let droite = document.querySelector(".droite")
let user_id_choix = document.querySelector("#user_id_choix")
let btn_retour = document.querySelector(".btn_retour")
let contact_ed = document.querySelector("#contact_ed")

droite.innerHTML = localStorage.getItem("current_ville")

// Contenu des champs (textarea)

let text_connaitre = document.querySelector("#text_connaitre")
let text_j_aime = document.querySelector("#text_aime")
let text_informations = document.querySelector("#text_informations")

// Ecouteurs d'évennements des différents boutons de modification

// btn_retour.addEventListener("click", () => { history.go(-1) })

// Gestion du numéro de téléphone en localstorage

if(localStorage.getItem(user_id_choix.value + "_phone") === "phone_locked") {
    fetch_number(user_id_choix.value, phone_number)
    phone_number.className = "italic"
}

btn_see_number.addEventListener('click', () => {
    if(localStorage.getItem(user_id_choix.value + "_phone") !== "phone_locked") {
        fetch_number(user_id_choix.value, phone_number)
        fetch_click_tracking_telephone(user_id_choix.value)
        localStorage.setItem(user_id_choix.value + "_phone", 'phone_locked')
    }
})

async function fetch_number(id_user) {
    const reponse = await fetch('controller/display_number.php?id_user=' + id_user, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
		phone_number.innerHTML = answer
    }
}

let bruh = new Date();

async function fetch_click_tracking_telephone(id_user) {
    const reponse = await fetch('controller/click_tracking_telephone.php?id_user=' + id_user + '&mois=' + bruh.getMonth() + '&annee=' + bruh.getFullYear(), {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
		console.log(answer)
    }
}

// TRACKING EMAIL BUTTON

let btn_mail = document.querySelector("#btn_mail")

btn_mail.addEventListener('click', () => {
    if(localStorage.getItem(user_id_choix.value + "_mail") !== "mail_locked") {
        fetch_click_tracking_mail(user_id_choix.value)
        localStorage.setItem(user_id_choix.value + "_mail", 'mail_locked')
    }
    window.location.href = "#"
})


async function fetch_click_tracking_mail(id_user) {
    const reponse = await fetch('controller/click_tracking_mail.php?id_user=' + id_user + '&mois=' + bruh.getMonth()+ '&annee=' + bruh.getFullYear(), {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
		console.log(answer)
    }
}

// COMPETENCES - SAVOIR FAIRE

let liste_competence = document.querySelector(".ed")

window.onload = onload()

// AFFICHER LES MATERIAUX

async function fetch_display_materiel() {

    const reponse = await fetch('controller/afficher_materiel_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer

    }

}

// AFFICHER LES MATERIAUX

async function fetch_display_image_jv() {

    const reponse = await fetch('controller/afficher_image_jv_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer

    }

}

// AFFICHER LES INFRASTRUCTURES

async function fetch_display_infrastructure() {

    const reponse = await fetch('controller/afficher_infrastructure_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer

    }

}

// AFFICHER LES LOGICIELS ET SYSTEMES

async function fetch_display_logiciel_systeme() {

    const reponse = await fetch('controller/afficher_logiciel_systeme_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer

    }

}

// AFFICHER LES DEMARCHES

async function fetch_display_demarche() {

    const reponse = await fetch('controller/afficher_demarche_fiche.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        let answer = await reponse.text();
        liste_competence.innerHTML += answer

    }

}

// BOX CONTACT ED

let btn_close_contact = document.querySelector("#Fermer")

btn_mail.addEventListener("click", () => {
    contact_ed.style.display = "flex"
})

btn_close_contact.addEventListener("click", () => {
    contact_ed.style.display = "none"
})

//////////////////////////

// URL IMAGE ERROR GESTION

// let path_url = document.querySelector("#url_id");
// let PP = document.querySelector("#PP")

// fetch('https://ed-numerique.ovh' + path_url.value, { method: 'HEAD' })
//     .then(res => {
//         if (res.ok) {
//             console.log('Image exists.');
//         } else {
//             console.log("bruh")
//             PP.src = "../views/assets/img/annuaire/icone_ed.png"
//         }
//     }).catch(err => console.log('Error:', err));

// FENÊTRE MODALE 

    // Récuperer la fenêtre
let modal = document.getElementById("myModal");

    // Récup bouton qui ouvre la fenêtre
let btnContact = document.querySelector(".myBtn");

    // Récup l'élemnt qui la ferme
let span = document.getElementsByClassName("close")[0];

    // Quand l'utilisateur clique sur le bouton ouvrir la fenêtre

btnContact.addEventListener("click", function dropDown() {
        
        if(modal.style.display === "none"){
            modal.style.display = "block";
        } else {
            modal.style.display ="none";
        }
    
});


    // Quand l'utilisateur clique sur la corix la fenêtre se ferme
span.onclick = function() {
  modal.style.display = "none";
}

    // Quand l'utilisateur clique en dehors de la fenêtre elle se ferme
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

//FENÊTRE MODAL MESSAGE BIEN ENVOYE

    // Récuperer la fenêtre
let modalCheck = document.querySelector(".modal2");
let modalBox = document.querySelector("#modal2")

    // Récup l'élemnt qui la ferme
let croix = document.querySelector(".close2");

    // Quand l'utilisateur clique sur le bouton ouvrir la fenêtre

croix.addEventListener("click", function openCheck() {

    console.log(modalCheck)

    modalCheck.style = "display: none;"; 

});

let btn_envoie_modal = document.querySelector("#envoiMod")
let input_mod = document.querySelectorAll(".inputJs")
let textarea_mod = document.querySelector("#messageMod")
let label_mod = document.querySelector(".labelMod")
let contact_form_ed = document.querySelector("#contact_form_ed")
let check_form = []

function isMail(email){
    let mailRegex = /^([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/i;
    let regex = new RegExp(mailRegex);
    return email.match(regex);
}
function isFrenchNumber(number){
    let phoneNumberRegex = /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/g;
    let regex = new RegExp(phoneNumberRegex);
    return number.match(regex);
}

function isCodePostal(cp) {
    let codePostalRegex = /[0-9]{5}/g;
    let regex = new RegExp(codePostalRegex);
    return cp.match(regex);
}

function error_input(element) {
    element.style.borderBottom = "2px solid #Ec1d1d"
    check_form.push("empty")
    element.nextElementSibling.style.display = "block"
    element.nextElementSibling.style.marginBottom = "1rem"
    element.style.marginBottom = "0.5rem"
}

function send(e,form) {
    fetch(form.action, {
    	method:'post', body: new FormData(form)
    });    
    e.preventDefault();
}

btn_envoie_modal.addEventListener("click", (event) => {

    check_form = []

    input_mod.forEach(element => {

        let valueInput = element.value

        switch (element.type) {
            case "text":
                if(element.id === "cpMod"){      
                    let checkCodePostal = isCodePostal(valueInput);           
                    if(checkCodePostal == null){
                        error_input(element)
                    } else {
                        element.style.borderBottom = "1px solid #35353550"
                        element.nextElementSibling.style.display = "none"
                    }
                } else if(element.id === "prenomMod"){               
                    if(valueInput === ""){
                        error_input(element)
                    } else {
                        element.style.borderBottom = "1px solid #35353550"
                        element.nextElementSibling.style.display = "none"
                    }
                } else if(element.id === "nomMod"){               
                    if(valueInput === ""){
                        error_input(element)
                    } else {
                        element.style.borderBottom = "1px solid #35353550"
                        element.nextElementSibling.style.display = "none"
                    }
                } else if(element.id === "phoneMod"){      
                    let checkFrenchNumber = isFrenchNumber(valueInput);           
                    if(checkFrenchNumber == null){
                        error_input(element)
                    } else {
                        element.style.borderBottom = "1px solid #35353550"
                        element.nextElementSibling.style.display = "none"
                    }
                }
                break;
            case "email":
                let check_mail = isMail(valueInput)
                if(valueInput === "" || check_mail == null) {
                    error_input(element)
                } else {
                    element.style.borderBottom = "1px solid #35353550"
                    element.nextElementSibling.style.display = "none"
                }
                break;
            case "textarea":
                if(valueInput === "") {
                    element.style.border = "2px solid #Ec1d1d"
                    check_form.push("empty")
                    element.nextElementSibling.style.display = "block"
                    element.nextElementSibling.style.marginBottom = "1rem"
                    element.style.marginBottom = "0.5rem"
                } else {
                    element.style.border = "1px solid #35353550"
                    element.nextElementSibling.style.display = "none"
                }
                break;
            case "checkbox":
                if(element.checked == false) {
                    label_mod.style.color = "#Ec1d1d"
                    check_form.push("unchecked")
                } else if (element.checked == true) {
                    label_mod.style.color = "#35353599"
                }
                break;
            default:
                break;
        }
    })


    if(check_form.length === 0) {
        send(event, contact_form_ed)
        modal.style.display = "none"
        modalBox.style.display = "block"
        setTimeout(function(){ modalBox.style.display = "none" }, 3000)
        input_mod.forEach(element => { element.value = "" })
    } else {
    	event.preventDefault();
    }

});




//AFFICHE LE FORMULAIRE DE COMMENTAIRE QUAND ON CLIQUE SUR LE BOUTON
document.querySelector("#BTNCommentaire").addEventListener("click", function(){
        
    if(document.querySelector("#ModalCommentaire").style.display === "none"){
        document.querySelector("#ModalCommentaire").style.display = "block";
    } else {
        document.querySelector("#ModalCommentaire").style.display ="none";
    }
    
});

//FERME LE FORMULAIRE AU CLICK SUR LA CROIX
document.querySelector("#closeCom").onclick = function() {
    document.querySelector("#ModalCommentaire").style.display = "none";
}

//FERME LE FORMULAIRE AU CLICK EN DEHORS DU FORMULAIRE 
window.onclick = function(event) {
  if (event.target == document.querySelector("#ModalCommentaire")) {
    document.querySelector("#ModalCommentaire").style.display = "none";
  }
}

let inputPrenomCommentaire = document.querySelector("#inputPrenomCommentaire");
let inputNomCommentaire = document.querySelector("#inputNomCommentaire");
let inputTitreCommentaire = document.querySelector("#inputTitreCommentaire");
let inputEmailCommentaire = document.querySelector("#inputEmailCommentaire");
let inputMessageCommentaire = document.querySelector("#inputMessageCommentaire");
let inputConditionCommentaire = document.querySelector("#inputConditionCommentaire");
// let recaptcha = document.querySelector("#inputRecaptchaCommentaire");
let labelModCom = document.querySelector(".labelModCom");

document.querySelector("#BTNenvoyer_commentaire").addEventListener("click", async function(){
    let i = 0;

    if(inputPrenomCommentaire.value === ""){
        i++;
        error_input(inputPrenomCommentaire);
    } else {
        inputPrenomCommentaire.style.borderBottom = "1px solid #35353550";
        inputPrenomCommentaire.nextElementSibling.style.display = "none";
    }

    if(inputNomCommentaire.value === ""){
        i++;
        error_input(inputNomCommentaire);
    } else {
        inputNomCommentaire.style.borderBottom = "1px solid #35353550";
        inputNomCommentaire.nextElementSibling.style.display = "none";
    }

    if(inputTitreCommentaire.value === ""){
        i++;
        error_input(inputTitreCommentaire);
    } else {
        inputTitreCommentaire.style.borderBottom = "1px solid #35353550";
        inputTitreCommentaire.nextElementSibling.style.display = "none";
    }

    if(inputEmailCommentaire.value === ""){
        i++;
        error_input(inputEmailCommentaire);
    } else {
        let email = isMail(inputEmailCommentaire.value);
        if(email == null){
            i++;
            error_input(inputEmailCommentaire);
        } else {
            inputEmailCommentaire.style.borderBottom = "1px solid #35353550"
            inputEmailCommentaire.nextElementSibling.style.display = "none"
        }
    }

    if(inputMessageCommentaire.value === ""){
        i++;
        inputMessageCommentaire.style.border = "2px solid #Ec1d1d"
        inputMessageCommentaire.nextElementSibling.style.display = "block";
        inputMessageCommentaire.nextElementSibling.style.marginBottom = "1rem";
        inputMessageCommentaire.style.marginBottom = "0.5rem";
    } else {
        inputMessageCommentaire.style.border = "1px solid #35353550"
        inputMessageCommentaire.nextElementSibling.style.display = "none"
    }

    if(inputConditionCommentaire.checked == false) {
        labelModCom.style.color = "#Ec1d1d";
        i++;
    } else {
        labelModCom.style.color = "#35353599";
    }

    if(i == 0){
        const reponse = await fetch('models/nouveauCom.php?titre_com=' + inputTitreCommentaire.value + '&nom_com=' + inputNomCommentaire.value + '&prenom_com=' + inputPrenomCommentaire.value + '&mail_com=' + inputEmailCommentaire.value + '&contenu_com=' + inputMessageCommentaire.value + '&id_user=' + user_id_choix.value , {
            method: "GET"
        });
    
        if (reponse.ok) {
    
            let answer = await reponse.text();
            document.querySelector("#ModalCommentaire").style.display ="none";
            document.querySelector(`div[id=modal2] > div > h4`).innerHTML = "";
            document.querySelector(`div[id=modal2] > div > h4`).innerHTML = answer;
            modalBox.style.display = "block";
        }
    }
})




// AU DEMARRAGE

function onload() {

    fetch_display_materiel()
    fetch_display_image_jv()
    fetch_display_logiciel_systeme()
    fetch_display_demarche()
    fetch_display_infrastructure()

}
