let nousTrouver = document.querySelector("#NousTrouver");
let fermer = document.querySelector("#Fermer");
let plus = document.querySelector("#plus");
let moins = document.querySelector("#moins");
let sectionFooter = document.querySelector("#sectionFooter");
let mobile = document.querySelector('#mobile');

function Animate(){
    let Animation = document.getElementById("EaseIn");
    Animation.style.display = "flex";
    plus.style.display = "none";
    moins.style.display = "block";
    Animation.className = "Animate";
}

function AnimateClose(){
    let Animation = document.getElementById("EaseIn");
    Animation.className = "AnimateClose";
    setTimeout(() => {
        Animation.style.display = "none";
    }, 1000);

}

function FooterTel(){
    let Animation = document.getElementById("sectionFooter");
    Animation.classList.toggle("toggleFooter");
    
    if(plus.style.display == "block"){
        plus.style.display = "none";
        moins.style.display = "block";
    } else {
        plus.style.display = "block";
        moins.style.display = "none";
    }
    window.scrollTo(0, document.body.scrollHeight);
}

nousTrouver.addEventListener("click", Animate);
fermer.addEventListener("click", AnimateClose);

if (window.matchMedia("(max-width: 670px)").matches) {
    mobile.addEventListener("click", FooterTel);
}

// ARROW

// Set a variable for our button element.
const scrollToTopButton = document.getElementById('js-top');

// Let's set up a function that shows our scroll-to-top button if we scroll beyond the height of the initial window.
const scrollFunc = () => {
  // Get the current scroll value
  let y = window.scrollY;
  
  // If the scroll value is greater than the window height, let's add a class to the scroll-to-top button to show it!
  if (y > 0) {
    scrollToTopButton.className = "top-link show";
  } else {
    scrollToTopButton.className = "top-link hide";
  }
};

window.addEventListener("scroll", scrollFunc);

const scrollToTop = () => {
  // Let's set a variable for the number of pixels we are from the top of the document.
  const c = document.documentElement.scrollTop || document.body.scrollTop;
  
  // If that number is greater than 0, we'll scroll back to 0, or the top of the document.
  // We'll also animate that scroll with requestAnimationFrame:
  // https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame
  if (c > 0) {
    window.requestAnimationFrame(scrollToTop);
    // ScrollTo takes an x and a y coordinate.
    // Increase the '10' value to get a smoother/slower scroll!
    window.scrollTo(0, c - c / 10);
  }
};

// When the button is clicked, run our ScrolltoTop function above!
scrollToTopButton.onclick = function(e) {
  e.preventDefault();
  scrollToTop();
}