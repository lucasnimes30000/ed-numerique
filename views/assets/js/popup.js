let btn_switch = document.querySelector("#btn_switch");
let btn_switchey = document.querySelector("#btn_switchey");
let contenu_1 = document.querySelector("#contenu_1");
let contenu_2 = document.querySelector("#contenu_2");
let btn_accepter = document.querySelector("#btn_accepter");
let btn_acceptey = document.querySelector("#btn_acceptey");
let popup_cookie = document.querySelector("#popup_cookie");
let blure = document.querySelector("#blur");

window.onload = onload();

btn_switch.addEventListener("click", plusInfo);
btn_switchey.addEventListener("click", moinsInfos);
btn_accepter.addEventListener("click", setCookie);
btn_acceptey.addEventListener("click", setCookie);



async function plusInfo(){
    contenu_1.style.display = "none";
    contenu_2.style.display = "flex";
}

async function moinsInfos(){
    contenu_2.style.display = "none";
    contenu_1.style.display = "flex";
}

function setCookie(){
    document.cookie = "RGPD = accepted";
    verifCookie();
}

async function verifCookie(){
    const issetCookie = getCookie("RGPD");

    if(issetCookie == "accepted"){
        popup_cookie.style.display = "none";
        blure.style.display = "none";
    }
}

function getCookie(name){
    let cookieArr = document.cookie.split(";");
    
    for(let i = 0; i < cookieArr.length; i++) {
        let cookiePair = cookieArr[i].split("=");
        
        if(name == cookiePair[0].trim()) {
            return cookiePair[1];
        }
    }
    
    return null;
}

function onload(){
    verifCookie();
}