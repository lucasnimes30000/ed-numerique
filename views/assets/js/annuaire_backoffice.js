let BTN_profil = document.querySelectorAll(".BTN_profil")

BTN_profil.forEach(element => {
    element.addEventListener("click", () => {
        window.location.href = "profil_ed_backoffice.php?id_user=" + element.value
    })
})


// URL IMAGE ERROR GESTION

let PP = document.querySelectorAll(".photo_profil_ed")

PP.forEach(element => {
    element.addEventListener("error", () => {
        element.src = "../views/assets/img/annuaire/icone_ed.png";
    })
})