// DIVERS

let id_user = document.querySelector("#id_user")
let content = document.querySelector("#content")
let h2_page = document.querySelector("#h2_page")

// BOUTONS

let btn_contrat = document.querySelector("#btn_contrat")
let btn_fiche = document.querySelector("#btn_fiche")
let btn_mdp = document.querySelector("#btn_mdp")
let btn_deconnexion = document.querySelector("#btn_deconnexion")

// ECOUTEURS D'ÉVENNEMENTS

btn_fiche.addEventListener("click", () => {
    window.location.href = "?fiche_ed&id_user=" + id_user.value
})
btn_contrat.addEventListener("click", fetch_display_contrat)
btn_mdp.addEventListener("click", fetch_display_mdp_form)
btn_deconnexion.addEventListener("click", () => {
    window.location.href = "?logout"
})

window.onload = onload()


async function fetch_display_contrat() {
    content.innerHTML = "<table><thead><tr id='trth'><th id='date' class='header'>Date d'ajout</th><th id='nom_docu' class='header'>Nom du document</th><th id='dl' class='header'>Télécharger</th></tr></thead><tbody id='content_table'></tbody></table>"

    const reponse = await fetch('controller/display_contrat.php?id_user=' + id_user.value, {
        method: "GET"
    });

    if (reponse.ok) {
        let content_table = document.querySelector("#content_table")

        let answer = await reponse.text();
        content_table.innerHTML += answer
        h2_page.innerHTML = "Mes contrats"

    }

}

async function fetch_display_mdp_form() {
    content.innerHTML = "<article id='content_form'></article>"

    const reponse = await fetch('controller/display_mdp_form.php?id_user=' + id_user.value, {
        method: "GET"
    });

    if (reponse.ok) {
        let content_form = document.querySelector("#content_form")

        let answer = await reponse.text();
        content_form.innerHTML = answer

        content_form.style.display = 'flex';
        h2_page.innerHTML = "Changer mon mot de passe"

        // INPUTS

        let old_password = document.querySelector("#old_password")
        let new_password = document.querySelector("#new_password")
        let new_password_2 = document.querySelector("#new_password_2")
        let btn_password_submit = document.querySelector("#btn_password")
        let alert_password = document.querySelector("#alert_password")

        btn_password_submit.addEventListener("click", fetch_change_password)

    }

}

async function fetch_change_password() {
    const reponse = await fetch('controller/change_password.php?id_user=' + id_user.value + '&old=' + old_password.value + '&new_1=' + new_password.value + '&new_2=' + new_password_2.value, {
        method: "GET"
    });

    if (reponse.ok) {
        let input_password = document.querySelectorAll(".input_password")
        let answer = await reponse.text();
        console.log(answer)

        switch(answer) {
            case "0":
                alert_password.style.background = "#ce4b4b44"
                alert_password.innerHTML = "Un ou plusieurs champs vide(s)";
                alert_password.style.display = "flex"

                input_password.forEach(input => {
                    if(input.value === "") {
                        input.style.border = "2px solid rgb(173, 15, 15)"
                    } else {
                        input.style.border = "1px solid black"
                    }
                })
                break;
            case "1":
                input_password.forEach(input => {
                    input.style.border = "1px solid black" 
                })

                new_password.style.border = "2px solid rgb(173, 15, 15)"
                new_password_2.style.border = "2px solid rgb(173, 15, 15)"

                alert_password.style.background = "#ce4b4b44"
                alert_password.innerHTML = "Mots de passe différents";
                alert_password.style.display = "flex"

                break;
            case "2":
                input_password.forEach(input => {
                    input.style.border = "1px solid black" 
                })

                old_password.style.border = "2px solid rgb(173, 15, 15)"

                alert_password.style.background = "#ce4b4b44"
                alert_password.innerHTML = "Ancien mot de passe erroné";
                alert_password.style.display = "flex"
                break;
            case "3":
                input_password.forEach(input => {
                    input.style.border = "1px solid black" 
                })

                new_password.style.border = "2px solid rgb(173, 15, 15)"
                new_password_2.style.border = "2px solid rgb(173, 15, 15)"

                alert_password.style.background = "#ce4b4b44"
                alert_password.innerHTML = "Minimum 8 caractères requis";
                alert_password.style.display = "flex"
                break;
            case "4":
                input_password.forEach(input => {
                    input.style.border = "1px solid black" 
                })

                new_password.style.border = "2px solid rgb(173, 15, 15)"
                new_password_2.style.border = "2px solid rgb(173, 15, 15)"

                alert_password.style.background = "#ce4b4b44"
                alert_password.innerHTML = "Impossible d'utiliser à nouveau le même mot de passe";
                alert_password.style.display = "flex"
                break;
            case "5":
                input_password.forEach(input => {
                    input.style.border = "1px solid black" 
                })

                alert_password.style.background = "#4d9e3f44"
                alert_password.innerHTML = "Modification effectuée avec succès !";
                alert_password.style.display = "flex"
                break;
            default:
                break;
        }

    }

}


//////////////////////////

// URL IMAGE ERROR GESTION

let path_url = document.querySelector("#url_id");
let PP = document.querySelector("#PP")
let nom_prenom = document.querySelector("#nom_prenom");

fetch('https://ed-numerique.ovh' + path_url.value, { method: 'HEAD' })
    .then(res => {
        if (res.ok) {
            console.log('Image exists.');
        } else {
            PP.src = "views/assets/img/annuaire/icone_ed.png"
            PP.style = "border: solid red;";
            nom_prenom.insertAdjacentHTML('afterend', "<i style='color: red;'>(pensez à ajouter votre photo)</i>");
        }
    }).catch(err => console.log('Error:', err));



// #4d9e3f44 vert
// #ce4b4b44 rouge
function onload() {
    fetch_display_contrat()
}