/*

    Script JS . ed-numérique
    Gère la partie fetch() de la page profil des ed (côté back-office)
    + La partie dynamique de la page

*/

// DIVERS
let main_html     = document.querySelector("main")
let user_id_choix = document.querySelector("#user_id_choix")
let body_choix    = document.querySelector("#body_choix")

// MAIL INFORMATIONS
let mail_user = document.querySelector("#mail_user")
let prenom_user = document.querySelector("#prenom_user")
let nom_user = document.querySelector("#nom_user")

// STATUT
let creer_statut_alert      = document.querySelector("#creer_statut_alert")
let statut_alert            = document.querySelector("#statut_alert")
let creer_statut_form       = document.querySelector("#creer_statut_form")
let creer_statut_label      = document.querySelector("#creer_statut_label")
let creer_statut_input      = document.querySelector("#creer_statut_input")
let creer_statut_btn        = document.querySelector("#creer_statut_btn")
let supprimer_statut_form   = document.querySelector("#supprimer_statut_form")
let supprimer_statut_label  = document.querySelector("#supprimer_statut_label")
let supprimer_statut_input  = document.querySelector("#supprimer_statut_input")
let supprimer_statut_btn    = document.querySelector("#supprimer_statut_btn")
let BTN_statut              = document.querySelector("#BTN_statut")
let statut_form             = document.querySelector("#statut_form")
let statut_checkbox         = document.querySelectorAll(".statut_checkbox")
let statut_section          = document.querySelector("#section_statut")
let BTN_confirmation_statut = document.querySelector("#BTN_statut")

// VILLE
let BTN_ajouter_ville = document.querySelector("#BTN_ajouter_ville")
let BTN_delete        = document.querySelector(".BTN_delete")
let ville_alert       = document.querySelector("#ville_alert")
let choix_ville       = document.querySelector("#choix_ville")

// DEMARCHE
let demarche_alert            = document.querySelector("#demarche_alert")
let demarche_section          = document.querySelector("#demarche")
let creer_demarche_form       = document.querySelector("#creer_demarche_form")
let creer_demarche_label      = document.querySelector("#creer_demarche_label")
let creer_demarche_input      = document.querySelector("#creer_demarche_input")
let creer_demarche_btn        = document.querySelector("#creer_demarche_btn")
let supprimer_demarche_form   = document.querySelector("#supprimer_demarche_form")
let supprimer_demarche_label  = document.querySelector("#supprimer_demarche_label")
let supprimer_demarche_input  = document.querySelector("#supprimer_demarche_input")
let supprimer_demarche_btn    = document.querySelector("#supprimer_demarche_btn")
let BTN_confirmation_demarche = document.querySelector("#BTN_confirmation_demarche")

// LOGICIEL & SYSTEME
let logiciel_systeme_alert            = document.querySelector("#logiciel_systeme_alert")
let logiciel_systeme_section          = document.querySelector("#logiciel_systeme")
let creer_logiciel_systeme_form       = document.querySelector("#creer_logiciel_systeme_form")
let creer_logiciel_systeme_label      = document.querySelector("#creer_logiciel_systeme_label")
let creer_logiciel_systeme_input      = document.querySelector("#creer_logiciel_systeme_input")
let creer_logiciel_systeme_btn        = document.querySelector("#creer_logiciel_systeme_btn")
let supprimer_logiciel_systeme_form   = document.querySelector("#supprimer_logiciel_systeme_form")
let supprimer_logiciel_systeme_label  = document.querySelector("#supprimer_logiciel_systeme_label")
let supprimer_logiciel_systeme_input  = document.querySelector("#supprimer_logiciel_systeme_input")
let supprimer_logiciel_systeme_btn    = document.querySelector("#supprimer_logiciel_systeme_btn")
let BTN_confirmation_logiciel_systeme = document.querySelector("#BTN_confirmation_logiciel_systeme")

// IMAGE, SON & JEUX VIDEO
let image_jv_alert            = document.querySelector("#image_jv_alert")
let image_jv_section          = document.querySelector("#image_jv")
let creer_image_jv_form       = document.querySelector("#creer_image_jv_form")
let creer_image_jv_label      = document.querySelector("#creer_image_jv_label")
let creer_image_jv_input      = document.querySelector("#creer_image_jv_input")
let creer_image_jv_btn        = document.querySelector("#creer_image_jv_btn")
let supprimer_image_jv_form   = document.querySelector("#supprimer_image_jv_form")
let supprimer_image_jv_label  = document.querySelector("#supprimer_image_jv_label")
let supprimer_image_jv_input  = document.querySelector("#supprimer_image_jv_input")
let supprimer_image_jv_btn    = document.querySelector("#supprimer_image_jv_btn")
let BTN_confirmation_image_jv = document.querySelector("#BTN_confirmation_image_jv")

// MATERIEL
let materiel_alert            = document.querySelector("#materiel_alert")
let materiel_section          = document.querySelector("#materiel")
let creer_materiel_form       = document.querySelector("#creer_materiel_form")
let creer_materiel_label      = document.querySelector("#creer_materiel_label")
let creer_materiel_input      = document.querySelector("#creer_materiel_input")
let creer_materiel_btn        = document.querySelector("#creer_materiel_btn")
let supprimer_materiel_form   = document.querySelector("#supprimer_materiel_form")
let supprimer_materiel_label  = document.querySelector("#supprimer_materiel_label")
let supprimer_materiel_input  = document.querySelector("#supprimer_materiel_input")
let supprimer_materiel_btn    = document.querySelector("#supprimer_materiel_btn")
let BTN_confirmation_materiel = document.querySelector("#BTN_confirmation_materiel")

// INFRASTRUCTURE
let infrastructure_alert            = document.querySelector("#infrastructure_alert")
let infrastructure_section          = document.querySelector("#infrastructure")
let creer_infrastructure_form       = document.querySelector("#creer_infrastructure_form")
let creer_infrastructure_label      = document.querySelector("#creer_infrastructure_label")
let creer_infrastructure_input      = document.querySelector("#creer_infrastructure_input")
let creer_infrastructure_btn        = document.querySelector("#creer_infrastructure_btn")
let supprimer_infrastructure_form   = document.querySelector("#supprimer_infrastructure_form")
let supprimer_infrastructure_label  = document.querySelector("#supprimer_infrastructure_label")
let supprimer_infrastructure_input  = document.querySelector("#supprimer_infrastructure_input")
let supprimer_infrastructure_btn    = document.querySelector("#supprimer_infrastructure_btn")
let BTN_confirmation_infrastructure = document.querySelector("#BTN_confirmation_infrastructure")

// SIRET
let siret_alert          = document.querySelector("#siret_alert")
let small_siret          = document.querySelector("#small_siret")
let small_siret_longueur = document.querySelector("#small_siret_longueur")
let BTN_siret            = document.querySelector("#BTN_siret")
let statut_siret         = document.querySelector("#statut_siret")

// INFOS PERSO
let alert_diverses            = document.querySelector("#alert_diverses")
let alert_info_perso          = document.querySelector("#alert_info_perso")
let input_update_nom          = document.querySelector("#input_update_nom")
let update_nom_btn            = document.querySelector("#update_nom_btn")
let input_update_adresse_mail = document.querySelector("#input_update_adresse_mail")
let update_adresse_mail_btn   = document.querySelector("#update_adresse_mail_btn")
let input_update_prenom       = document.querySelector("#input_update_prenom")
let update_prenom_btn         = document.querySelector("#update_prenom_btn")
let input_update_telephone    = document.querySelector("#input_update_telephone")
let update_telephone_btn      = document.querySelector("#update_telephone_btn")
let alert_check_fiche         = document.querySelector("#alert_check_fiche")
let changing_fiche_btn        = document.querySelector("#changing_fiche_btn")
let disabled_fiche_btn        = document.querySelector("#disabled_fiche_btn")
let nom_prenom                = document.querySelector("#nom_prenom")

// REPONSE FETCH
let answer;

// AU CHARGEMENT DE LA PAGE (fonction en bas de script)
window.onload = onload()

// ECOUTEURS D'ÉVENNEMENT
BTN_ajouter_ville.addEventListener("click", fetch_add_choix_ville)
update_nom_btn.addEventListener("click", fetch_update_nom_user)
update_prenom_btn.addEventListener("click", fetch_update_prenom_user)
update_adresse_mail_btn.addEventListener("click", fetch_update_adresse_mail_user)
update_telephone_btn.addEventListener("click", fetch_update_telephone_user)

async function fetch_display_nom_user() {
    const reponse = await fetch('../controller/afficher_nom_user.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()

        input_update_nom.value = answer;
        nom_user.value = answer;

    }

}
async function fetch_update_nom_user() {

    const reponse = await fetch('../controller/update_nom_user.php?id_user=' + user_id_choix.value + '&nom_user=' + input_update_nom.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        alert_info_perso.innerHTML = answer;
        alert_info_perso.style.display = "flex";

        fetch_display_nom_prenom()

    }
}

async function fetch_display_prenom_user() {
    const reponse = await fetch('../controller/afficher_prenom_user.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()

        input_update_prenom.value = answer;
        prenom_user.value = answer;

    }

}

async function fetch_update_prenom_user() {

    const reponse = await fetch('../controller/update_prenom_user.php?id_user=' + user_id_choix.value + '&prenom_user=' + input_update_prenom.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        alert_info_perso.innerHTML = answer;
        alert_info_perso.style.display = "flex";

        fetch_display_nom_prenom()

    }
}

async function fetch_display_adresse_mail_user() {
    const reponse = await fetch('../controller/afficher_adresse_mail_user.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()

        input_update_adresse_mail.value = answer;
        mail_user.value = answer;
    }

}

async function fetch_update_adresse_mail_user() {

    const reponse = await fetch('../controller/update_adresse_mail_user.php?id_user=' + user_id_choix.value + '&adresse_mail_user=' + input_update_adresse_mail.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        alert_info_perso.innerHTML = answer;
        alert_info_perso.style.display = "flex";
        fetch_display_adresse_mail_user();

    }
}

async function fetch_display_telephone_user() {
    const reponse = await fetch('../controller/afficher_telephone_user.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()

        input_update_telephone.value = answer;

    }

}

async function fetch_update_telephone_user() {

    const reponse = await fetch('../controller/update_telephone_user.php?id_user=' + user_id_choix.value + '&telephone_user=' + input_update_telephone.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        alert_info_perso.innerHTML = answer;
        alert_info_perso.style.display = "flex";

    }
}

// VILLE


async function fetch_add_choix_ville() {

    // console.log(choix_ville.value)

    const reponse = await fetch('../controller/create_choix_ville.php?id_user=' + user_id_choix.value + '&nom_ville=' + choix_ville.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        ville_alert.innerHTML = answer;
        ville_alert.style.display = "flex";

        fetch_display_villes_choisies();

    }
}



async function fetch_display_villes_choisies() {
    const reponse = await fetch('../controller/afficher_choix_ville.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()

        body_choix.innerHTML = answer;

        let BTN_delete = document.querySelectorAll(".BTN_delete");

        [...BTN_delete].forEach(element => {

            element.addEventListener("click", () => {

                id_ville = element.value;
                delete_ville_user();

            })
        })

    }

}

async function delete_ville_user() {

    const reponse = await fetch('../controller/delete_choix_ville.php?id_ville=' + id_ville + '&id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text();

        ville_alert.innerHTML = answer;
        ville_alert.style.display = "flex";
        fetch_display_villes_choisies();

    }

}

async function fetch_display_choix_ville_liste() {
    const reponse = await fetch('../controller/afficher_choix_ville_liste.php', {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        choix_ville.innerHTML = answer;

    }
}

// IL ACCOMPAGNE LES DIGINAUTES SUR -------------------------- 


async function fetch_display_nom_prenom() {

    const reponse = await fetch('../controller/afficher_nom_prenom.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()

        if (answer === "0") {
            main_html.innerHTML = "Compte utilisateur inconnu"
        } else {
            nom_prenom.innerHTML = answer;
            main_html.style.display = 'flex';
        }
    }

}

// IL ACCOMPAGNE LES DIGINAUTES SUR materiel



BTN_confirmation_materiel.addEventListener("click", () => {
    let option_materiel = document.querySelectorAll(".option_materiel")

    option_materiel.forEach(element => {
        if (element.checked) {
            fetch_materiel_checked(element.value);
        } else if (!element.checked) {
            fetch_materiel_unchecked(element.value)
        }
    })

    materiel_alert.style.display = "flex"
    materiel_alert.innerHTML = "Terminaux mis à jour avec succès !";
})


async function fetch_materiel_checked(id_materiel) {

    const reponse = await fetch('../controller/ajouter_materiel_user.php?id_user=' + user_id_choix.value + '&id_materiel=' + id_materiel, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
            // materiel_alert.innerHTML = answer
    };
}

async function fetch_materiel_unchecked(id_materiel) {

    const reponse = await fetch('../controller/supprimer_materiel_user.php?id_user=' + user_id_choix.value + '&id_materiel=' + id_materiel, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        // materiel_alert.innerHTML = answer
    };
}


async function fetch_display_materiel_liste() {
    const reponse = await fetch('../controller/afficher_materiel_liste.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        materiel_section.innerHTML = answer;

    }
}

creer_materiel_btn.addEventListener("click", fetch_creer_materiel)

async function fetch_creer_materiel() {
    const reponse = await fetch('../controller/creer_materiel.php?nom_materiel=' + creer_materiel_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        materiel_alert.innerHTML = answer;
        materiel_alert.style.display = "flex"
        if (answer === "Ce materiel existe déjà") {
            creer_materiel_input.style.border = "2px solid red"
        } else if (answer === "Nouveau materiel créé avec succès !") {
            fetch_display_materiel_liste()
            creer_materiel_input.value = ""
            creer_materiel_input.style.border = "1px solid black"
        }

    }
}

supprimer_materiel_btn.addEventListener("click", fetch_supprimer_materiel)

async function fetch_supprimer_materiel() {
    const reponse = await fetch('../controller/supprimer_materiel.php?id_materiel=' + supprimer_materiel_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        materiel_alert.innerHTML = answer;
        materiel_alert.style.display = "flex"

        if (answer === "Cet identifiant materiel n'existe pas") {
            supprimer_materiel_input.style.border = "2px solid red"
        } else if (answer === "materiel supprimé avec succès !") {
            fetch_display_materiel_liste()
            supprimer_materiel_input.value = ""
            supprimer_materiel_input.style.border = "1px solid black"
        }

    }
}


// IL ACCOMPAGNE LES DIGINAUTES SUR LES DEMARCHES



BTN_confirmation_demarche.addEventListener("click", () => {
    let option_demarche = document.querySelectorAll(".option_demarche")

    option_demarche.forEach(element => {
        if (element.checked) {
            fetch_demarche_checked(element.value);
        } else if (!element.checked) {
            fetch_demarche_unchecked(element.value)
        }
    })

    demarche_alert.style.display = "flex"
    demarche_alert.innerHTML = "demarches mis à jour avec succès !";

})


async function fetch_demarche_checked(id_demarche) {

    const reponse = await fetch('../controller/ajouter_demarche_user.php?id_user=' + user_id_choix.value + '&id_demarche=' + id_demarche, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
            // demarche_alert.innerHTML = answer;
    };
}


async function fetch_demarche_unchecked(id_demarche) {

    const reponse = await fetch('../controller/supprimer_demarche_user.php?id_user=' + user_id_choix.value + '&id_demarche=' + id_demarche, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
            // demarche_alert.innerHTML = answer
    };
}


async function fetch_display_demarche_liste() {
    const reponse = await fetch('../controller/afficher_demarche_liste.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        demarche_section.innerHTML = answer;

    }
}

creer_demarche_btn.addEventListener("click", fetch_creer_demarche)

async function fetch_creer_demarche() {
    const reponse = await fetch('../controller/creer_demarche.php?nom_demarche=' + creer_demarche_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        demarche_alert.innerHTML = answer;
        demarche_alert.style.display = "flex"
        if (answer === "Ce demarche existe déjà") {
            creer_demarche_input.style.border = "2px solid red"
        } else if (answer === "Nouveau demarche créé avec succès !") {
            fetch_display_demarche_liste()
            creer_demarche_input.value = ""
            creer_demarche_input.style.border = "1px solid black"
        }

    }
}

supprimer_demarche_btn.addEventListener("click", fetch_supprimer_demarche)

async function fetch_supprimer_demarche() {
    const reponse = await fetch('../controller/supprimer_demarche.php?id_demarche=' + supprimer_demarche_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        demarche_alert.innerHTML = answer;
        demarche_alert.style.display = "flex"

        if (answer === "Cet identifiant demarche n'existe pas") {
            supprimer_demarche_input.style.border = "2px solid red"
        } else if (answer === "demarche supprimé avec succès !") {
            fetch_display_demarche_liste()
            supprimer_demarche_input.value = ""
            supprimer_demarche_input.style.border = "1px solid black"
        }

    }
}

// IL ACCOMPAGNE LES DIGINAUTES SUR IMAGE SON JV



BTN_confirmation_image_jv.addEventListener("click", () => {
    let option_image_jv = document.querySelectorAll(".option_image_jv")

    option_image_jv.forEach(element => {
        if (element.checked) {
            fetch_image_jv_checked(element.value);
        } else if (!element.checked) {
            fetch_image_jv_unchecked(element.value)
        }
    })

    image_jv_alert.style.display = "flex"
    image_jv_alert.innerHTML = "image_jvs mis à jour avec succès !";

})


async function fetch_image_jv_checked(id_image_jv) {

    const reponse = await fetch('../controller/ajouter_image_jv_user.php?id_user=' + user_id_choix.value + '&id_image_jv=' + id_image_jv, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
            // image_jv_alert.innerHTML = answer;
    };
}


async function fetch_image_jv_unchecked(id_image_jv) {

    const reponse = await fetch('../controller/supprimer_image_jv_user.php?id_user=' + user_id_choix.value + '&id_image_jv=' + id_image_jv, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
            // image_jv_alert.innerHTML = answer
    };
}


async function fetch_display_image_jv_liste() {
    const reponse = await fetch('../controller/afficher_image_jv_liste.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        image_jv_section.innerHTML = answer;

    }
}

creer_image_jv_btn.addEventListener("click", fetch_creer_image_jv)

async function fetch_creer_image_jv() {
    const reponse = await fetch('../controller/creer_image_jv.php?nom_image_jv=' + creer_image_jv_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        image_jv_alert.innerHTML = answer;
        image_jv_alert.style.display = "flex"
        if (answer === "Ce image_jv existe déjà") {
            creer_image_jv_input.style.border = "2px solid red"
        } else if (answer === "Nouveau image_jv créé avec succès !") {
            fetch_display_image_jv_liste()
            creer_image_jv_input.value = ""
            creer_image_jv_input.style.border = "1px solid black"
        }

    }
}

supprimer_image_jv_btn.addEventListener("click", fetch_supprimer_image_jv)

async function fetch_supprimer_image_jv() {
    const reponse = await fetch('../controller/supprimer_image_jv.php?id_image_jv=' + supprimer_image_jv_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        image_jv_alert.innerHTML = answer;
        image_jv_alert.style.display = "flex"

        if (answer === "Cet identifiant image_jv n'existe pas") {
            supprimer_image_jv_input.style.border = "2px solid red"
        } else if (answer === "image_jv supprimé avec succès !") {
            fetch_display_image_jv_liste()
            supprimer_image_jv_input.value = ""
            supprimer_image_jv_input.style.border = "1px solid black"
        }

    }
}

// IL ACCOMPAGNE LES DIGINAUTES SUR LOGICIEL & OUTIL

BTN_confirmation_logiciel_systeme.addEventListener("click", () => {
    let option_logiciel_systeme = document.querySelectorAll(".option_logiciel_systeme")

    option_logiciel_systeme.forEach(element => {
        if (element.checked) {
            fetch_logiciel_systeme_checked(element.value);
        } else if (!element.checked) {
            fetch_logiciel_systeme_unchecked(element.value)
        }
    })

    logiciel_systeme_alert.style.display = "flex"
    logiciel_systeme_alert.innerHTML = "Logiciels & outils mis à jour avec succès !";
})


async function fetch_logiciel_systeme_checked(id_logiciel_systeme) {

    const reponse = await fetch('../controller/ajouter_logiciel_systeme_user.php?id_user=' + user_id_choix.value + '&id_logiciel_systeme=' + id_logiciel_systeme, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        // logiciel_systeme_alert.innerHTML = answer
    };
}

async function fetch_logiciel_systeme_unchecked(id_logiciel_systeme) {

    const reponse = await fetch('../controller/supprimer_logiciel_systeme_user.php?id_user=' + user_id_choix.value + '&id_logiciel_systeme=' + id_logiciel_systeme, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        // logiciel_systeme_alert.innerHTML = answer
    };
}


async function fetch_display_logiciel_systeme_liste() {
    const reponse = await fetch('../controller/afficher_logiciel_systeme_liste.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        logiciel_systeme_section.innerHTML = answer;

    }
}

creer_logiciel_systeme_btn.addEventListener("click", fetch_creer_logiciel_systeme)

async function fetch_creer_logiciel_systeme() {
    const reponse = await fetch('../controller/creer_logiciel_systeme.php?nom_logiciel_systeme=' + creer_logiciel_systeme_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        logiciel_systeme_alert.innerHTML = answer;
        logiciel_systeme_alert.style.display = "flex"
        if (answer === "Ce logiciel_systeme existe déjà") {
            creer_logiciel_systeme_input.style.border = "2px solid red"
        } else if (answer === "Nouveau logiciel_systeme créé avec succès !") {
            fetch_display_logiciel_systeme_liste()
            creer_logiciel_systeme_input.value = ""
            creer_logiciel_systeme_input.style.border = "1px solid black"
        }

    }
}

supprimer_logiciel_systeme_btn.addEventListener("click", fetch_supprimer_logiciel_systeme)

async function fetch_supprimer_logiciel_systeme() {
    const reponse = await fetch('../controller/supprimer_logiciel_systeme.php?id_logiciel_systeme=' + supprimer_logiciel_systeme_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        logiciel_systeme_alert.innerHTML = answer;
        logiciel_systeme_alert.style.display = "flex"

        if (answer === "Cet identifiant logiciel_systeme n'existe pas") {
            supprimer_logiciel_systeme_input.style.border = "2px solid red"
        } else if (answer === "logiciel_systeme supprimé avec succès !") {
            fetch_display_logiciel_systeme_liste()
            supprimer_logiciel_systeme_input.value = ""
            supprimer_logiciel_systeme_input.style.border = "1px solid black"
        }

    }
}

// IL ACCOMPAGNE LES DIGINAUTES SUR LES INFRASTRUCTURES

BTN_confirmation_infrastructure.addEventListener("click", () => {
    let option_infrastructure = document.querySelectorAll(".option_infrastructure")

    option_infrastructure.forEach(element => {
        if (element.checked) {
            fetch_infrastructure_checked(element.value);
        } else if (!element.checked) {
            fetch_infrastructure_unchecked(element.value)
        }
    })

    infrastructure_alert.style.display = "flex"
    infrastructure_alert.innerHTML = "Logiciels & outils mis à jour avec succès !";
})


async function fetch_infrastructure_checked(id_infrastructure) {

    const reponse = await fetch('../controller/ajouter_infrastructure_user.php?id_user=' + user_id_choix.value + '&id_infrastructure=' + id_infrastructure, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        // infrastructure_alert.innerHTML = answer
    };
}

async function fetch_infrastructure_unchecked(id_infrastructure) {

    const reponse = await fetch('../controller/supprimer_infrastructure_user.php?id_user=' + user_id_choix.value + '&id_infrastructure=' + id_infrastructure, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        // infrastructure_alert.innerHTML = answer
    };
}


async function fetch_display_infrastructure_liste() {
    const reponse = await fetch('../controller/afficher_infrastructure_liste.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {

        answer = await reponse.text()
        infrastructure_section.innerHTML = answer;

    }
}

creer_infrastructure_btn.addEventListener("click", fetch_creer_infrastructure)

async function fetch_creer_infrastructure() {
    const reponse = await fetch('../controller/creer_infrastructure.php?nom_infrastructure=' + creer_infrastructure_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        infrastructure_alert.innerHTML = answer;
        infrastructure_alert.style.display = "flex"
        if (answer === "Ce infrastructure existe déjà") {
            creer_infrastructure_input.style.border = "2px solid red"
        } else if (answer === "Nouveau infrastructure créé avec succès !") {
            fetch_display_infrastructure_liste()
            creer_infrastructure_input.value = ""
            creer_infrastructure_input.style.border = "1px solid black"
        }

    }
}

supprimer_infrastructure_btn.addEventListener("click", fetch_supprimer_infrastructure)

async function fetch_supprimer_infrastructure() {
    const reponse = await fetch('../controller/supprimer_infrastructure.php?id_infrastructure=' + supprimer_infrastructure_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        infrastructure_alert.innerHTML = answer;
        infrastructure_alert.style.display = "flex"

        if (answer === "Cet identifiant infrastructure n'existe pas") {
            supprimer_infrastructure_input.style.border = "2px solid red"
        } else if (answer === "infrastructure supprimé avec succès !") {
            fetch_display_infrastructure_liste()
            supprimer_infrastructure_input.value = ""
            supprimer_infrastructure_input.style.border = "1px solid black"
        }

    }
}

// SON STATUT



BTN_confirmation_statut.addEventListener("click", () => {
    let option_statut = document.querySelectorAll(".option_statut")

    option_statut.forEach(element => {
        if (element.checked) {
            fetch_statut_checked(element.value)
        } else if (!element.checked) {
            fetch_statut_unchecked(element.value)
        }
    })

    statut_alert.style.display = "flex"
    statut_alert.innerHTML = "Statuts mis à jour avec succès !";
})


async function fetch_statut_checked(id_statut) {

    const reponse = await fetch('../controller/ajouter_statut_user.php?id_user=' + user_id_choix.value + '&id_statut=' + id_statut, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        // statut_alert.innerHTML = answer
        statut_alert.style.display = "flex"
    };
}

async function fetch_statut_unchecked(id_statut) {

    const reponse = await fetch('../controller/supprimer_statut_user.php?id_user=' + user_id_choix.value + '&id_statut=' + id_statut, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        // statut_alert.innerHTML = answer
        statut_alert.style.display = "flex"
    };
}

async function fetch_display_statut_liste() {
    const reponse = await fetch('../controller/afficher_statut_liste.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        statut_section.innerHTML = answer;

    }
}

creer_statut_btn.addEventListener("click", fetch_creer_statut)

async function fetch_creer_statut() {
    const reponse = await fetch('../controller/creer_statut.php?nom_statut=' + creer_statut_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        statut_alert.innerHTML = answer;
        statut_alert.style.display = "flex";
        if (answer === "Ce statut existe déjà" || answer === "Nom de statut invalide") {
            creer_statut_input.style.border = "2px solid red"
        } else if (answer === "Nouveau statut créé avec succès !") {
            fetch_display_statut_liste()
            creer_statut_input.value = ""
            creer_statut_input.style.border = "1px solid black"
        }

    }
}

supprimer_statut_btn.addEventListener("click", fetch_supprimer_statut)

async function fetch_supprimer_statut() {
    const reponse = await fetch('../controller/supprimer_statut.php?id_statut=' + supprimer_statut_input.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        statut_alert.innerHTML = answer;
        statut_alert.style.display = "flex";

        if (answer === "Cet identifiant statut n'existe pas") {
            supprimer_statut_input.style.border = "2px solid red"
        } else if (answer === "Statut supprimé avec succès !") {
            fetch_display_statut_liste()
            supprimer_statut_input.value = ""
            supprimer_statut_input.style.border = "1px solid black"
        }

    }
}


// UPDATE NUMERO SIRET

BTN_siret.addEventListener("click", fetch_siret_check)

async function fetch_siret_check() {
    if (statut_siret.value) {

        const reponse = await fetch('../controller/modifier_siret_user.php?id_user=' + user_id_choix.value + '&siret=' + statut_siret.value, {
            method: "GET"
        });

        if (reponse.ok) {
            answer = await reponse.text()
            console.log(answer)
            if (answer === "Le numéro de siret est obligatoire") {
                siret_alert.innerHTML = answer
                siret_alert.style.display = "flex"
                statut_siret.style.border = "2px solid red"
                small_siret.style.display = "block"
                small_siret_longueur.style.display = "none"
            } else if (answer === "Longueur obligatoire de 14 chiffres") {
                siret_alert.innerHTML = answer
                siret_alert.style.display = "flex"
                statut_siret.style.border = "2px solid red"
                small_siret_longueur.style.display = "block"
                small_siret.style.display = "none"
            } else if (answer === "Succès de la modification du Siret") {
                siret_alert.innerHTML = answer
                siret_alert.style.display = "flex"
                statut_siret.style.border = "1px solid black"
                small_siret_longueur.style.display = "none"
                small_siret.style.display = "none"
            } else {
                siret_alert.innerHTML = answer
            }
        }
    } else {
        console.log(statut_siret.value)
    }
}


async function fetch_display_siret_input() {
    const reponse = await fetch('../controller/afficher_siret_user.php?id_user=' + user_id_choix.value, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        statut_siret.value = answer
    }
}


// AU DEMARRAGE

function onload() {
    fetch_display_villes_choisies()
    fetch_display_nom_prenom()
    fetch_display_choix_ville_liste()
    fetch_display_materiel_liste()
    fetch_display_image_jv_liste()
    fetch_display_logiciel_systeme_liste()
    fetch_display_infrastructure_liste()
    fetch_display_demarche_liste()
    fetch_display_statut_liste()
    fetch_display_siret_input()
    fetch_display_nom_user()
    fetch_display_prenom_user()
    fetch_display_adresse_mail_user()
    fetch_display_telephone_user()
  
}