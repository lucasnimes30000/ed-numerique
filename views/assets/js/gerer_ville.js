/*

    Script JS . ed-numérique
    Gère la modification d'état des vignettes de ville (côté back-office)
    + La partie dynamique de la page

*/

let alert_box = document.querySelector("#alert")
let btn_spot = document.querySelector("#buttons")

window.onload = onload()

async function fetch_display_ville_btn() {
    const reponse = await fetch('../controller/display_ville_btn.php', {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()
        btn_spot.innerHTML = answer
        let btn_ville = document.querySelectorAll(".btn_ville")

        btn_ville.forEach(element => {
        
            element.addEventListener("click", () => {
                let explosion = element.value.split(' ')
                let statut_vignette = explosion[0]
                let id_ville = explosion[1]
        
        
                switch(statut_vignette) {
                    case "0":
                        fetch_enable_ville(id_ville)
                        break;
                    case "1":
                        fetch_disable_ville(id_ville)
                        break;
                    default:
                        break;
                }
            })
        })
        
    }
}



async function fetch_enable_ville(id) {
    const reponse = await fetch('../controller/enable_ville.php?id_ville=' + id, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        switch (answer) {
            case "ok":
                alert_box.innerHTML = "Ville activée avec succès !"
                alert_box.style.display = "flex"
                fetch_display_ville_btn()
                break;
            default:
                alert_box.innerHTML = "<!> Erreur dans l'activation de la vignette ville <!><br>"
                alert_box.style.display = "flex"
                break;
        }
    }
}

async function fetch_disable_ville(id) {
    const reponse = await fetch('../controller/disable_ville.php?id_ville=' + id, {
        method: "GET"
    });

    if (reponse.ok) {
        answer = await reponse.text()

        switch (answer) {
            case "ok":
                alert_box.innerHTML = "Ville désactivée avec succès !"
                alert_box.style.display = "flex"
                fetch_display_ville_btn()
                break;
            default:
                alert_box.innerHTML = "<!> Erreur dans la désactivation de la vignette ville <!><br>"
                alert_box.style.display = "flex"
                break;
        }
    }
}

// AU DEMARRAGE

function onload() {
    fetch_display_ville_btn()

}