let collapsible = document.getElementsByClassName("h2");
let txt = document.getElementsByClassName("txt");

if (window.matchMedia("(max-width: 1180px)").matches){
    [...collapsible].forEach(element => {
        let content = element.nextElementSibling;
        element.addEventListener("click", function dropDown() {
    
                if(content.style.display === "none"){
                content.style.display = "block";
            } else {
                content.style.display ="none";
            }
            
         });
    
    });
}

function VideOrNot() {
    
    let prenomMod = document.getElementById("prenomMod");
    let nomMod =  document.getElementById("nomMod");
    let cpMod =  document.getElementById("cpMod");
    let phoneMod =  document.getElementById("phoneMod");
    let mailMod =  document.getElementById("mailMod");
    let messageMod =  document.getElementById("messageMod");
    let checkMod =  document.getElementById("checkMod");
    
    let champsValeurPrenom = prenomMod.value;
    let champsValeurNom = nomMod.value;
    let champsValeurCp = cpMod.value;
    let champsValeurPhone = phoneMod.value;
    let champsValeurEmail = mailMod.value;
    let champsValeurMessage = messageMod.value;
    let champsValeurCheck = checkMod.value;

    let obligatoire  = document.querySelectorAll(".emptyTxt");

if (champsValeurPrenom == "") {
    prenomMod.classList.remove("inputMod");
    prenomMod.classList.add("incorrect");
    obligatoire[0].style.display ="block"
}else {
    prenomMod.className = ("inputMod");
    obligatoire[0].style.display ="none"
}

if ( champsValeurNom =="") {
    nomMod.classList.remove("inputMod");
    nomMod.classList.add("incorrect");
    obligatoire[1].style.display ="block"
}
else {
    nomMod.className = ("inputMod");
    obligatoire[1].style.display ="none"
}

if (champsValeurPhone=="") {
    
    phoneMod.classList.remove("inputMod");
    phoneMod.classList.add("incorrect");
    obligatoire[2].style.display ="block"
}else {
    phoneMod.className = ("inputMod");
    obligatoire[2].style.display ="none"
}

if (champsValeurCp =="") {
    cpMod.classList.remove("inputMod");
    cpMod.classList.add("incorrect");
    obligatoire[3].style.display ="block"
}else {
    cpMod.className = ("inputMod");
    obligatoire[3].style.display ="none"
}

if( champsValeurEmail =="") {
    mailMod.classList.remove("inputMod");
    mailMod.classList.add("incorrect");
    obligatoire[4].style.display ="block"
}else {
    mailMod.className = ("inputMod");
    obligatoire[4].style.display ="none"
}

if (champsValeurMessage =="") {
    messageMod.classList.remove("messageModA");
    messageMod.classList.add("incorrectMessage")
}
else {
    messageMod.className = ("messageModA")
}


}

let envoiMod = document.querySelector("#envoiMod");


envoiMod.addEventListener("click", VideOrNot);

