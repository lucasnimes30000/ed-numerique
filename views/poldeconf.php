<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Contrat expliquant le traitement ainsi que la publication et la suppression des
données à titre personnel transmises par les utilisateurs du site Ed numérique.">
    <?php include("metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/poldeconf.css">
    <title>Politique de Confidentialité</title>
</head>
<body>
    <?php include("views/header_annuaire.php"); ?>

    <h1>Politique de Confidentialité des données et RGPD</h1>
    <section id="poldeconf">
        <p>L’Utilisateur (intervenant Ed) est informé qu’ED NUMÉRIQUE collecte et traite les données personnelles suivantes : </p>
        <ul>
            <li>le nom de l’utilisateur;</li>
            <li>le numéro de téléphone ;</li>
            <li>l’adresse mail ;</li>
            <li>le numéro de SIRET si applicable et agrément SAP ;</li>
            <li>le numéro de compte bancaire.</li>
        </ul>

        <p>Un fichier utilisateurs, regroupant les données présentées ci-dessus, est constitué par Ed numérique. Ce document est strictement confidentiel et interne. Il est protégé par des mesures de sécurité. Les données qu’il comporte ne sont pas communiquées aux tiers, exceptions faites des réquisitions de la justice.</p>

        <p>La durée de conservation des données personnelles varie en fonction de la finalité de leurs collectes :</p>
        <ul>
            <li>Les données de Compte (identité, coordonnées électroniques, historique d’utilisation des services) sont conservées 3 ans à compter de la fin de la relation commerciale ;</li>
            <li>Les données relatives à la gestion de votre Compte sont conservées pour une durée de 5 ans à compter de leur suppression et ce, exclusivement à des fins de preuve ;</li>
            <li>Les documents et pièces comptables sont conservés 10 ans, à titre de preuve comptable ;</li>
            <li>Les données susceptibles de faire l’objet d’une réquisition judiciaire (données de connexion, identité, coordonnées de contact, données relatives aux transactions) sont conservées 12 mois à compter de leur collecte ;</li>
            <li>Les conversations téléphoniques avec notre support client professionnel sont conservées pendant une durée de 6 mois.</li>
        </ul>

        <p>Conformément au règlement no 2016/679, sur la protection des données (RGPD), ED-NUMÉRIQUE et ses intervenants s'engagent à protéger les données à caractère personnel de l’Utilisateur, ainsi que celles du Bénéficiaire lorsqu’il s’agit de deux personnes distinctes. Ces données personnelles ne seront jamais communiquées à des tiers extérieurs, sauf réquisition de justice.</p>

        <p>Conformément aux dispositions de la loi n°78-17 du 6 janvier 1978 dite "Informatique et Libertés" et sous réserve de justifier de son identité, l’Utilisateur dispose du droit de demander à ce que les données à caractère personnel le concernant soient rectifiées, complétées, mises à jour, verrouillées ou effacées notamment si ces données sont inexactes, incomplètes, équivoques, périmées, ou si la collecte, l'utilisation, la communication ou la conservation de ces données est interdite.</p>

        <p>L’Utilisateur peut exercer ses droits en envoyant un courrier postal accompagné d'un justificatif d'identité à Monsieur BOURNONVILLE Aurélien, directeur général, chargé en cette qualité du traitement des donnée personnelles, <b>Ed numérique</b>, Société par Actions Simplifiée au capital de 1.000 €, dont le siège social est situé Via Innova – 177 bis avenue Louis Lumière 34400 LUNEL.</p>
    </section>

    <?php include("views/footer.php");
    include("views/popup.php"); ?>

    <a class="top-link hide" href="" id="js-top">   
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>

    <script src="views/assets/js/header_annuaire.js"></script>
</body>
</html>
