<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../views/assets/css/commentaire.css">
    <link rel="stylesheet" href="../views/assets/css/structure_commune.css">

    <title>Commentaire</title>
</head>
<body>
    <?php 
        include("../views/header_admin.php");
    ?>
    <input type="text" id="recherche" placeholder="Recherche par Nom/Prenom Particulier">
    <input type="text" id="recherche2" placeholder="Recherche par Nom/Prenom Ed">
    <button id='choixTout' value="Tout">Tout afficher</button>
    <button id='choixAttenteDeValidation' value="0" >En attente de validation</button>
    <button id='choixSuspendu' value="2" >Suspendu</button>
    <button id='choixVisible' value="1" >Visible</button>

    <table>
        <thead>
            <tr>
            <th>Date de l'avis</th>
            <th>Particulier</th>
            <th>Avis</th>
            <th>Ed</th>
            </tr>
        </thead>
        <tbody id="table">
        </tbody>
    </table>

    <table id="test"></table>
    <script src="../views/assets/js/commentaire.js"></script>
</body>
</html>