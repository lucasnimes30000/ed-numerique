<?php

    include("../controller/inc_session.php");
    sessionInit();

    if (sessionValidAdmin() !== true) {
        
        sessionBye(); 
        header("Location: connexion_admin.php#sessionerror");
        exit;
        
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="../views/assets/css/inscription.css">
    <link rel="stylesheet" href="../views/assets/css/inscription_premiere.css">
    <link rel="shortcut icon" type="image/png" href="../views/assets/img/logo.svg"/>
    <title>Inscription</title>
</head>
<body>
    <?php
        include("../views/header_admin.php");
    ?>
    <main>
        <h1 style = "font-size: 3rem; margin-top: 7rem;">Inscription Ed</h1><br>

        <p id="alerte-box"></p>
        
        <div style="text-align:center;width:36%;margin:auto" id="premiere_OK" class="alert alert-danger" role="alert">
                        Contenu
        </div>

        <br>
        <p class = "titre_inscription premiere_partie"> Veuillez remplir les champs suivants pour inscrire un Ed:</p>
        <br>
            <section id="inscription_ed">
                <section id = "CTN_inscription_form">
                    <form action="" class="formBlocGauche">
                        <div id="premiere_partie"class="CTN_flex_ed ">
                            <form action="" method="post" id="first_part_form">
                            <div class="formFlex_left">
                                <div class="formFlex">
                                    <div>
                                        <img src="../views/assets/img/formulaires/user_icon_svg.svg" alt="Logo utilisateur" class="svg_inscription">
                                        <input type="tel" id="nom" class="formInput" name = "nom" placeholder="Nom"><br><br>
                                    </div>

                                    <div>
                                    <img src="../views/assets/img/formulaires/user_icon_svg.svg" alt="Logo utilisateur" class="svg_inscription">
                                        <input type="text" id="prenom" class="formInput" name = "prenom" placeholder="Prénom"><br><br>
                                    </div>



                                                                        
                                    <div style="height:75.99px">
                                        <img src="../views/assets/img/formulaires/phone_icon_svg.svg" alt="Logo mot de passe" class="svg_inscription">
                                        <input type="tel" id="telephone" class="formInput" name = "telephone" maxlength="10" placeholder="N° de téléphone">
                                    </div>

                                </div>
                            </div>
                            <div class="formFlex_right">
                            
                                <div>
                                        <label id="label_mail" style="display:none;" for="mail">Adresse différente</label>
                                        <img src="../views/assets/img/formulaires/mail_icon_svg.svg" alt="Logo enveloppe" class="svg_inscription">
                                    
                                        <input type="tel" id="mail" class="formInput" name = "mail" placeholder="Adresse mail"><br><br>
                                    </div>

                                <div>
                                    <label style="display:none" id="label_confirmation_mail" style="display:none;" for="mail">Adresse différente</label>
                                    <img src="../views/assets/img/formulaires/mail_icon_svg.svg" alt="Logo enveloppe" class="svg_inscription">
                                    <input type="mail" id="mail_confirmation" class="formInput" name = "mail_confirmation" placeholder="Adresse mail confirmation"><br><br>
                                </div>

                            </div>
                        </div>
                    </form>

                    <button class="BTN_next" id="BTN_display_second_part" type="button">
                                    Confirmer et passer à la suite    
                    </button>
                </section>      
            </section>
        </section>
    </main>
    <script src="assets/js/inscription_premiere.js"></script>
</body>
</html>