<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Mentions légales du site Ed numérique.">
    <?php include("metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/mentionslegals.css">
    <title>Mentions légales</title>
</head>
<body>
    <?php include("views/header_annuaire.php"); ?>

    <h1>Informations légales</h1>

    <section id="mentionslegals">
        <p>Merci de lire attentivement les présentes modalités d'utilisation du présent site avant de le parcourir. En vous connectant sur ce site, vous acceptez sans réserve les présentes modalités.</p>

        <h2>Editeur du site</h2>
        <p>www.ed-numerique.fr</p>
        <p>par Ed numérique</p>
        <p>Via Innova – 177 bis avenue Louis Lumière 34400 LUNEL</p>
        <p>France</p>
        <p>Tél. : 09.72.50.75.54</p>
        <p>Gérant : Nicola Goubet</p>
        <p>https://www.ed-numerique.fr</p>
        <p>Ed numérique est une société anonyme simplifiée au capital de 1000€</p>
        <p>SIREN 888609997</p>
        <p>SIRET 88860999700018</p>

        <h2>Conditions d'utilisation</h2>
        <p>Le site accessible par l’url suivante : www.ed-numerique.fr est exploité dans le respect de la législation française. L'utilisation de ce site est régie par les présentes conditions générales. En utilisant le site, vous reconnaissez avoir pris connaissance de ces conditions et les avoir acceptées. Celles-ci pourront être modifiées à tout moment et sans préavis par la société Ed numérique.</p>
        <p>Ed numérique ne saurait être tenu pour responsable en aucune manière d’une mauvaise utilisation du service.</p>

        <h2>Limitation de responsabilité</h2>
        <p>Les informations contenues sur ce site sont aussi précises que possible et le site est périodiquement remis à jour, mais peut toutefois contenir des inexactitudes, des omissions ou des lacunes. Si vous constatez une lacune, erreur ou ce qui parait être un dysfonctionnement, merci de bien vouloir le signaler par email en décrivant le problème de la manière la plus précise possible (page posant problème, action déclenchante, type d’ordinateur et de navigateur utilisé, …).</p>
        <p>Tout contenu téléchargé se fait aux risques et périls de l'utilisateur et sous sa seule responsabilité. En conséquence, Ed numérique ne saurait être tenu responsable d'un quelconque dommage subi par l'ordinateur de l'utilisateur ou d'une quelconque perte de données consécutives au téléchargement.</p>
        <p>Les photos sont non contractuelles.</p>
        <p>Les liens hypertextes mis en place dans le cadre du présent site internet en direction d'autres ressources présentes sur le réseau Internet ne sauraient engager la responsabilité de Ed numérique.</p>

        <h2>Litiges</h2>
        <p>Les présentes conditions sont régies par les lois françaises et toute contestation ou litiges qui pourraient naître de l'interprétation ou de l'exécution de celles-ci seront de la compétence exclusive des tribunaux dont dépend le siège social de la société Ed numérique. La langue de référence, pour le règlement de contentieux éventuels, est le français.</p>

        <h2>Droit d'accès</h2>
        <p>En application de cette loi, les internautes disposent d’un droit d’accès, de rectification, de modification et de suppression concernant les données qui les concernent personnellement. Ce droit peut être exercé par voie postale auprès de Ed numérique Via Innova – 177 bis avenue Louis Lumière 34400 LUNEL ou par voie électronique à l’adresse email suivante : contact@ed-numerique.fr.</p>
        <p>Les informations personnelles collectées ne sont en aucun cas confiées à des tiers hormis pour l’éventuelle bonne exécution de la prestation commandée par l’internaute.</p>

        <h2>Confidentialité</h2>
        <p>Vos données personnelles sont confidentielles et ne seront en aucun cas communiquées à des tiers hormis pour la bonne exécution de la prestation. Les détails sur la politique de confidentialité sont disponibles ici (lien vers le document « RGPD et confidentialité »).</p>

        <h2>Propriété intellectuelle</h2>
        <p>Tout le contenu du présent site, incluant, de façon non limitative, les graphismes, images, textes, vidéos, animations, sons, logos, gifs et icônes ainsi que leur mise en forme sont la propriété exclusive de la société Ed numérique à l'exception des marques, logos ou contenus appartenant à d'autres sociétés partenaires ou auteurs.</p>
        <p>Toute reproduction, distribution, modification, adaptation, retransmission ou publication, même partielle, de ces différents éléments est strictement interdite sans l'accord exprès par écrit de Ed numérique. Cette représentation ou reproduction, par quelque procédé que ce soit, constitue une contrefaçon sanctionnée par les articles L.335-2 et suivants du Code de la propriété intellectuelle. Le non-respect de cette interdiction constitue une contrefaçon pouvant engager la responsabilité civile et pénale du contrefacteur. En outre, les propriétaires des Contenus copiés pourraient intenter une action en justice à votre encontre.</p>
        <p>Ed numérique est identiquement propriétaire des "droits des producteurs de bases de données" visés au Livre III, Titre IV, du Code de la Propriété Intellectuelle   (loi n° 98-536 du 1er juillet 1998) relative aux droits d'auteur et aux bases de données.</p>
        <p>Les utilisateurs et visiteurs du site internet peuvent mettre en place un hyperlien en direction de ce site, mais uniquement en direction de la page d’accueil, accessible à l’URL suivante : www.ed-numerique.fr, à condition que ce lien s’ouvre dans une nouvelle fenêtre. En particulier un lien vers une sous page (« lien profond ») est interdit, ainsi que l’ouverture du présent site au sein d’un cadre (« framing »), sauf l'autorisation expresse et préalable de Ed numérique.</p>
        <p>Pour toute demande d'autorisation ou d'information, veuillez nous contacter par email : contact@ed-numerique.fr. Des conditions spécifiques sont prévues pour la presse.</p>
        <p>Par ailleurs, la mise en forme de ce site a nécessité le recours à des sources externes dont nous avons acquis les droits ou dont les droits d'utilisation sont ouverts.</p>

        <h2>Hébergeur</h2>
        <p>OVH</p>
        <p>Plateforme de gestion et création de sites internet</p>
        <p>www.ovhcloud.com</p>
        <p>contact sur : https://www.ovhcloud.com/fr/contact/</p>

        <h2>Conditions de service</h2>
        <p>Ce site est proposé en langage HTML, pour un meilleur confort d'utilisation et un graphisme plus agréable, nous vous recommandons de recourir à des navigateurs modernes comme Safari, Firefox, Chrome... Les conditions générales d’utilisation et de ventes sont consultables <a href="?poldeconf">ici</a> (renvoi aux CGU). En cas d’inscription sur le site www.ed-numerique.fr, les Conditions générales d’utilisation et de vente ainsi que les conditions d’utilisation des données personnelles doivent être acceptées.</p>

        <h3>Informations et exclusion</h3>
        <p>Ed numérique met en œuvre tous les moyens dont elle dispose, pour assurer une information fiable et une mise à jour fiable de ses sites internet. Toutefois, des erreurs ou omissions peuvent survenir. L'internaute devra donc s'assurer de l'exactitude des informations auprès de Ed numérique, et signaler toutes modifications du site qu'il jugerait utile. Ed numérique n'est en aucun cas responsable de l'utilisation faite de ces informations, et de tout préjudice direct ou indirect pouvant en découler.</p>

        <h3>Cookies</h3>
        <p>Pour des besoins de statistiques et d'affichage, le présent site utilise des cookies. Il s'agit de petits fichiers textes stockés sur votre disque dur afin d'enregistrer des données techniques sur votre navigation. Certaines parties de ce site ne peuvent être fonctionnelles sans l’acceptation de cookies. Le paramétrage des cookies est disponible en cliquant ici (renvoi à la page « Cookies »).</p>

        <h3>Liens hypertextes</h3>
        <p>Les sites internet de Ed numérique peuvent offrir des liens vers d’autres sites internet ou d’autres ressources disponibles sur Internet.</p>
        <p>Ed numérique ne dispose d'aucun moyen pour contrôler les sites en connexion avec ses sites internet. Ed numérique ne répond pas de la disponibilité de tels sites et sources externes, ni ne la garantit. Elle ne peut être tenue pour responsable de tout dommage, de quelque nature que ce soit, résultant du contenu de ces sites ou sources externes, et notamment des informations, produits ou services qu’ils proposent, ou de tout usage qui peut être fait de ces éléments. Les risques liés à cette utilisation incombent pleinement à l'internaute, qui doit se conformer à leurs conditions d'utilisation.</p>
        <p>Les utilisateurs, les abonnés et les visiteurs des sites internet de Ed numérique ne peuvent mettre en place un hyperlien en direction de ce site sans l'autorisation expresse et préalable de Ed numérique.</p>
        <p>Dans l'hypothèse où un utilisateur ou visiteur souhaiterait mettre en place un hyperlien en direction d’un des sites internet de Ed numérique, il lui appartiendra d'adresser un email accessible sur le site afin de formuler sa demande de mise en place d'un hyperlien. Ed numérique se réserve le droit d’accepter ou de refuser un hyperlien sans avoir à en justifier sa décision.</p>

        <h3>Recherche</h3>
        <p>En outre, le renvoi sur un site internet pour compléter une information recherchée ne signifie en aucune façon que Ed numérique reconnaît ou accepte quelque responsabilité quant à la teneur ou à l'utilisation dudit site.</p>

        <h3>Précautions d'usage</h3>
        <p>Il vous incombe par conséquent de prendre les précautions d'usage nécessaires pour vous assurer que ce que vous choisissez d'utiliser ne soit pas entaché d'erreurs voire d'éléments de nature destructrice tels que virus, trojans, etc.</p>

        <h3>Responsabilité</h3>
        <p>Aucune autre garantie n'est accordée au client, auquel incombe l'obligation de formuler clairement ses besoins et le devoir de s'informer. Si des informations fournies par Ed numérique apparaissent inexactes, il appartiendra au client de procéder lui-même à toutes vérifications de la cohérence ou de la vraisemblance des résultats obtenus. Ed numérique ne sera en aucune façon responsable vis-à-vis des tiers de l'utilisation par le client des informations ou de leur absence contenues dans ses produits y compris un de ses sites Internet.</p>

        <h2>Contactez-nous</h2>
        <p>Ed numérique est à votre disposition pour tous vos commentaires ou suggestions. Vous pouvez nous écrire en français ou en anglais par courrier électronique à : <a href="mailto:contact@ed-numerique.fr.">contact@ed-numerique.fr.</a>.</p>
    </section>

    <?php include("views/footer.php");
    include("views/popup.php"); ?>

    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>

    <script src="views/assets/js/header_annuaire.js"></script>
</body>
</html>
