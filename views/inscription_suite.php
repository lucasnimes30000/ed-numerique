
<?php

    include("../controller/inc_session.php");
    sessionInit();

    if (sessionValidAdmin() !== true) {
        
        sessionBye(); 
        header("Location: connexion_admin.php#sessionerror");
        exit;
        
    }

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="../views/assets/css/profil_ed_backoffice.css">
    <link rel="stylesheet" href="../views/assets/css/inscription_second.css">
        <link rel="shortcut icon" type="image/png" href="../views/assets/img/logo.svg"/>
    <title>Inscription</title>
</head>
<body>
    <?php
        include("../views/header_admin.php");
    ?>
    <main>

        <h1 style = "font-size: 2+rem;">Inscription Ed</h1>
    
        <p class = "titre_inscription premiere_partie"> Veuillez remplir les champs suivants pour continuer l'inscription de<br><span id="nom_prenom" style="font-size:1.5rem;font-weight:bold;color:#258989"></span></p>

        <section id="inscription_ed">

            <p class = "titre_inscription">Informations personnelles</p>

            <p id="alert_info_perso"></p>

            <article id="info_perso">
                <section class="form_column">
                    <article id="update_nom" class="form_row">
                        <label for="update_nom">Nom</label>
                        <input type="text" placeholder="Nom" name="update_nom" id="input_update_nom">
                        <button type="button" id="update_nom_btn">Confirmer</button>
                    </article>

                    <article id="update_adresse_mail" class="form_row">
                        <label for="update_adresse_mail">Adresse mail</label>
                        <input type="text" placeholder="adresse_mail" name="update_adresse_mail" id="input_update_adresse_mail">
                        <button type="button" id="update_adresse_mail_btn">Confirmer</button>
                    </article>
                </section>

                <section class="form_column">
                    <article id="update_prenom" class="form_row">
                        <label for="update_prenom">Prénom</label>
                        <input type="text" placeholder="prenom" name="update_prenom" id="input_update_prenom">
                        <button type="button" id="update_prenom_btn">Confirmer</button>
                    </article>

                    <article id="update_telephone" class="form_row">
                        <label for="update_telephone">Numéro de téléphone</label>
                        <input type="text" placeholder="telephone" name="update_telephone" id="input_update_telephone">
                        <button type="button" id="update_telephone_btn">Confirmer</button>
                    </article>
                </section>
            </article>

            <article id="deuxieme_partie">
         


            <article id="deuxieme_partie">

                <p class = "titre_inscription">Indiquez son champ d’action</p>

                <p id="ville_alert"></p>

                <article id="article_ville" class="CTN_flex_ed">
                    <section class="ville">
                        <section action="" method="post">
                            <select id="choix_ville" name="villes">                  
                            </select>
                            <input id="user_id_choix" name="user_id_choix" type="hidden" value=<?= htmlentities($_GET['id_user'], ENT_QUOTES) ?>>
                            <button value="1" id="BTN_ajouter_ville" type="button">Ajouter</button>
                        </section>
                    </section>
                    <section class="choix">
                        <table>
                            <thead>
                                <tr>
                                    <th colspan="3">Choix</th>
                                </tr>
                            </thead>
                            <tbody id="body_choix">
                            </tbody>
                        </table>
                    </section>
                </article>  
            </article>

            <p class = "titre_inscription">Son statut</p>

            <p id="statut_alert"></p>
            <!-- <h2 id="creer_statut_alert"></h2>
            <h2 id="supprimer_statut_alert"></h2> -->

            <article id="statut_global">

                <section id="statut_form" class="formFlex_left">
                    <section id="section_statut"></section>
                    <button type="button" id="BTN_statut">Confirmer</button>
                </section>

                <section id="statut_droite">
                    <article id="creer_statut_form">
                        <label for="creer_statut_input" id="creer statut_label">Créer un statut</label><br>
                        <input placeholder="nom du statut" type="text" name="creer_statut_input" id="creer_statut_input"><br>
                        <button id="creer_statut_btn" type="button">Créer</button>
                    </article>

                    <article id="supprimer_statut_form">
                        <label for="supprimer_statut_input" id="supprimer statut_label">Supprimer un statut</label><br>
                        <input placeholder="id du statut" type="text" name="supprimer_statut_input" id="supprimer_statut_input"><br>
                        <button id="supprimer_statut_btn" type="button">Supprimer</button>
                    </article>
                </section>
            </article>


            <p id="siret_alert"></p>

            <section id="siret_form">
                <label for="statut_siret">Numéro Siret *</label><br>
                <input type="text" id="statut_siret" name="statut_siret" value="" placeholder="Numéro siret (obligatoire)">
                <br>
                <small id="small_siret" style="color:red;font-weight:bold;display:none">Numéro siret obligatoire</small>
                <small id="small_siret_longueur" style="color:red;font-weight:bold;display:none">14 chiffres requis</small>
                <button type="button" id="BTN_siret">Confirmer</button>
            </section>
            <br>

            <p id="quatrieme_partie" class = "titre_inscription">Il accompagne les diginautes sur ...</p>
            <br>

            <h4>Divers materiaux</h4>
        
            <p id="materiel_alert"></p>

            <article id="materiel_global">
                <section id="materiel_form">
                    <section id="materiel"></section>
                    <button type="button" id="BTN_confirmation_materiel">Confirmer</button>
                </section>

                <section id="materiel_droite">
                    <article id="creer_materiel_form">
                        <label for="creer_materiel_input" id="creer materiel_label">Créer un materiel</label><br>
                        <input placeholder="nom du materiel" type="text" name="creer_materiel_input" id="creer_materiel_input"><br>
                        <button id="creer_materiel_btn" type="button">Créer</button>
                    </article>

                    <br>
                    <article id="supprimer_materiel_form">
                    <label for="supprimer_materiel_input" id="supprimer materiel_label">Supprimer un materiel</label><br>
                    <input placeholder="id du materiel" type="text" name="supprimer_materiel_input" id="supprimer_materiel_input"><br>
                    <button id="supprimer_materiel_btn" type="button">Supprimer</button>
                    </article>
                </section>
            </article>

            <h4>Image, Son et Jeux</h4>

            <p id="image_jv_alert"></p>
            
            <article id="image_jv_global">
                
                <section id="image_jv_form">
                    <section id="image_jv"></section>
                    <button type="button" id="BTN_confirmation_image_jv">Confirmer</button>
                </section>

                <section id="image_jv_droite">
                    <article id="creer_image_jv_form">
                        <label for="creer_image_jv_input" id="creer image_jv_label">Créer un image_jv</label><br>
                        <input placeholder="nom de l'élément" type="text" name="creer_image_jv_input" id="creer_image_jv_input"><br>
                        <button id="creer_image_jv_btn" type="button">Créer</button>
                    </article>

                    <br>
                    <article id="supprimer_image_jv_form">
                        <label for="supprimer_image_jv_input" id="supprimer image_jv_label">Supprimer un image_jv</label><br>
                        <input placeholder="id de l'élément" type="text" name="supprimer_image_jv_input" id="supprimer_image_jv_input"><br>
                        <button id="supprimer_image_jv_btn" type="button">Supprimer</button>
                    </article>
                </section>
            </article>
            
            <h4>Ses logiciels et systemes</h4>

            <p id="logiciel_systeme_alert"></p>

            <article id="logiciel_systeme_global">

                <section id="logiciel_systeme_form">
                    <section id="logiciel_systeme"></section>
                    <button type="button" id="BTN_confirmation_logiciel_systeme">Confirmer</button>
                </section>
 
                <section class="logiciel_systeme_droite">
                    <article id="creer_logiciel_systeme_form">
                        <label for="creer_logiciel_systeme_input" id="creer logiciel_systeme_label">Créer un logiciel & outil</label><br>
                        <input placeholder="nom du logiciel & outil" type="text" name="creer_logiciel_systeme_input" id="creer_logiciel_systeme_input"><br>
                        <button id="creer_logiciel_systeme_btn" type="button">Créer</button>
                    </article>

                    <article id="supprimer_logiciel_systeme_form">
                        <label for="supprimer_logiciel_systeme_input" id="supprimer logiciel_systeme_label">Supprimer un logiciel & outil</label><br>
                        <input placeholder="id du logiciel & outil" type="text" name="supprimer_logiciel_systeme_input" id="supprimer_logiciel_systeme_input"><br>
                        <button id="supprimer_logiciel_systeme_btn" type="button">Supprimer</button>
                    </article>
                </section>
            </article>

            <h4>Ses démarches administratives</h4>

            <p id="demarche_alert"></p>

            <article id="demarche_global">

                <section id="demarche_form">
                    <section id="demarche"></section>
                    <button type="button" id="BTN_confirmation_demarche">Confirmer</button>
                </section>
 
                <section class="demarche_droite">
                    <article id="creer_demarche_form">
                        <label for="creer_demarche_input" id="creer demarche_label">Créer un logiciel & outil</label><br>
                        <input placeholder="nom du logiciel & outil" type="text" name="creer_demarche_input" id="creer_demarche_input"><br>
                        <button id="creer_demarche_btn" type="button">Créer</button>
                    </article>

                    <article id="supprimer_demarche_form">
                        <label for="supprimer_demarche_input" id="supprimer demarche_label">Supprimer un logiciel & outil</label><br>
                        <input placeholder="id du logiciel & outil" type="text" name="supprimer_demarche_input" id="supprimer_demarche_input"><br>
                        <button id="supprimer_demarche_btn" type="button">Supprimer</button>
                    </article>
                </section>
            </article>

            <h4>Ses infrastructures</h4>

            <p id="infrastructure_alert"></p>

            <article id="infrastructure_global">

                <section id="infrastructure_form">
                    <section id="infrastructure"></section>
                    <button type="button" id="BTN_confirmation_infrastructure">Confirmer</button>
                </section>
 
                <section class="infrastructure_droite">
                    <article id="creer_infrastructure_form">
                        <label for="creer_infrastructure_input" id="creer infrastructure_label">Créer un logiciel & outil</label><br>
                        <input placeholder="nom du logiciel & outil" type="text" name="creer_infrastructure_input" id="creer_infrastructure_input"><br>
                        <button id="creer_infrastructure_btn" type="button">Créer</button>
                    </article>

                    <article id="supprimer_infrastructure_form">
                        <label for="supprimer_infrastructure_input" id="supprimer infrastructure_label">Supprimer un logiciel & outil</label><br>
                        <input placeholder="id du logiciel & outil" type="text" name="supprimer_infrastructure_input" id="supprimer_infrastructure_input"><br>
                        <button id="supprimer_infrastructure_btn" type="button">Supprimer</button>
                    </article>
                </section>
            </article>
        </section>  

        <form action="../controller/mail_inscription.php" method="post">
            <input type="hidden" name="id_user" value="<?= htmlspecialchars($_GET['id_user']) ?>">
            <input id="mail_user" type="hidden" name="mail_user" value="">
            <input id="prenom_user" type="hidden" name="prenom_user" value="">
            <input id="nom_user" type="hidden" name="nom_user" value="">
            <button>Confirmer l'inscription</button>  
        </form>  
        </main>
    <script type="text/javascript" src="assets/js/inscription_seconde.js"></script>
</body>
</html>
