<?php

    include("../controller/inc_session.php");
    sessionInit();

    if (sessionValidAdmin() !== true) {
        
        sessionBye(); 
        header("Location: connexion_admin.php#sessionerror");
        exit;
        
    }

    include('../controller/id.php');

    try {
        $sql_user = "SELECT * FROM TBLuser ORDER BY LOWER(nom_user) ASC";
        $req_user = $bdd->prepare($sql_user);
        $req_user->execute();

        $info_perso = $req_user->fetchAll();

    } catch (PDOException $e) {
        echo "Failed to create user: " . $e->getMessage();
    }

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="../views/assets/css/annuaire_backoffice.css">
    <link rel="shortcut icon" type="image/png" href="../views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="../views/assets/css/footer.css">

    <title>Annuaire admin</title>
</head>
<body>
    <?php
        include("../views/header_admin.php");
    ?>
    <main>
<?php
        foreach($info_perso as $value) {
?>

            <section class="vignette_ed">

<?php
            try {

                $id_user = htmlentities($value['id_user'], ENT_QUOTES);
            
                $sql_ville = "SELECT DISTINCT TBLville.nom_ville, TBLville.id_ville, TBLville.code_postal
                            FROM TBLassoc_ville_user 
                            INNER JOIN TBLville
                            ON TBLassoc_ville_user.id_ville = TBLville.id_ville
                            INNER JOIN TBLuser
                            ON TBLassoc_ville_user.id_user = TBLuser.id_user
                            WHERE TBLuser.id_user = :id_user
                            ORDER BY lower(nom_ville)";
            
                $req_ville = $bdd->prepare($sql_ville);
                $req_ville->execute([
                    ':id_user' => $id_user
                ]);
                $resultat_ville = $req_ville->fetchAll();
            
            } catch (PDOException $e) {
            
                echo "Failed to load user choices " . $e->getMessage();
                
            }
            foreach($resultat_ville as $ville) {
                echo "<article class='nom_ville'>" . htmlentities($ville['nom_ville'], ENT_QUOTES) . "</article>";
            }
            echo "<article class='nom_prenom'>" . htmlentities($value['prenom'], ENT_QUOTES) . " " . htmlentities($value['nom_user'], ENT_QUOTES) . "</article>";
            echo "<article class='telephone'>" . htmlentities($value['telephone'], ENT_QUOTES) . "</article><br>";
            echo "<img class='photo_profil_ed' src='assets/user/" . htmlentities($value['id_user'], ENT_QUOTES) . "/PP/" . htmlentities($value['url_photo'], ENT_QUOTES) . "' alt='Photo profil user'>";
            echo "<button type='button' class='BTN_profil' value='" . htmlentities($value['id_user'], ENT_QUOTES) . "'>Voir le profil</button>";
?>

            </section>

<?php
        }
?>
    </main>
    <script type="text/javascript" src="../views/assets/js/annuaire_backoffice.js"></script>
</body>
</html>
