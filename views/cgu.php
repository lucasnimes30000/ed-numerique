<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Les conditions générales d’utilisation définissent et encadrent les modalités d’accès et de navigation sur la plateforme Ed numérique.">
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <?php include("metadonnee.php"); ?>
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/cgu.css">
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <title>CGU</title>
</head>
<body>
    <?php include("views/header_annuaire.php") ?>

    <section class="CGU">
        <article class="enTete">
            <h1>Conditions Générales d’Utilisation</h1>
            <h3>01.04.2021</h3>
            <p>Les présentes Conditions Générales d’Utilisation (ci-après CGU) de la plateforme ED-NUMÉRIQUE sont applicables à toutes commandes passées via le Site internet <a href="www.ed-numerique.fr">www.ed-numerique.fr</a>.</p>
        </article>
        <article class="preanbule">
            <h3>PRÉAMBULE</h3>
            <p>ED-NUMÉRIQUE est une plateforme numérique spécialisée dans la mise en relation entre des utilisateurs ayant un besoin de formation en numérique et des utilisateurs qui eux sont susceptibles de les aider.</p>
            <ul>
                <li>Le service se caractérise par l’accès gratuit à un annuaire sélectif où sont référencés un nombre déterminé d’intervenants capables d’accompagner ou de former au numérique.</li>
                <li>Ce service permet ainsi une visibilité aux intervenants qui sont présents sur la plateforme, en contrepartie d’une cotisation mensuelle.</li>
            </ul>
            <p>Le contenu intégral de l’offre est disponible sur le Site <a href="www.ed-numerique.fr">www.ed-numerique.fr</a>.</p>
            <p>ED-NUMÉRIQUE se réserve la faculté de modifier ses CGU à tout moment. Les modifications sont notifiées quinze (15) jours avant la date de leur entrée en vigueur aux utilisateurs de la plateforme Ed-numérique, dans les conditions prévues par le règlement UE du 20 juin 2019.</p>
            <p>En cas de modification des CGU, les CGU applicables sont celles en vigueur à la date de la commande ou de la souscription de l’abonnement.</p>
            <p>La nullité d’une ou plusieurs clauses des présentes CGU n’entraînera pas la nullité de l’ensemble de celles-ci, lesquelles continueront de produire normalement leurs effets.</p>
        </article>
        <article class="articleUn">
            <h3>Article 1 – Description du service</h3>
            <h4>1.1 Définition</h4>
            <p>ED-NUMÉRIQUE est une plateforme de mise en relation.</p>
            <p>Elle permet à des personnes ayant des difficultés dans l’utilisation des outils numériques, les Diginautes, de recourir aux services de personnes pouvant les accompagner dans la maîtrise de ces outils, les "Ed’s".</p>
            <p>Les Diginautes ont également la possibilité de recourir à des ateliers thématiques gratuits, s’ils adhèrent à l'association EdDi.</p>
 
            <h4>1.2 Fonctionnement du Service</h4>
            <p>Le service offert par ED-NUMÉRIQUE est disponible 7 jours sur 7, selon les disponibilités de nos intervenants.</p>
            <p>Il permet essentiellement au Diginaute de trouver un intervenant (Ed) proche de son domicile pour une problématique numérique ou informatique.</p>
            <p>La plateforme ED-NUMÉRIQUE permet le contact entre le Diginaute et le Ed en vue de ladite intervention, en affichant les coordonnées téléphoniques et email du Ed.</p>

            <h4>1.3 Territoire</h4>
            <p>Le service Ed-numérique est disponible à ce jour dans les secteurs de l’Hérault et du Gard.</p>
            <p>La zone d’intervention sera susceptible d’être élargie à d’autres départements ; toute nouvelle ouverture sera précisée à l’avance sur le site internet <a href="www.ed-numerique.fr">www.ed-numerique.fr</a>.</p>
        </article>
        <article class="articleDeux">
            <h3>Article 2 - Informatique et libertés</h3>
            <h4>2.1 Données personnelles</h4>
            <p>L’Utilisateur est informé que, pour les besoins du Service, ED-NUMÉRIQUE collecte et traite leurs données confidentielles telles que l’adresse du domicile où a lieu l’intervention.</p>
            <p>Elle traite des données telles que le genre ou l’âge à des fins statistiques. </p>
            <p>Conformément au règlement no 2016/679, sur la protection des données (RGPD), ED-NUMÉRIQUE s'engage à protéger les données à caractère personnel de l’Utilisateur et du Bénéficiaire lorsqu’il s’agit de deux personnes distinctes.</p>
            <p>Les détails concernant les données personnelles collectées, leurs utilisations et leur temps de conservation sont consultables dans la politique de confidentialité et RGPD (renvoi au consentement sur l’utilisation des données personnelles).</p>
            <p>Conformément aux dispositions de la loi n°78-17 du 6 janvier 1978 dite "Informatique et Libertés" et sous réserve de justifier de son identité, l’Utilisateur dispose du droit de demander à ce que les données à caractère personnel le concernant soient rectifiées, complétées, mises à jour, verrouillées ou effacées si ces données sont inexactes, incomplètes, équivoques, périmées, ou si la collecte, l'utilisation, la communication ou la conservation de ces données est interdite.</p>
            <p>L’Utilisateur peut exercer ces droits en envoyant un courrier postal accompagné d'un justificatif d'identité à l'adresse suivante : ED-NUMÉRIQUE - Informatique et Libertés – Via Innova, 177 bis avenue Louis Lumière, 34400 Lunel.</p>

            <h4>2.2 Cookies</h4>
            <p>Afin de faciliter la navigation des utilisateurs sur la plateforme Ed-numérique, des cookies sont utilisés. L’utilisateur a la possibilité de paramétrer l’utilisation des cookies via la politique en matière de cookies (renvoi au consentement et au paramétrage des cookies).</p>
        </article>
        <article class="articleTrois">
            <h3>Article 3 – Inscription</h3>
            <p>L'inscription en tant qu’intervenant Ed sur la plateforme entraîne l'acceptation sans réserve des présentes CGU.</p>
            <h4>3.1 Inscription en tant que Ed</h4>
            <p>L’inscription en tant que Ed est réalisée en ligne sur le site <a href="www.ed-numerique.fr">www.ed-numerique.fr</a>.</p>
            <p>Le candidat-intervenant contacte la plateforme en qualité de potentiel Ed.</p>
            <p>Cette possibilité est ouverte aux personnes entrepreneurs individuels spécialisés en informatique, ou aux personnes ayant une appétence en informatique et à l’utilisation d’outils numériques, de préférence en recherche d’emploi, et/ou d’un complément de revenu avec le statut de salarié d’un particulier pour les Ed ne disposant de SIRET.</p>
            <p>Le candidat doit fournir lors de l’inscription les justificatifs de sa situation et de ses compétences.</p>
            <p>L’inscription en ligne ne vaut que présentation de candidature, et l’inscription est définitive au terme d’un entretien physique ou par téléphone.</p>
            <p>Une fois l’inscription validée, le Ed est visible sur l’annuaire (plateforme) et joignable par les utilisateurs par les moyens de communications affichés sur la plateforme.</p>
            <p>Chaque intervenant signe la « charte Ed numérique» et s’engage à respecter les engagements de Ed numérique vis-à-vis des Diginautes en termes de politesse, de ponctualité, de confidentialité, de qualité, de sécurité et de pédagogie.</p>
            <p>Cette charte est disponible sur le site Internet <a href="www.ed-numerique.fr">www.ed-numerique.fr</a>.</p>

            <h4>3.2 Restriction, suspension, résiliation</h4>
            <p>L’accès d’un Ed à son espace personnel peut être restreint, suspendu ou résilié en cas de manquements graves et répétés à ses obligations contractuelles vis-à-vis de la plateforme et aux obligations prévues aux présentes Conditions Générales d’Utilisation.</p>
            <p>Conformément aux dispositions de l’article 4 du règlement du 20 juin 2019, la décision de limiter/suspendre le compte d’un Ed est notifiée à l’intéressé par lettre recommandée avec AR 15 jours avant l’entrée en vigueur de la décision. La notification comporte l’exposé de l’ensemble des motifs de la décision. En cas de résiliation de l’accès, le délai de 15 jours est porté à 30 jours.</p>
            <p>En cas de limitation/suspension/résiliation, le Ed en cause peut recourir à la procédure de traitement interne des plaintes pour clarifier les faits et les circonstances.</p>
        </article>
        <article class="articleQuatre">
            <h3>Article 4 – Espace personnel</h3>
            <h4>Espace Ed</h4>
            <p>Dans son espace personnel, le Ed trouvera un historique des factures afférant à l’abonnement à la plateforme, et l’accès à sa fiche Ed visible des utilisateurs.</p>
        </article>
        <article class="articleCinq">
            <h3>Article 5 – Prix et règlement</h3>
            <p>Le tarif pratiqué par Ed-numérique est transparent et unique :</p>
            <p>Le service est accessible et gratuit pour tout utilisateur ayant besoin de formation au numérique.</p>
            <p>Pour les intervenants visibles sur la plateforme, le prix du service est basé sur une cotisation mensuelle payée par l’intervenant lui-même, au titre de l’accès aux différents services proposés par la plateforme. Le niveau de la cotisation et la période d’engagement sont fixés contractuellement avec chaque Ed.</p>
            <p>La cotisation est réglée par voie de prélèvement SEPA en suivant les coordonnées bancaires indiquées par le Ed.</p>
        </article>
        <article class="articleSix">
            <h3>Article 6 - Informations du Ed</h3>
            <p>L’Ed s'engage à fournir à ED-NUMÉRIQUE des informations exactes : numéro de SIRET et numéro d’agrément SAP si applicable, coordonnées complètes, et coordonnées bancaires pour la mise en place du prélèvement SEPA.</p>
        </article>
        <article class="articleSept">
            <h3>Article 7 - Obligations de ED-NUMÉRIQUE</h3>
            <p>ED-NUMÉRIQUE s'engage à assurer le bon fonctionnement du Service et à prendre les mesures nécessaires au maintien de la continuité et de la qualité de ce Service.</p>
            <p>Ed-numérique n’assure pas un mandat de représentation commerciale.</p>
            <p>Sa responsabilité ne peut être engagée sur ce fondement.</p>
            <p>Il est cependant convenu que ED-NUMÉRIQUE ne sera pas tenue de fournir le Service en cas de force majeure tels que grèves, intempéries, guerres, embargos, défaillances du réseau électrique, du réseau Internet, de satellites, manquements des opérateurs de téléphonie mobile, suicide collectif des experts comptables, chute d’astéroïdes, épidémie de super grippe, invasion de reptiliens, invasion d’aliens, retour du Commandeur Spoke, attaque d’un tyrannosaure despotique, inversion du champ magnétique, attaque des borgs, retour à la civilisation Klingon, complot sith, invasion zombie, invasion de bio-organisme Lunaire, ou tout autre évènement de nature à provoquer l’effondrement de la société humaine ou vous empêcher d’aller aux toilettes.</p>
        </article>
        <article class="articleHuit">
            <h3>Article 8 – Procédure de traitement interne des plaintes</h3>
            <p>Dans le cas d’une restriction/supension/résiliation de l’accès à son compte, un Ed peut, une fois que la décision lui a été notifiée, recourir à la procédure de traitement interne des plaintes.</p>
            <p>Il adresse un courrier recommandé avec AR au responsable du traitement interne des plaintes, dans lequel il expose les faits et circonstances susceptibles d’apporter des éclairages sur la situation, à l’adresse suivante : ED-NUMÉRIQUE – Service de traitement interne des plaintes – Via Innova, 177 bis avenue Louis Lumière, 34400 Lunel. Le responsable du service de traitement interne des plaintes, une fois qu’il a pris connaissance du courrier du Ed, le convoque, dans les plus brefs délais, à un entretien, quel que soit sa forme. Au terme de cet entretien, le responsable du service de traitement interne des plaintes confirme ou infirme la décision de restriction/suspension/résiliation de l’accès au compte du Ed.</p>
        </article>
        <article class="articleNeuf">
            <h3>Article 9 - Responsabilité et assurance</h3>
            <h4>9.1 Exclusion de responsabilité</h4>
            <p>La responsabilité de ED-NUMÉRIQUE ne saurait être engagée si l'inexécution ou la mauvaise exécution du service est imputable au ED ou au DIGINAUTE ou à des contraintes indépendantes de la volonté de ED-NUMÉRIQUE telles qu'énoncées ci- après.</p>
            <p>ED-NUMÉRIQUE décline, entre autres, toute responsabilité :</p>
            <ul>
            <li>Pour tout dommage lié à un dysfonctionnement ou à un mauvais état du matériel informatique et/ou numérique antérieurement à sa prise en charge ;</li>
            <li>Pour tout logiciel ou matériel dont le Diginaute bénéficiaire de l’intervention ne possède pas la licence.</li>
            </ul>

            <h4>9.2 Assurance</h4>
            <p>Conformément à la législation en vigueur, ED-NUMÉRIQUE dispose d’une responsabilité civile professionnelle auprès d'une compagnie d’assurance notoirement solvable, qui couvre les risques liés à son activité.</p>
        </article>
        <article class="articleDix">
            <h3>Article 10 - Propriété intellectuelle</h3>
            <p>ED-NUMÉRIQUE est titulaire exclusif des droits de propriété intellectuelle relatifs au Service d’intermédiation proposé par la plateforme Ed-numérique. </p>
            <p>ED-NUMÉRIQUE est une marque déposée propriété de la Société ED-NUMÉRIQUE.</p>
            <p>Aucune stipulation des présentes CGU ne peut être interprétée comme opérant un quelconque transfert de droit de propriété intellectuelle sur ces signes au profit de l’Utilisateur.</p>
        </article>
        <article class="articleOnze">
            <h3>Article 11 - Entrée en vigueur et durée des CGU</h3>
            <p>Les présentes CGU entrent en vigueur :</p>
            <p>Au jour de l’inscription définitive comme intervenant (Ed)</p>
        </article>
        <article class="articleDouze">
            <h3>Article 12 - Loi applicable et juridiction compétente</h3>
            <p>Le Contrat est régi par la loi française.</p>
            <p>Toute contestation relative au Contrat sera soumise à la compétence exclusive du tribunal de commerce de Montpellier, y compris en cas de référé, appels en garantie et pluralité de défendeurs, à l'exception des litiges concernant des personnes non commerçants et pour lesquelles les règles légales d'attribution de compétence s'appliquent.</p>
        </article>
    </section>
    
    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>

    <?php include("views/footer.php") ?>
    <script type="text/javascript" src="views/assets/js/header_annuaire.js"></script>
</body>
</html>