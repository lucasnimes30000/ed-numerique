<?php

    include("../controller/inc_session.php");
    sessionInit();

    if (sessionValidAdmin() !== true) {
        
        sessionBye(); 
        header("Location: connexion_admin.php#sessionerror");
        exit;
        
    }

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="../views/assets/css/gerer_ville.css">
    <link rel="shortcut icon" type="image/png" href="../views/assets/img/logo.svg"/>

    <title>Gérer les villes</title>
</head>
<body>

    <?php
        include("../views/header_admin.php");
    ?>
    <main>

        <h2>Gérer les villes</h2>
        <section id="global">
            <p id="alert"></p>
            
            <article id="buttons">

            </article>
        </section>
    </main>
    <?php
    ?>
<script type="text/javascript" src="assets/js/gerer_ville.js"></script>
</body>
</html>