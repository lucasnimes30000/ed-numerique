<?php

    include("../controller/inc_session.php");
    sessionInit();

    if (sessionValidAdmin() !== true) {
        
        sessionBye(); 
        header("Location: connexion_admin.php#sessionerror");
        exit;
        
    }

    include("../controller/id.php");

    try {
        $sql_user = "SELECT TBLuser.url_photo, TBLuser.telephone, TBLuser.prenom, TBLuser.nom_user, TBLfiche_ed.savoir_faire, TBLfiche_ed.j_aime, TBLfiche_ed.me_connaitre, TBLfiche_ed.info_complementaire
                    FROM TBLfiche_ed
                    INNER JOIN TBLassoc_fiche_ed
                        ON TBLfiche_ed.id_fiche_ed = TBLassoc_fiche_ed.id_fiche_ed
                    INNER JOIN TBLuser
                        ON TBLuser.id_user = TBLassoc_fiche_ed.id_user
                    WHERE TBLuser.id_user = :id_user";
        $req_user = $bdd->prepare($sql_user);
        $req_user->execute([
            ':id_user' => htmlspecialchars_decode($_GET['id_user'], ENT_QUOTES)
        ]);

        $info_perso = $req_user->fetch();

    } catch (PDOException $e) {
        echo "Failed to select informations: " . $e->getMessage();
    }

    try {

        $sql_ville = "SELECT DISTINCT TBLville.nom_ville
                    FROM TBLassoc_ville_user 
                    INNER JOIN TBLville
                    ON TBLassoc_ville_user.id_ville = TBLville.id_ville
                    INNER JOIN TBLuser
                    ON TBLassoc_ville_user.id_user = TBLuser.id_user
                    WHERE TBLuser.id_user = :id_user
                    ORDER BY lower(nom_ville)";

        $req_ville = $bdd->prepare($sql_ville);
        $req_ville->execute([
            ':id_user' => htmlspecialchars_decode($_GET['id_user'], ENT_QUOTES)
        ]);
        $resultat_ville = $req_ville->fetchAll();
        
    } catch (PDOException $e) {

        echo "Failed to load user choices " . $e->getMessage();
        
    }

    // Formatage du numéro de téléphone pour avoir des points " . " tous les deux caractères

    $phoneNumber = htmlspecialchars_decode($info_perso['telephone'], ENT_QUOTES);

    $un = substr($phoneNumber, 0, 2);
    $deux = substr($phoneNumber, 2, 2);
    $trois = substr($phoneNumber, 4, 2);
    $quatre = substr($phoneNumber, 6, 2);
    $cinq = substr($phoneNumber, 8, 2);

    $phoneNumber = $un . '.' . $deux . '.' . $trois . '.' . $quatre . '.' . $cinq;

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="../views/assets/css/fiche_ed_backoffice.css">
    <link rel="shortcut icon" type="image/png" href="../views/assets/img/logo.svg"/>
    <title>Fiche ed - admin</title>
</head>
<body>

<?php
    include("../views/header_admin.php");
?>
    <main>

        <section id="haut">
            <input id="user_id_choix" type="hidden" value="<?= htmlspecialchars($_GET['id_user'], ENT_QUOTES) ?>" name="user_id_choix">
            <button id="btn_retour" type="button">Retour</button>
            <article id="milieu">
                <img src="../views/assets/user/<?= htmlspecialchars_decode($_GET['id_user'], ENT_QUOTES) ?>/PP/<?= htmlspecialchars_decode($info_perso['url_photo'], ENT_QUOTES) ?>" alt="photo de profil utilisateur" id="PP">
                <p><?= htmlspecialchars_decode($info_perso['nom_user'], ENT_QUOTES) . " " . htmlspecialchars_decode($info_perso['prenom'], ENT_QUOTES) ?><br><span class="italic"><?= htmlspecialchars_decode($phoneNumber, ENT_QUOTES) ?></span></p>
            </article>
            <article id="droite">

            <input type="hidden" name="" id="url_id" value="/views/assets/user/<?= htmlspecialchars_decode($_GET['id_user'], ENT_QUOTES) ?>/PP/<?= htmlspecialchars_decode($info_perso['url_photo'], ENT_QUOTES) ?>">

            <?php foreach ($resultat_ville as $row) { 
                echo htmlspecialchars_decode($row['nom_ville'], ENT_QUOTES) . "<br>";
            } ?>
            </article>
        </section>

        <section id="btn_haut">
            <article class="button">
                <button id="btn_mail" type="button">Envoyer un mail ➔</button>
            </article>
            <article class="button">
                <button type="button" style="display:none;">Recommander ce Ed</button>
            </article>
        </section>

        <hr>

        <section id="alert"></section>

        <section id="categories_full">
            <article id="top_categorie">
                <section id="connaitre" class="categorie">
                    <h3 class="titre_categorie">Me connaître</h3>
                    <textarea maxlength="500"  id="text_connaitre" cols="30" rows="10"><?= htmlspecialchars_decode($info_perso['me_connaitre'], ENT_QUOTES) ?></textarea>
                    <small>(maximum 500 caractères)</small>

                    <button name="btn_connaitre" type="submit" id="update_connaitre" class="BTN_head">Modifier</button>

                    <section id="informations" class="">
                        <h3 class="titre_categorie">Infos complémentaire</h3>
                        <textarea maxlength="500"  id="text_informations" cols="30" rows="10"><?= htmlspecialchars_decode($info_perso['info_complementaire'], ENT_QUOTES) ?></textarea>
                        <small>(maximum 500 caractères)</small>

                        <button name="btn_informations" type="submit" id="update_informations" class="BTN_head">Modifier</button>
                    </section>

                    <section id="aime" class="">
                    <h3 class="titre_categorie">J'aime ...</h3>
                    <textarea maxlength="500"  cols="30" rows="10" id="text_aime"><?= htmlspecialchars_decode($info_perso['j_aime'], ENT_QUOTES) ?></textarea>
                    <small>(maximum 500 caractères)</small>

                    <button name="btn_j_aime" type="submit" id="update_j_aime" class="BTN_head">Modifier</button>
                </section>
                </section>

                <section id="competences" class="categorie">
                    <h3 class="titre_categorie">Mes compétences</h3>
                    <article id="liste_competence"></article>
                </section>
            </article>
        </section>
    
        <section class="commentaireEd">
        </section>
    </main>

    <script type="text/javascript" src="assets/js/fiche_ed_backoffice.js"></script>
</body>
</html>