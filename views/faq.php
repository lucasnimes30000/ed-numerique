<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Rubrique regroupant les réponses aux questions les plus fréquemment posées.">
    <?php include("metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/faq.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="stylesheet" href="views/assets/css/footer.css">

    <title>F.A.Q</title>
</head>
<body style="background-color: #E7E7E7 !important;">
    <?php include("views/header_annuaire.php") ?>
    <main>
        <h1>Questions fréquentes</h1>
        <button id="BtnClient">Client</button>
        <button id="BtnEd">Intervenant / Ed</button>
        <section id="client" style="display: flex;">
            <article>
                <button type="button" class="collapsible">Préparer votre formation avec votre intervenant</button>
                <aside class="content">
                    <p>Avant votre formation avec votre intervenant</p>
                    <p>&nbsp&nbsp- Les intervenants sont équipés de masques & de gel hydroalcoolique & sont formés aux gestes barrières.</p>
                    <p>Pendant votre formation avec votre intervenant</p>
                    <p>&nbsp&nbsp- Nous vous conseillons d'aérer les pièces à chaque début d'intervention et si possible pendant toute la durée de votre formation.</p>
                    <p>&nbsp&nbsp- Une distance minimum d'1 mètre dans la mesure du possible doit être établie entre vous & votre intervenant.</p>
                    <p>Après votre formation avec votre intervenant</p>
                    <p>&nbsp&nbsp- Pour éviter tout contact physique, toutes vos demandes d'intervention devront de préférence être réglées par chèque.</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Que faire si un intervenant n'a pas répondu à ma sollicitation</button>
                <aside class="content">
                    <p>Si un intervenant ne vous répond pas, il est peut-être sur une autre intervention.</p>
                    <p>Si toutefois l'attente est trop importante, vous avez la possibilité de contacter un autre intervenant de notre plateforme.</p>
                    <p>Vous pouvez également nous contacter au contact@ed-numerique.fr, nous sommes là pour vous aider.</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Que faire si je ne trouve pas d'intervenant répondant à mes besoins</button>
                <aside class="content">
                    <p>Si votre intervenant ne semble pas avoir les compétences adéquates pour vous aider, vous pouvez vous adresser à un autre intervenant sur notre annuaire, ou nous contacter à l'adresse mail suivante : contact@ed-numerique.fr</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Quels systèmes d'exploitation et quels navigateurs sont compatibles avec la plateforme ?</button>
                <aside class="content">
                    <p>Tous les systèmes d'exploitation peuvent faire fonctionner la plateforme Ed numérique ; celle-ci est optimisée pour les navigateurs web exclusivement pour l'instant.</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Est-ce qu'Ed numérique est responsable de la prestation réalisée par l'intervenant ?</button>
                <aside class="content">
                    <p>La mission d'Ed numérique est de vous mettre en relation avec des intervenants numériques (les Eds) de votre région. Par conséquent, la prestation de la responsabilité de l'intervenant, mais nous veillons à ce que nos Ed's respectent scrupuleusement notre Charte de Qualité (URL)</p>
                </aside>
            </article>
        </section>
        <section id="intervenantEd" style="display: none;">
            <article>
                <button type="button" class="collapsible">Je n’ai pas reçu le mail de confirmation de mon inscription </button>
                <aside class="content">
                    <p>Dans le cas où vous n’auriez pas reçu notre mail de confirmation, vérifiez s’il n’a pas atterri dans vos courriels indésirables. Sinon, contactez-nous à l'adresse contact@ed-numerique.fr en précisant les informations suivantes : nom, prénom, numéro de téléphone, ville où vous intervenez. Nous vérifierons si votre inscription est bien effective et vous renverrons une confirmation.</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Processus de validation pour devenir intervenant Ed numérique</button>
                <aside class="content">
                    <p>Faire partie d'Ed numérique nécessite plusieurs étapes (entrevue téléphonique, sélection, vérification des compétences, recommandation). Une fois ces étapes passées, nous créérons avec vous votre fiche de présentation, et mettrons en place votre profil sur la plateforme. A partir de là, vous serez visible et joignable depuis la plateforme Ed numérique pour les clients de vos villes !</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Comment créer votre micro entreprise ?</button>
                <aside class="content">
                    <p>Vous souhaitez devenir intervenant numérique et vous avez besoin d'un numéro de SIRET ?
                        Connectez-vous ici : <a href="https://www.autoentrepreneur.urssaf.fr/portail/accueil/creer-mon-auto-entreprise.html">https://www.autoentrepreneur.urssaf.fr/portail/accueil/creer-mon-auto-entreprise.html</a>. Sinon, écrivez-nous à contact@ed-numerique.fr, nous pouvons vous assister !</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Mettre fin à sa micro-entreprise</button>
                <aside class="content">
                    <p>1) Munissez-vous du document d’inscription.</p>
                    <p>2) Rendez-vous sur le site l’autoentrepreneur.fr : cliquez sur « Gérer mon auto-entreprise », puis cliquez sur « Cessez mon auto-entreprise »</p>
                    <p>3) Faites comme indiqué ci-dessous :
                        d’abord, pour le domaine d’activité, faites défiler la liste pour sélectionner tout en bas « je ne connais pas mon domaine d’activités » puis choisissez votre activité : tapez « livraison de courses à domicile ».</p>
                    <p>4) Vous allez ensuite arriver le formulaire de cessation d’activité :
                        Pour remplir ce dernier, veuillez vous référer au formulaire de déclaration de début d’activité en recopiant les informations renseignées plus tôt.
                        Validez cette démarche, imprimez le formulaire et envoyez-le par recommandé avec accusé de réception à l’URSSAF à l’adresse indiquée sur la dernière page.
                        Puis envoyez-nous la version numérique (pdf) à contact@ed-numerique.fr.</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">De quel matériel dois-je disposer pour pouvoir intervenir chez le client ?</button>
                <aside class="content">
                    <p>Nous recommandons aux Eds de se déplacer avec leur ordinateur, des tournevis pour ordinateur (en cas de dépannage), une clef usb, un câble RJ 45, leur téléphone portable, un calepin pour prendre des notes, et surtout, leur motivation et leur bonne humeur !</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Quelles compétences (techniques ou autres) dois-je avoir pour devenir un Ed ?</button>
                <aside class="content">
                    <p>Il faut avoir un niveau technique en informatique d'utilisateur au quotidien maitrisant autant l'aspect matériel, logiciel et procéduriel. Mais nous demandons avant tout d'avoir un sens aigu de la pédagogie, de la transmission du savoir, et du contact avec les gens. Résoudre les problèmes fait partie de votre mantra le plus puissant !</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Où trouver des outils/supports pour m’aider à assister les personnes physiquement et/ou à distance ?</button>
                <aside class="content">
                    <p>Nous allons prochainement mettre en ligne des contenus pédagogiques pour vous aider.  Nous vous transmettrons aussi des guides techniques et des liens de formation en ligne.</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Concernant l’aide à distance : un chat d’assistance est-il envisagé ?</button>
                <aside class="content">
                    <p>Oui ! Un chat d’assistance sera intégré sur la plateforme et facilitera les échanges et les interactions entre les médiateurs et les usagers. Nous vous tiendrons informés quand il sera déployé.</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Est-ce que je peux devenir un Ed en étant salarié ? Si oui, faut-il l’accord de son employeur ?</button>
                <aside class="content">
                    <p>Il est possible de cumuler le statut auto-entrepreneur et celui de salarié. Vous pourrez donc être un Ed sur votre temps libre, il n'est pas nécessaire d'obtenir l'accord de son employeur sauf pour la fonction publique.</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Qu’est-il fait des données récoltées sur la plateforme ?</button>
                <aside class="content">
                    <p>Les données récoltées dans les fiches Ed servent à avoir un meilleur suivi du dossier. 
                    En aucun cas ces données ne seront utilisées pour un autre usage que la plateforme. 
                    Ces données serviront au comité de pilotage de la plateforme pour ajuster le dimensionnement et la qualité des réponses. Dans le cadre de ce comité de pilotage, les données seront anonymisées.  
                    Pour plus d’information, voir la politique de confidentialité du site ed-numerique.fr : <a href="https://www.ed-numerique.fr?poldeconf">https://www.ed-numerique.fr?poldeconf</a>.</p>
                </aside>
            </article>
            <article>
                <button type="button" class="collapsible">Est-ce qu'Ed numérique est responsable de la prestation réalisée par l'intervenant ?</button>
                <aside class="content">
                    <p>La mission d'Ed numérique est de vous mettre en relation avec des intervenants numériques (les Eds) de votre région. Par conséquent, la prestation est de la responsabilité de l'intervenant, mais nous veillons à ce que nos Ed's respectent scrupuleusement notre Charte de Qualité (URL)</p>
                </aside>
            </article>
        </section> 
    </main>

    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>
    
    <?php include("views/footer.php");
    include("views/popup.php"); ?>
    <script type="text/javascript" src="views/assets/js/header_annuaire.js"></script>
    <script src="views/assets/js/faq.js"></script>
</body>
</html>
