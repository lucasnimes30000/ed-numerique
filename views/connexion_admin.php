<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="../views/assets/css/connexion.css">
    <link rel="shortcut icon" type="image/png" href="../views/assets/img/logo.svg"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>Connexion Admin</title>
</head>
<body>
    <main>
        <section class="mainBlock">
            <article class="blockDroit">
                <p id="emptyMail">Aucun mail renseigné</p>
                <p id="emptyPassword">Aucun mot de passe renseigné</p>
                <p id="empty">Aucune information renseignée</p>
                <p id="notFound">Identifiants incorrects</p>
                <p id="sessionerror">identifiants incorrects</p>

                <section class="txtBlock">
                    <h2 class="titreDroit">Administration</h2>
                    <hr><br>
                    <form action="../controller/verif_login_admin.php" method="POST" class="formBlocGauche">
                        <div>
                            <img src="../views/assets/img/formulaires/mail_icon_svg.svg" alt="Logo enveloppe" class="logoProfil_1">
                            <input required name="username" type="text" class="mail1Input formInput" placeholder="Nom d'utilisateur">
                        </div>
                        <div>
                            <img src="../views/assets/img/formulaires/mdp_icon_svg.svg" alt="Logo enveloppe" class="logoProfil_2">
                            <input required name="password" type="password" id = "input_mdp_log" class="MDPinput formInput" placeholder="Mot de passe">

                        </div>    
                        <br>

                        <div class="BTNs">
                            <button type="sumbit" class="BTNconnexion">Connexion</button>
                        </div>
                    </form>
                </section>
            </article>
        </section>
    </main>
</body>
</html>