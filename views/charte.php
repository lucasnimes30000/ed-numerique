<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Charte de bonne conduite qu’Ed numérique et les intervenants s’engagent à suivre tout au long
du processus d’accompagnement.">
    <?php include("views/metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/charte.css">
    <title>Charte Ed</title>
</head>
<body>
    <?php include("views/header_annuaire.php") ?>

    <section id="charte">
        <h1>Charte de bonne conduite Ed numérique</h1>

        <p>La présente charte s’inscrit dans le cadre de l’engagement d’Ed numérique et des intervenants ci-après dénommés les Ed’s, au bénéfice des personnes ayant pris contact en vue d’obtenir un accompagnement au numérique, ci-après dénommées les Diginautes.</p>
        <p>Les intervenants, en la signant, s’engagent à la respecter à tout moment lors de leur intervention au domicile des Diginautes qu’ils pourraient accompagner. </p>
        <p>Les obligations énoncées dans cette charte s’imposent à chaque intervenant de Ed numérique et sont susceptibles d’évoluer dans le temps à partir des observations et réclamations des Diginautes, des résultats d’ enquêtes de satisfaction, et de la consultation de chaque intervenant présent sur la plateforme Ed-numérique.</p>
        <p>Chaque intervenant s’impose le respect des obligations suivantes dans les limites fixées par la législation et la réglementation en vigueur. Le non-respect répété, par un intervenant, des obligations énoncées à la présente charte peut constituer un motif de rupture de la relation entre l’intervenant et Ed-numérique.</p>

        <h2>Une attitude générale de respect et de bienséance</h2>
        <h3>Confidentialité, ponctualité et autres règles de conduite</h3>
        <p>Ed numérique et l’ensemble de ses intervenants s’engagent formellement à :</p>
        <ul>
            <li>Respecter la confidentialité des informations qu’ils pourraient recueillir auprès des clients.</li>
            <li>Observer scrupuleusement les règles de politesse et de bienséance chez les Diginautes en toute circonstance.</li>
            <li>Observer l’interdiction de fumer et consommer des boissons alcoolisées au domicile des clients lors des interventions.</li>
            <li>Respecter les horaires définis avec le Diginaute et informer le client en priorité de tout retard ou empêchement de dernière minute.</li>
            <li>Informer l’équipe de Ed numérique de tout problème relationnel ou autre survenu lors de l’intervention.</li>
            <li>Adopter une hygiène personnelle.</li>
        </ul>

        <h3>Respect des biens matériels</h3>
        <ul>
            <li>Prendre soin des matériels et logiciels informatiques et numériques lors de chaque intervention.</li>
        </ul>

        <h3>Respect de la vie privée et de l’intimité</h3>
        <ul>
            <li>Respecter le secret professionnel en ne divulguant aucune information concernant un Diginaute à un tiers.</li>
            <li>Seuls les espaces nécessaires à la réalisation de la prestation de service à domicile seront fréquentés au domicile du Diginaute.</li>
            <li>Ne pas prendre position sur les croyances exprimées, sur les idées, opinions et choix de vie du client.</li>
        </ul>

        <h2>Un contrôle assidu de la qualité du service</h2>
        <h3>Avant l’intervention</h3>
        <ul>
            <li>Proposer une offre large, explicite et adaptée aux besoins individuels du Diginaute dans la mise en service, la configuration et l’utilisation des matériels informatiques et numériques du quotidien.</li>
            <li>Sélectionner des intervenants compétents, pédagogues, et empathiques.</li>
            <li>Proposer un prix transparent au diginaute.</li>
            <li>Utiliser des procédures de qualité et les faire appliquer par les intervenants lors de chaque intervention.</li>
        </ul>

        <h3>Pendant l'intervention</h3>
        <ul>
            <li>Assurer un accompagnement sur-mesure pour répondre à chaque besoin du Diginaute, dans le respect des individualités et des particularités de chacun.</li>
            <li>Fournir des informations complètes et fiables tout au long de la prestation.</li>
            <li>Répondre aux questions et donner des conseils et astuces au client afin de l’aider dans l’utilisation de ses matériels informatiques et numériques.</li>
        </ul>

        <h3>Après l’intervention</h3>
        <ul>
            <li>Ed numérique assure un suivi non systématique des prestations réalisées par les intervenants au domicile des Diginautes, mais procéde à des mesures correctives et d’amélioration constante de la qualité du service offert.</li>
            <li>Les réclamations, commentaires et observations apportés par les Diginautes à l’issue de l’intervention sont traités avec efficacité et attention.</li>
            <li>Ed numérique  est également attentif aux retours des Ed’s afin d’améliorer la qualité du service offert aux Diginautes.</li>
        </ul>
    </section>

    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>
    
    <?php include("views/footer.php");
    include("views/popup.php"); ?>
    <script src="views/assets/js/header_annuaire.js"></script>
</body>
</html>
