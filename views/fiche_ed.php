<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include("views/metadonnee.php"); ?>
    <link rel="stylesheet" href="views/assets/css/structure_commune.css">
    <link rel="stylesheet" href="views/assets/css/fiche_ed_az.css">
    <link rel="stylesheet" href="views/assets/css/header_annuaire.css">
    <link rel="stylesheet" href="views/assets/css/footer.css">
    <link rel="shortcut icon" type="image/png" href="views/assets/img/logo.svg"/>
    <script src="https://kit.fontawesome.com/af69fa6e52.js" crossorigin="anonymous"></script>
    <title>Fiche ed</title>
</head>
<body>

    <?php include("views/header_annuaire.php"); ?>
    <main>

        <div class="banniere">
            <h1 class="droite"></h1>
        </div>

        <img src="views/assets/user/<?= $id_user ?>/PP/<?= htmlspecialchars_decode($info_perso['url_photo'], ENT_QUOTES) ?>" class="pp" id="PP">

        <div class="test">

            <div class="infos">
                        <input id="user_id_choix" type="hidden" value="<?= $id_user ?>" name="user_id_choix">

                        <input type="hidden" name="" id="url_id" value="/views/assets/user/<?= $id_user ?>/PP/<?= htmlspecialchars_decode($info_perso['url_photo'], ENT_QUOTES) ?>">
                        <p class="NomPrenom"><?= $info_perso["nom_user"] ?> <?= $info_perso["prenom"] ?></p>
                        <p class="num"><span id="phone_number"><button  class="btnNum" id="see_number" type="button" style="font-size: 1.2rem"> <i class="fas fa-phone-alt"></i> <p>Numéro</p> </button></span></p>
                    
            </div>

            <div class="btns">

                <button class="btn myBtn" id="btn_mail">

                    <i class="fas fa-envelope"></i>
                    <p class="btnTxt" id="contact_ed" >Contacter</p>

                </button>

                <button class="btn" id="BTNCommentaire">

                    <i class="fas fa-thumbs-up"></i>
                    <p class="btnTxt">Recommander <span class="hid">ce Ed</span></p>

                </button>

            </div>

        </div>
       

        <div class="categories">

            <div class="gauche">

                <div class="petitBlocs">

                    <div class="h2">
                        <h2> Me connaître 
                            
                        </h2>
                        <i class="fas fa-caret-down "></i>
                    </div>
                    <p class="txt"><?= htmlspecialchars_decode($info_perso['me_connaitre'], ENT_QUOTES) ?></p>

                </div>

                <div class="petitBlocs">

                    <div class="h2">
                        <h2> Infos complémentaires</h2>
                        <i class="fas fa-caret-down "></i>
                    </div>
                    <div>
                        <?php 
                            foreach ($info_complementaire as $value){echo"<p>" . htmlspecialchars_decode($value,ENT_QUOTES) . "</p>";}
                        ?>
                    </div>
                </div>
                
                <div class="petitBlocs">

                    

                    <div class="h2">
                        <h2 id="text_aime"> J'aime </h2>
                        <i class="fas fa-caret-down "></i>
                    </div>
                   
                    <p class="txt"><?= htmlspecialchars_decode($info_perso['j_aime'], ENT_QUOTES) ?></p>

                </div>

            </div>

            <div class="Droite">

                <div class="grosBloc">

                    <div class="h2">
                        <h2 >Compétences</h2>
                        <i class="fas fa-caret-down "></i>
                    </div>
                    
                    <div class="txt ed">
                        

                    </div>  
                

                </div>
            </div>


        </div>
    
        <!-- Formulaire de contact -->

        <!-- La fenêtre -->
        <div id="myModal" class="modal" style="display:none">

        <!-- le contenu -->
        <div class="modal-content">
            <!-- croix pour fermer la fenêtre-->
            <span class="close"><i class="fas fa-times"></i></span>

            <!-- titre-->
            <h4 class="h4Mod"> Formulaire de contact</h4>
            <!--formulaire-->
            <form action="controller/bruh.php?id_user=<?= $id_user ?>" id="contact_form_ed" method="post" class="form">

                <article class="topMod">

                    <section class="gaucheMod">

                        <input class="inputMod inputJs inputPrenom" type="text" name="prenom" id="prenomMod" placeholder="prénom" value="">
                        <small class="emptyTxt">Champs obligatoire</small>
                        <input class="inputMod inputJs inputNom" type="text" name="nom" id="nomMod" placeholder="nom" value="">
                        <small class="emptyTxt">Champs obligatoire</small>
                        <input class="inputMod inputJs inputTelephone" type="text" name="telephone" id="phoneMod" placeholder="Numéro de téléphone" value="">
                        <small class="emptyTxt">Champs obligatoire</small>

                    </section>
                                    
                                    
                    <section class="droiteMod">

                        <img src="views\assets\img\formulaires\plane.png" alt="avion en papier volant" class="imgMod">
                        <input class="inputMod inputJs inputCp" type="text" name="cp" id="cpMod" placeholder="Code postal" value="">
                        <small class="emptyTxt">Champs obligatoire</small>
                        <input class="inputMod inputJs inputMail" type="email" name="mail" id="mailMod" placeholder="Adresse email" value="">
                        <small class="emptyTxt">Champs obligatoire</small>

                    </section>

                </article>
                
                <section class="messageMod">
                    <p class="txtMod">Message</p>

                    <textarea class="messageModA inputJs" name="message" id="messageMod" cols="30" rows="20"></textarea>
                    <small class="emptyTxt">Champs obligatoire</small>

                    <input class="inputJs"type="checkbox" name="checkMod" id="checkMod">
                    <label class="labelMod" for="checkMod">J'ai lu, compris et accepté. J’autorise Ed-Numérique à utiliser les données mentionnées ci-dessus, <a href="?poldeconf">dans les conditions prévues</a>. *</label>

                </section>
                <input type="hidden" id="recaptchaResponse" name="recaptcha-response">
                <button type="submit" name="envoyer" id="envoiMod">Envoyer</button>

            </form>
            
        </div>

        </div>
        

        <!-- fenetre de confirmation envoie message --> 
        <div id="modal2" class="modal2" >

            <!-- le contenu -->
            <div class="modal-content2">
                <!-- croix pour fermer la fenêtre-->
                <span class="close2"><i class="fas fa-times close2"></i></span>
                <span><i class="fas fa-check-circle checkMark"></i></span>
                <h4 class="h4Conf">MESSAGE BIEN ENVOYÉ</h4>
            </div>

        </div>

        <!-- Formulaire envoi Commentaire -->
        <div id="ModalCommentaire" class="modal" style="display: none;">

            <div class="modal-content">

                <span class="close" id="closeCom"><i class="fas fa-times"></i></span>

                <h4 class="h4Mod"> Laissez un commentaire</h4>

                <section class="form">
                
                    <article class="topMod">

                        <section class="gaucheMod">
                            <input class="inputMod inputPrenom" type="text" name="prenom_commentaire" id="inputPrenomCommentaire" placeholder="prénom" value="">
                            <small class="emptyTxt">Champs obligatoire</small>
                            <input class="inputMod inputNom" type="text" name="nom_commentaire" id="inputNomCommentaire" placeholder="nom" value="">
                            <small class="emptyTxt">Champs obligatoire</small>
                            <input class="inputMod inputTelephone" type="text" name="titre_commentaire" id="inputTitreCommentaire" placeholder="Titre de votre commentaire" value="">
                            <small class="emptyTxt">Champs obligatoire</small>
                        </section>

                        <section class="droiteMod">
                            <img src="views\assets\img\formulaires\plane.png" alt="avion en papier volant" class="imgMod">
                            <input class="inputMod inputMail" type="email" name="mail_commentaire" id="inputEmailCommentaire" placeholder="Adresse email" value="">
                            <small class="emptyTxt">Champs obligatoire</small>
                        </section>

                    </article>

                    <section class="messageMod">
                        <p class="txtMod">Commentaire</p>

                        <textarea class="messageModA" name="contenu_commentaire" id="inputMessageCommentaire" cols="30" rows="20"></textarea>
                        <small class="emptyTxt">Champs obligatoire</small>

                        <input type="checkbox" name="check_com" id="inputConditionCommentaire">
                        <label class="labelModCom" for="checkMod">J'ai lu, compris et accepté. J’autorise Ed-Numérique à utiliser les données mentionnées ci-dessus, <a href="?poldeconf">dans les conditions prévues</a>. *</label>
                    </section>

                    <!-- <input type="hidden" id="inputRecaptchaCommentaire"> -->
                    <button name="envoyer_commentaire" id="BTNenvoyer_commentaire">Envoyer</button>

                </section>

            </div>

        </div>

        <!-- <script src="https://www.google.com/recaptcha/api.js?render=6LfCxvgaAAAAAPn2k1k60rqRFUhhO9eBi8QEVsP2"></script>
            <script>
            grecaptcha.ready(function() {
                grecaptcha.execute('6LfCxvgaAAAAAPn2k1k60rqRFUhhO9eBi8QEVsP2', {action: 'homepage'}).then(function(token) {
                    document.getElementById('recaptchaResponse').value = token
                    document.getElementById('inputRecapchatCommentaire').value = token
                });
            });
            </script> -->

        <section class="commentaireEd">
            <?php
                foreach($displayCommentaire as $value){
                    echo $value;
                }
            ?>
        </section>
    </main>

    <a class="top-link hide" href="" id="js-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 6"><path d="M12 6H0l6-6z"/></svg>
        <span class="screen-reader-text">Back to top</span>
    </a>
    
    <?php
        include("views/footer.php");
        include("views/popup.php");
    ?>

    <script type="text/javascript" src="views/assets/js/header_annuaire.js"></script>
    <script type="text/javascript" src="views/assets/js/fiche_ed_az.js"></script>
    <script type="text/javascript" src="views/assets/js/fiche_ed.js"></script>
</body>
</html>
